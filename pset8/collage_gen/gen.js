var store_pics = [];

$(document).ready(function () {
                
    var text_canvas = document.getElementById('textCanvas');
    var text_canvas_ctx = text_canvas.getContext('2d');
    var image_elem = document.getElementById('collage');
    var text_field = document.getElementById('text');
    // used to determine if word one, and how much space left for words>1
    var area_remaining = 0;
    
    text_field.focus();

    text_field.addEventListener('keypress', function (e) {
        // create image on enter - one word at a time
        area_remaining = 0;
        var key = event.which || event.keyCode;
         
        if (key === 13) {
        
        // remove the previously created images, if applicable - clean canvas
        beforeText();
        
        // if no text entered, print message   
        if (text_field.value == "")
        {
            text_field.value = "Please type something before pressing enter. It gets much cooler than this...";
        }  
            // store each word, split by space
            var text_array = [];
            text_array = text_field.value.split(" ");
            var word_count = text_array.length;
            
            for (var i = 0; i < word_count; i++)
            {
                // get font, font size - store for later
                var new_size;
                var new_font = randomFont();
                // shadow for size limits - must match following "shadow" vars
                var new_shadow = '1px 1px 5px #595959';
                var new_colour = randomFontRGB();
                
                // shadow vars for canvas
                var shadow_blur = 2;
                var shadow_color = "#595959";
                var shadow_offsetX = 1;
                var shadow_offsetY = 1;

                // setup new div to aquire font size, "snug" fit
                var div = document.createElement('div');
                    div.setAttribute('id', 'tempDiv');
                    div.innerHTML = text_array[i];
                    div.style.position = 'absolute';
                    div.style.left = '-9999px';
                    div.style.top  = '-9999px';
                    div.style.fontFamily = new_font;
                    div.style.textShadow = new_shadow;
                    var word_width;
                    var word_height;
                    var count = 0;
                do {
                    
                    var canvas_height = document.getElementById('collage').clientHeight;
                    var canvas_width = document.getElementById('collage').clientWidth;
                    var cur_size;
                    new_size = randomFontSize(i, count, area_remaining, cur_size, word_count);
                    cur_size = new_size;
                    div.style.fontSize = new_size + 'pt'; // or 'px'
                    
                    document.body.appendChild(div);
                    // add two for shadow - has been tested with font size variations
                    word_width = div.offsetWidth + 2;
                    
                    // height is parsed to provide int on - not "n px"
                    // also, I had to use both heights and divide by two for a happy medium
                    word_height = (parseInt(window.getComputedStyle(div, null).fontSize, 10) + div.offsetHeight) / 2;
                    document.body.removeChild(div);
                    count++;
                }
                while (canvas_width < word_width || canvas_height < word_height);
                    
                // set area once size has been determined
                area_remaining += (word_width * word_height);
                // set the newly aquired attributes
                text_canvas_ctx.canvas.width = word_width;
                text_canvas_ctx.canvas.height = word_height;
                text_canvas_ctx.font = new_size + "pt " + new_font;
                text_canvas_ctx.shadowBlur = shadow_blur;
                text_canvas_ctx.shadowColor = shadow_color;
                text_canvas_ctx.shadowOffsetX = shadow_offsetX;
                text_canvas_ctx.shadowOffsetY = shadow_offsetY;
                text_canvas_ctx.textBaseline = "alphabetic";
                text_canvas_ctx.fillStyle = new_colour;
                // use text from user array, filled not outline
                // location is 0 - far left, new_size, size of font
                text_canvas_ctx.fillText(text_array[i], 0 , new_size);
                
                // store seperate images onto collage canvas
                image_elem.src = text_canvas_ctx.canvas.toDataURL();
                store_pics.push(image_elem.src);
                if (i == 0) {
                    var first_width = word_width;
                    var first_height = word_height;
                }
            }
            // clear text box after each word 
            $(this).val('');
            event.preventDefault();
            afterText(first_width, first_height);
        }
    });
    
    // used to display the dimensions of the collage - top left
    image_elem.addEventListener('click', function (e) {
        sizeIs();
    });
});

// clear the pics if the array has been populated already - fresh canvas
function beforeText() {

var image_count = store_pics.length;
    
    if ( image_count == 0) {
        return;
    }
    else{
        // Insert new elements in "collage" <div>
        // Create with DOM
        var remove_image = document.getElementById('collage');
        
        while (remove_image.hasChildNodes()) {
            remove_image.removeChild(remove_image.lastChild);
        }
        store_pics = [];
    }
}

// rotates the words randomly and prepends divs
function afterText(first_width, first_height) {
var image_count = store_pics.length;
var rotate = ["imagesNorm", "imagesRotate90", "imagesRotate270"];
var random;
var first_position;

    for (var i = 0; i < image_count; i++)
    {
        // first word always level
        if (i == 0) {
            var insert_one = '<div class=\"mainImage\" style=\"margin-left:';
            var insert_two = 'px; margin-top:';
            var insert_three = 'px;\"><img id=imagesMain src=';
            $("#collage").prepend( insert_one + ((first_width / 2) * -1) + insert_two + ((first_height / 2) * -1) + insert_three + store_pics[i] + "></div>");
            first_position =  findPosition(imagesMain);
            
var img = document.getElementById('imagesMain');
var canvas = document.createElement('canvas');

canvas.width = img.width;
canvas.height = img.height;
var draw_canvas_ctx =  canvas.getContext('2d');
draw_canvas_ctx.drawImage(img, 0, 0, img.width, img.height);
var imgd = canvas.getContext.getImageData(0, 0, img.width, img.height);
var pix = imgd.data;

// Loop over each pixel and invert the color.
for (var i = 0, n = pix.length; i < n; i += 4) {
    pix[i  ] = 255 - pix[i  ]; // red
    pix[i+1] = 255 - pix[i+1]; // green
    pix[i+2] = 255 - pix[i+2]; // blue
    // i+3 is alpha (the fourth element)
}
// Draw the ImageData at the given (x,y) coordinates.
canvas.putImageData(imgd, x, y);
            
        }
        else
        {
            // rotate the image
            random = Math.floor(Math.random() * rotate.length);
            var top;
            var left;
            random = 1;
            
            switch (true) {
                case (random == 1) :
                    top = first_position.y + "px";
                    left = first_position.x + "px";
                    break;
                case (random == 2) :
                    top = first_position.y + "px";
                    left = first_position.x + "px";
                    break;
                default :
                    top = first_position.y + "px";
                    left = first_position.x + "px";
            }
            var style = "style=\"top:" + top + "; left:" + left;
            // Insert new elements in "collage" <div> when creating DOM elements
            $("#collage").prepend("<img id=\"otherWords\" class=\"" + rotate[1] + "\" src=" + store_pics[i] + " " + style + "\">");
        }
    }
}

// returns the position of the element - used to position next element
function findPosition(element) {
    
    var x = 0;
    var y = 0;
    
    while (element.id != "collage") {
        if (element.tagName == "BODY") {
            var x_scroll = document.documentElement.scrollLeft || element.scrollLeft;
            var y_scroll = document.documentElement.scrollTop || element.scrollTop;
            
            x += (element.offsetLeft - x_scroll + element.clientLeft);
            y += (element.offsetTop - y_scroll + element.clientTop);
        } 
        else {
            x += (element.offsetLeft - element.scrollLeft + element.clientLeft);
            y += (element.offsetTop - element.scrollTop + element.clientTop);
        }
        element = element.offsetParent;
    }
    return {
        x: x,
        y: y
    };
}

function randomFont() {
    var font_type = ["Helvetica", "Courier New", "Lucida Console", "Verdana", "Trebuchet MS", "Tahoma", "Lucida Sans Unicode", "Impact", "Comic Sans MS", "Arial Black", "Arial", "Times New Roman", "Palatino Linotype", "Georgia"];
    var num;
    var font_count = font_type.length;
    num = Math.floor(Math.random() * font_count);
    return font_type[num];
}

function randomFontSize(word_count, count, area_remaining, cur_size, total_words) {
    // get area of div
    var canvas_height = document.getElementById('collage').clientHeight;
    var canvas_width = document.getElementById('collage').clientWidth;
    // used for switch
    var switch_it = canvas_width / canvas_height;
    var shape = '';
    var font_size;
    
    // switch to determine shape of canvas
    switch (true) {
        case (switch_it < .33) :
            // size = vertical banner
            shape = 'v_banner';
        break;
        
        case (switch_it >= .33 && switch_it < .5) :
            // size = vertical aside banner - thick banner
            shape = 'v_aside';
        break;
        
        case (switch_it >= .5 && switch_it < .8) :
            // size = vertical rectangle
            shape = 'v_rect';
        break;
            case (switch_it >= .8 && switch_it < 1.2) :
                // size = square... ish
                shape = 'square';
            break;
            
        case (switch_it >= 1.2 && switch_it < 2) :
            // size = horizontal rectangle
            shape = 'h_rect';
        break;
        
        case (switch_it >= 2 && switch_it <= 3) :
            // size = thick horizontal banner
            shape = 'h_aside';
        break;
        
        case (switch_it > 3) :
            // size = horizontal banner
            shape = 'h_banner';
        break;
        
        default :
            // size = other, i.e. oops!
            shape = 'irregular';
    }
    
    // if it's the first word in array
    // adjust font size depending on canvas shape
    if (area_remaining == 0) {
        if (shape == 'v_rect' || shape == 'square' || shape == 'h_rect') {
            // if word too big, divide but count with each loop - cute
            font_size = (canvas_height < canvas_width) ? (canvas_height / (count + 2)) : (canvas_width / (count + 2));
            return font_size;
        }
        else if (shape == 'h_banner' || shape == 'v_banner') {
            font_size = (canvas_width / 3) / (count + 2);
            return font_size;
        }
        else {
            font_size = (canvas_height / (count + 2));
            return font_size;
        }
    }
    else {
        // 10% of words will be considerably bigger than the current font size
        var size_salt = (Math.floor(Math.random() * (10)));
        font_size;
        
        // if word too big, divide but count with each loop
        if (shape == 'v_rect' || shape == 'square' || shape == 'h_rect') {
            if (size_salt == 2) {
                font_size = (canvas_height < canvas_width) ? (canvas_height / (count + 2 + word_count)) : (canvas_width / (count + 2 + word_count));
            }
            else {
                font_size = (canvas_height < canvas_width) ? (canvas_height / (count + 2)) : (canvas_width / (count + 2));
            }
        }
        else if (shape == 'h_banner' || shape == 'v_banner') {
            if (size_salt == 2) {
                font_size = (canvas_width / 2) / (count + 2);
            }
            else {
                font_size = (canvas_width / 3) / (count + 2);
            }
        }
        else {
            if (size_salt == 2) {
                font_size = (canvas_height / (count + 2));
            }
            else {
                font_size = (canvas_height / (count + 2));
            }
        }
    }
    // if n > n - 1; reduce n - 1;
    font_size = checkSize(cur_size, font_size, total_words, word_count);
    return font_size;
}

// recursive funciton to ensure n !> (n-1);
function checkSize(cur_size, font_size, total_words, word_count) {
    if (font_size >= cur_size) {
        if (word_count == 0) {
            return font_size;
        }
        else if (word_count == 1) {
            // second word reduce size by 50% to increase margin from word one
            font_size = (cur_size / 2);
            return font_size;
        }
        else {
            // words > 1 can then be gradually reduced
            font_size -= (cur_size / total_words) < .05 ? .05 : (cur_size / total_words);
            font_size = checkSize(cur_size, font_size, total_words);
            return font_size;
        }
    }
    else if (font_size <= 2) {
        font_size = 2;
    }
    return font_size;   
}

// thanks to http://stackoverflow.com/questions/1484506/random-color-generator-in-javascript
// vute, I owuld have used an arrray for 'letters'
function randomFontRGB() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function sizeIs() {
    var height = document.getElementById('collage').clientHeight;
    var width = document.getElementById('collage').clientWidth;
    var both = "Collage Size: Height " + height + " x Width " + width;
    document.getElementById('divSize').innerHTML = both;
}
