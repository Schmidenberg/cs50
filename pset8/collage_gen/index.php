<!DOCTYPE html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="/gen.js"></script>
<link href="/gen.css" rel="stylesheet"/>

<script>

</script>


<html>
    <head>
        <title>Fun Times</title>
    </head>
    <body>
        <!-- Table to create columns -->
       <table id='table'>
           <!-- Step One -->
           <td id='step1'>
           <fieldset id='fieldSteps' class='button'>
                <legend class='legends'>
                    Step 1:
                </legend>
                        <div>
                           Drag the: <a><img class='dragCornerImg' alt="Drag Example" src="/img/DragCorner.png"/>, in the container below, </a>
                        </div>
                       <div style='padding-top: 5px'>
                        to resize the image.
                        </div>
                    <div style='padding-top: 5px'>
                        Or you can use my presets:
                    </div>
                </fieldset>
            </td>
            <!-- Step Two -->
            <td id='step2'>
                <fieldset id='fieldSteps' class='button'>
                    <legend class='legends'>
                        Step 2:
                    </legend>
                        Use your words! Typed or pasted...
                        <form id='textForm'>
                            <input type = 'text' id='text' autocomplete="off"></textarea>
                        </form>
                        <div class='rhyme'>
                        ... it matters none, it shant be wasted.
                        </div>
                        <div id='padRhyme' class='rhymeBottom'>
                        My only rule, for custom fun; only press enter once you're done!
                        </div>
                </fieldset>
                
            </td>
            <!-- Step Three -->
            <td id='step3'>
                <fieldset id='fieldSteps' class='button'>
                    <legend class='legends'>
                        Step 2.5 :
                    </legend>
                        Or quit the rhymes, and just import one of my templates...
                </fieldset>
            </td>
            <!-- Step Three -->
            <td id='step4'>
                <fieldset id='fieldSteps' class='button'>
                    <legend class='legends'>
                        Step 3:
                    </legend>
                        Pick a colour, any colour (well... 0 to 3 colours, really)
                </fieldset>
            </td>
        </table>
            <div id = 'maxsize'>
            <fieldset id='canvasFields' class='canvasButton'>
                <legend id = 'divSize'>
                </legend>
                <canvas id='textCanvas' height=100 onresize="myScript">
                </canvas>
                <div id = "collage">
                    <script>
                    // used to display the dimensions of the collage - top left
                        $(document).ready(function () {
                            $('#collage').css('click', sizeIs());
                        });
                    </script>
                </div>
            </fieldset>
            </div>
        </form>
    </body>
</html>