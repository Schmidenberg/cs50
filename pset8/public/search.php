<?php

    require(__DIR__ . "/../includes/config.php");

    // numerically indexed array of places
    $places = [];
    $user_typed = $_GET["geo"];
    
    //seperate geo words
    $words = explode(", " , $user_typed);
    
    // first, we check to see what $geo matches - numbers / text, comma / space
    if (is_numeric($words[0]))
    {
        // if numeric, only use numeric - ignore rest
        $places = CS50::query("SELECT * FROM places WHERE postal_code LIKE ?", $words[0] . "%");
    }
    else
    {
        $user_typed = strtolower($user_typed);
        
        // check if user added comma
        if (substr_count($user_typed, ",") != 0 )
        {
            if (empty($words[1]))
            {
                // if user omits word after comma, match place or state
                $places = CS50::query("SELECT * FROM places WHERE MATCH (place_name, admin_name1) AGAINST (?) LIMIT 25;", $user_typed);
            }
            else
            {
                // else assume that area & state are correct but state not complete
                $places = CS50::query("SELECT * FROM places WHERE MATCH (place_name) AGAINST (?) AND (admin_code1) LIKE ? OR (admin_name1) LIKE ? LIMIT 25;", $words[0], $words[1], $words[1] . "%");
            }
        }
        
        // if no comma but space separated word
        else if (substr_count($user_typed, " ") != 0)
        {
            // assume area from place or state
            $places = CS50::query("SELECT * FROM places WHERE (place_name) = ? OR (admin_name1) = ? LIMIT 25;", $user_typed, $user_typed);
        }
        else
        {
            // not number or comma/space seperated search
            $places = CS50::query("SELECT * FROM places WHERE MATCH (place_name, admin_name1) AGAINST (?) LIMIT 25;", $user_typed);
        }
    }
    
    // catch if places is empty - or abbreviated
    if (empty($places))
    {
        $places = CS50::query("SELECT * FROM places WHERE place_name LIKE ? OR admin_name1 LIKE ? LIMIT 25;", "%" . $user_typed . "%", "%" . $user_typed . "%");
    }
    
    // spelling issue - nothing to return
    
    // output places as JSON (pretty-printed for debugging convenience)
    header("Content-type: application/json");
    print(json_encode($places, JSON_PRETTY_PRINT));

?>