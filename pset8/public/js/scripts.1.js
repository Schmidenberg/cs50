/* global google */
/* global _ */
/**
 * scripts.js
 *
 * Computer Science 50
 * Problem Set 8
 *
 * Global JavaScript.
 */
'use strict';

// Google Map
var map;

// markers for map
var markers = [];

// info window
var info = new google.maps.InfoWindow();

function initMap() {
  }

// execute when the DOM is fully loaded
$(function() {

    // styles for map
    // https://developers.google.com/maps/documentation/javascript/styling
    var styles = [

        // hide Google's labels
        {
            featureType: 'all',
            elementType: 'labels',
            stylers: [
                {visibility: 'off'}
            ]
        }
    ];

    // options for map
    // https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var options = {
        center: {lat: 42.377019, lng: -71.116703}, // Harvard!!
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP, //HYBRID
        maxZoom: 14,
        panControl: true,
        styles: styles,
        zoom: 13,
        zoomControl: true
    };

    // get DOM node in which map will be instantiated
    var canvas = $('#map-canvas').get(0);

    // instantiate map
    map = new google.maps.Map(canvas, options);

    // configure UI once Google Map is idle (i.e., loaded)
    google.maps.event.addListenerOnce(map, 'idle', configure);

});

/**
 * Adds marker for place to map.
 */
 
function addMarker(db, timeout)
{
    var marker = [];
  
    // fancy drop icons
    window.setTimeout(function() {
        
        var placeName = (db.place_name+', '+db.admin_name1);
        // create the markers
        marker = new MarkerWithLabel({
            position: {lat: parseFloat(db.latitude), lng:parseFloat(db.longitude)},
            map: map,
            labelContent: placeName,
            animation: google.maps.Animation.DROP,
            labelAnchor: new google.maps.Point(15, 0),
            labelClass: 'labels', // the CSS class for the label
            icon: 'https://maps.google.com/mapfiles/kml/pal4/icon8.png'
        });
        // this ads each marker to array for removeMarkers()
        markers.push(marker);
        // call functions for listener
        openInfo(db, marker);
    }, timeout);
}

/**
 * Configures application.
 */
function configure()
{
    // update UI after map has been dragged
    google.maps.event.addListener(map, 'dragend', function() {
        update();
    });

    // update UI after zoom level changes
    google.maps.event.addListener(map, 'zoom_changed', function() {
        update();
    });

    // remove markers whilst dragging
    google.maps.event.addListener(map, 'dragstart', function() {
        removeMarkers();
    });

    // configure typeahead
    // https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md
    $('#q').typeahead({
        autoselect: true,
        highlight: true,
        minLength: 1
    },
    {
        source: search,
        templates: {
            empty: 'no places found yet',
            suggestion: _.template('<p><%- place_name %>, <%- admin_name1 %></p> ')
        }
    });

    // re-center map after place is selected from drop-down
    $('#q').on('typeahead:selected', function(eventObject, suggestion, name) {

        // ensure coordinates are numbers
        var latitude = (_.isNumber(suggestion.latitude)) ? suggestion.latitude : parseFloat(suggestion.latitude);
        var longitude = (_.isNumber(suggestion.longitude)) ? suggestion.longitude : parseFloat(suggestion.longitude);

        // set map's center
        map.setCenter({lat: latitude, lng: longitude});

        // update UI
        update();
    });

    // hide info window when text box has focus
    $('#q').focus(function(eventData) {
        hideInfo();
    });

    // re-enable ctrl- and right-clicking (and thus Inspect Element) on Google Map
    // https://chrome.google.com/webstore/detail/allow-right-click/hompjdfbfmmmgflfjdlnkohcplmboaeo?hl=en
    document.addEventListener('contextmenu', function(event) {
        event.returnValue = true; 
        event.stopPropagation && event.stopPropagation(); 
        event.cancelBubble && event.cancelBubble();
    }, true);

    // update UI
    update();

    // give focus to text box
    $('#q').focus();
}

/**
 * Hides info window.
 */
function hideInfo()
{
    info.close();
}

/**
 * Removes markers from map.
 */
function removeMarkers()
{
    // https://developers.google.com/maps/documentation/javascript/examples/marker-remove
    
    setMapOnAll();
    deleteMarkers();
    
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }
      
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }
    function clearMarkers() {
        setMapOnAll(null);
    }
}

/**
 * Searches database for typeahead's suggestions.
 */
function search(query, cb)
{
    // get places matching query (asynchronously)
    var parameters = {
        geo: query
    };
    $.getJSON('search.php', parameters)
    .done(function(data, textStatus, jqXHR) {

        // call typeahead's callback with search results (i.e., places)
        cb(data);
    })
    .fail(function(jqXHR, textStatus, errorThrown) {

        // log error to browser's console
        console.log(errorThrown.toString());
    });
}

/**
 * Shows info window at marker with content.
 */
function showInfo(marker, content)
{
    // start div
    var div = '<div id="info">';
    do
    {
        // http://www.ajaxload.info/
        div += '<img alt="loading" src="img/ajax-loader.gif"/>';
    }
    while (typeof(content) === 'undefined');
    
    div = '<div id="info">';
    div += content + '<br/>';
    
    showArticles(db, marker);
     div += 
    
    // end div
    div += '</div>';
    
    

    // set info window's content
    info.setContent(div);

    // open info window (if not already open)
    info.open(map, marker);
}

/**
 * Updates UI's markers.
 */
function update() 
{
    // get map's bounds
    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    // get places within bounds (asynchronously)
    var parameters = {
        ne: ne.lat() + ',' + ne.lng(),
        q: $('#q').val(),
        sw: sw.lat() + ',' + sw.lng()
    };
    $.getJSON('update.php', parameters)
    .done(function(data, textStatus, jqXHR) {

        // remove old markers from map
        removeMarkers();

        // add new markers to map
        for (var i = 0; i < data.length; i++)
        {
            addMarker(data[i], i * 200);
        }
     })
     .fail(function(jqXHR, textStatus, errorThrown) {

         // log error to browser's console
         console.log(errorThrown.toString());
     });
}

// function I've created
// Provides the info for articles when clicked
function openInfo(db, marker) 
{
    // add location for other functions
    var curTime = Date.now();
    var timeInfo;
    var timeNow = ('&timestamp=' + Math.floor(curTime / 1000));
    var myKey = '&key=AIzaSyB51GLajuH7L0bnnAE25QM3Q3cPGTH9pYg';
    var articles; 
    var weatherKey = '5af7a1254238df48';
    var url = 'https://api.wunderground.com/api/' + weatherKey + '/conditions/q/' + loc_zip + '.json?';
    var location = (parseFloat(db.latitude) + ', ' + parseFloat(db.longitude));
    var loc_zip = db.postal_code;
    
    $.when(
        $.getJSON('https://maps.googleapis.com/maps/api/timezone/json?location=' + location + timeNow + myKey),
        $.getJSON('articles.php', {
            geo: loc_zip,
        }),
         $.getJSON(url)
    )
    .then(
        function(db, textStatus, jqXHR) {
        var maxEntries = db.length;
        console.log(db);
        if ( maxEntries == 0 )
        {
            articles = 'No news is good news';
        }
        else {
            maxEntries = 10;
            articles = '<ul/>';
            for (var i = 0; i < maxEntries; i++){
                articles += '<li><a href=' + db[i].link + ' target=\'_blank\'>' + db[i].title + '</a></li>';
            }
            articles += '</ul>';
        }
    },
    function(data){
        // create local time from time zone
        var localTime = moment(curTime).tz(data.timeZoneId).format('YYYY-MM-DD HH:mm');
        
        if (data) 
        {
             timeInfo = '<p><b>This Area\'s Time Zone:</b><br/>' + data.timeZoneId + '</br>' + localTime + '</p>';
        } 
        else 
        {
            timeInfo = '<p>(No time zone data available)</p>';
        }
     },
     function(data) {
     var weatherInfo;
        
        if (data)
        {
            weatherInfo = '<p><b>Local Weather: </b><br/>' + data.current_observation.temperature_string + '</p>';
        }
        else 
        {
            weatherInfo = '<p>(No weather data available)</p>';
        }
    }
    );
    
    // use current data, and add timezone & weather data
    showTime(location);
    showWeather(loc_zip);
    showArticles(db, marker);
    
    
    // click events
    google.maps.event.addListener(marker, 'click', function() {
    marker.setAnimation(google.maps.Animation.BOUNCE);
    marker.setAnimation(null);
    
    // show one marker's info only, with data
    showInfo(marker, articles);
    });
}
