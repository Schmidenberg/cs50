<?php

    /**
     * helpers.php
     *
     * Computer Science 50
     * Problem Set 7
     *
     * Helper functions.
     */
    
    require_once("config.php");

    /**
     * Number of questions populated in m-choice quiz
     * Ultimatley will be stored in DB and updated for each quiz
     * by the admin that generates quiz
     */
    define("QUESTION_COUNT", 40); // change constants in JS
    define("PASS_RATE", 80); // change constants in JS
    
    /**
     * Apologizes to user with message.
     */
    function apologize($message)
    {
        render("apology.php", ["message" => $message, "title" => "Error"]);
    }

    /**
     * Facilitates debugging by dumping contents of argument(s)
     * to browser.
     */
    function dump()
    {
        $arguments = func_get_args();
        require("../views/dump.php");
        exit;
    }

    /**
     * Logs out current user, if any.  Based on Example #1 at
     * http://us.php.net/manual/en/function.session-destroy.php.
     */
    function logout()
    {
        // unset any session variables
        $_SESSION = [];

        // expire cookie
        if (!empty($_COOKIE[session_name()]))
        {
            setcookie(session_name(), "", time() - 42000);
        }

        // destroy session
        session_destroy();
    }

    /**
     * Redirects user to location, which can be a URL or
     * a relative path on the local host.
     *
     * http://stackoverflow.com/a/25643550/5156190
     *
     * Because this function outputs an HTTP header, it
     * must be called before caller outputs any HTML.
     */
    function redirect($location)
    {
        if (headers_sent($file, $line))
        {
            trigger_error("HTTP headers already sent at {$file}:{$line}", E_USER_ERROR);
        }
        header("Location: {$location}");
        exit;
    }

    /**
     * Renders view, passing in values.
     */
    function render($view, $values = [])
    {
        // if view exists, render it
        if (file_exists("../views/{$view}"))
        {
            // extract variables into local scope
            extract($values);

            // render view (between header and footer)
            require("../views/header.php");
            require("../views/{$view}");
            require("../views/footer.php");
            exit;
        }

        // else err
        else
        {
            trigger_error("Invalid view: {$view}", E_USER_ERROR);
        }
    }
    
        /**
     * 
     * used in header.php
     * 
     * Inputs: string, passed in from render() for each public page
     * Returns: no values
     * 
     * prints page title in browser tab
     * 
     */
    function browser_title($title)
    {
        if (isset($title))
            print('<title>Staff Training: '. htmlspecialchars($title) . '</title>');
        else
            print('<title>Staff Training</title>');
    }
    
    /**
     * 
     * used in header.php
     * 
     * Inputs: takes no arguments
     * Returns: no values
     * 
     * prints primary tabs in the navigation panel
     * 
     * makes use of:    $_SESSION["id"]
     *                  $_SESSION["otp_active"] 
     *                  $_SESSION["user_type"]
     *                  basename($_SERVER['SCRIPT_NAME'])
     * 
     */
    function insert_primary_tabs()
    {
        if (!empty($_SESSION["id"]))
        {
            // if user flagged for quiz - Quiz display time remaining when otp active and in quiz page
            if ($_SESSION["otp_active"] == 1 && basename($_SERVER['SCRIPT_NAME']) == 'questions.php')
            {
                $rows = CS50::query("SELECT last_login FROM users WHERE id = ?", $_SESSION["id"]);
                
                print("<div id=\"timeLeft\"></div>");
                print("<br><div id=\"answerCount\"></div>");
            }
        }
        
        // if user standard login (no OTP) and privileges > than consultant
        if (!empty($_SESSION["id"]) && $_SESSION["otp_active"] != 1  && $_SESSION["user_type"] != "Consultant")
        {
            print('<ul class="nav nav-tabs noMsgBorder">');
            print('<li><a id="bootstrapOverride" class="mainTabs" href="training.php">Training</a></li>');
            print('<li><a id="bootstrapOverride" class="mainTabs" href="reports.php">Reports</a></li>');
            print('<li><a id="bootstrapOverride" class="mainTabs" href="admin.php">Admin</a></li>');
            print('</ul>');
        } 
        // else a consultant is logged in - no need to have tabs as they can only view training
        elseif (!empty($_SESSION["id"]) && $_SESSION["otp_active"] != 1  && $_SESSION["user_type"] == "Consultant")
        {
            print('<ul class="nav nav-tabs noMsgBorder">');
            print('<li><a id="bootstrapOverride" class="mainTabs" href="training.php">Training</a></li>');
            print('</ul>');
        }
    }
    
    /**
     * 
     * used in header.php
     * 
     * Inputs: takes no arguments
     * Returns: no values
     * 
     * .user_profile database table
     * 
     * prints user details in nav panel, once logged in, and not in quiz
     * 
     * makes use of:    $_SESSION["id"]
     *                  $_SESSION["otp_active"] 
     *                  $_SESSION["user_type"]
     *                  basename($_SERVER['SCRIPT_NAME'])
     * 
     */    
    function logged_in_user_details()
    {
        if (!empty($_SESSION["id"]))
        {
            $rows = CS50::query("SELECT name FROM user_profile WHERE id = ?", $_SESSION["id"]);
            print("Welcome, ".$rows[0]["name"]);
        }
            
        if (!empty($_SESSION["id"]) && $_SESSION["otp_active"] != 1)
            print('<br/><a href="logout.php"><strong>Log Out</strong></a>');
    }
    
     /**
     * mySQL query to get otp status - used for login and page loads
     */    
    function get_otp_status($userId)
    {
        $otp_status = CS50::query("SELECT `otp_active` FROM users WHERE id = ?", $userId);
        return $otp_status[0]["otp_active"];
    }
    
     /**
     * checks each page load checks the users OTP
     * if OTP is changed while user is logged in - log user out
     */
    function check_otp($userId)
    {
        $otp_state = get_otp_status($userId);
        
        if (basename($_SERVER['SCRIPT_NAME']) == 'index.php')
            return false;
        else
        {
            if ($otp_state)
            {
                // a little abrubt, but clean
                redirect("/index.php");
            }
            else
                return false;
        }
    }
    
    /**
     * populates the "response.php" page with a custom message - response page also times out to redirect
     */
    function response($url, $linktext, $message, $header, $title)
    {
        render("response.php", ["url" => $url, "linktext" => $linktext, "header" => $header, "message" => $message, "title" => $title]);
    }
    
    /**
     * a number of "Distinct" SQL calls are made individually -
     * this consolidates the associative arrays into one, more friendly, version
     */
    function beautify_array($groupingToUse) 
    {
        $count = count($groupingToUse);
        $groups = [];
        // step through all possible companies and create array substrings of all possibilites
        for ($i = 0 ; $i < $count; $i++)
        {
            if (gettype($groupingToUse[$i]) == "array")
            {
                $comp = array($groupingToUse[$i]["company"]);
                $groups[$i] = array_merge($comp, $groupingToUse[$i]["division"], $groupingToUse[$i]["user_type"]);
            }
        }
        return $groups;
    }

    /**
     * checks if the users correct answers qualify as pass
     * makes use of php constants - JS also has a constant for pass rate, FYI
     */
    function checkPass($num)
    {
         $passRate = 0;
         if ($num >= ((QUESTION_COUNT / 100) * PASS_RATE))
         {
             $passRate++;
         }
         return $passRate;
    }
    
    /**
     * recursively sorts array-keys within a given array
     * charts will then print in order
     */
    function sortArrayKeys(&$myArray)
    {
        if (!is_array($myArray))
        {
            return false;
        }
        ksort($myArray);
        foreach ($myArray as $key=>$value) {
            sortArrayKeys($myArray[$key]);
        }
        return true;
    }

    /**
     * populates and prints the required groupings for the JS charts to "pickup"
     */
    function createJsonforJS($passRate_group, $failRate_group, $usersInCompany)
    {
        // provide total companies count and keys for dynamically populated JS reports
        $countCompanies = count($usersInCompany);
        $companyList = array_keys($usersInCompany);
        
        for ($i = 0; $i < $countCompanies ; $i++)
        {
            // store each pass grouping in the specific company if grouping is true
            if ($passRate_group)
            {
                foreach($passRate_group as $passComp=>$passGroupVal)
                {
                    if ($companyList[$i] == $passComp)
                    {
                        $groupPass = [
                            "company" => $companyList[$i],
                            "passCount" => $passGroupVal,
                        ];
                    }
                }
            }
            // if pass groups false - (all failed)
            else
            {
                $groupPass = [
                    "company" => $companyList[$i],
                    "passCount" => 0,
                ];
            }
            // store each fail group in specific company if groups are true
            if ($failRate_group)
            {
                foreach($failRate_group as $failComp=>$failGroupVal)
                {
                    if ($companyList[$i] == $failComp)
                    {
                        $groupFail = [
                            "company" => $companyList[$i],
                            "failCount" => $failGroupVal,
                        ];
                    }
                }
            }
            // if fail groups false - (100% pass rate)
            else
            {
                $groupFail = [
                    "company" => $companyList[$i],
                    "FailCount" => 0,
                ];
            }
            
            // if users in pass and fail groups exist - merge array on company, for ease of use
            if ($groupFail["company"] === $groupPass["company"])
            {
                $groupPassFail[$i] = array_merge($groupFail, $groupPass);
            }
            // else set pass to 0 - set fails to actual value
            else if ($groupFail["company"] === $companyList[$i])
            {
                $groupPass = [
                    "company" => $companyList[$i],
                    "passCount" => 0,
                ];
                $groupPassFail[$i] = array_merge($groupFail, $groupPass);
            }
            // else set fail to 0 - set passes to actual value
            else if ($groupPass["company"] === $companyList[$i])
            {
                $groupFail = [
                    "company" => $companyList[$i],
                    "failCount" => 0,
                ];
                $groupPassFail[$i] = array_merge($groupFail, $groupPass);
            }
        }
        // print the encoded result, in hidden group, for JavaScript to "pick-up"
        print_r(json_encode($groupPassFail));
    }

    /**
     * sends an email from the "bot" with custom name/s and email/s
     */    
    function send_email($userDetails, $msgType, $userId)
    {
        // repeat send for each user in array
        foreach($userDetails as $user) 
        {
            // rest pwd URL with random token URL
            // http://stackoverflow.com/questions/18830839
            $thisEmail = $user["email"];
            $thisName = $user["name"];
            $thisLastLogin = $user["last_login"];
            $token = sha1(uniqid($thisEmail . $thisName, true));
            $tokenedUrl = "https://ide50-warren-schmidenberg.cs50.io/reset_password.php?token=" . $token;

            // add token to db with expiry
            $rows = CS50::query("INSERT IGNORE INTO reset_passwords (`date_triggered`, `user_id`, `user_email`, `user_name`, `hash`, `is_active`)
                                 VALUES(now(), ?, ?, ?, ?, (DATE_ADD(NOW(), INTERVAL 24 HOUR)));", $userId, $thisEmail, $thisName, $token);
            
            $mail = new PHPMailer();
                $mail->IsSMTP();
                $mail->SMTPAuth = true;
                $mail->IsHTML(true);
                $mail->SMTPSecure = 'tls';
                $mail->Host = 'smtp.gmail.com'; // change to your email host
                $mail->Port = 587; // change to your email port
                $mail->Username = "w.schmidenberg@gmail.com"; // change to your username
                $mail->Password = "wchnftaclmwilmxw"; // change to your email password
                $mail->setFrom("quiz_bot@company.com", "Quiz_bot"); // change to your email - or alias
                $mail->AddAddress($thisEmail); // change to user's email address

            // possible msgtypes - which change the body and subject of the email
            if($msgType === "user_request")
            {
                if ($thisLastLogin === "2000-01-01 00:00:00")
                {
                    $mail->Subject = "Company Quiz Bot - Create a Password";
                    $str = "<p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">Hey " . $thisName . ",<p/>
                            <br/>
                            <p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">It's great to put a face to the name!<p/>
                            <p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">I see that you need to set your Training Site password, no problemo...<p/>
                            <br/>
                            <a style=\"font-family:Helvetica; font-size: 12px;\" href=" . $tokenedUrl . ">Follow me to create your password</a>.
                            <br/>
                            <br/>
                            <p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">BTW, this reset will expire in 24 hours. <p/>
                            <br/>
                            <p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">See you again soon,<p/>
                            <p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">Quiz_bot<p/>";
                    
                    $mail->Body = $str;
                }
                else
                {
                    $mail->Subject = "Company Quiz Bot - Password Change Request";
                    $str = "<p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">Hey " . $thisName . ",<p/>
                            <br/>
                            <p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">You requested a password reset<p/>
                            <br/>
                            <a style=\"font-family:Helvetica; font-size: 12px;\" href=" . $tokenedUrl . ">Follow me to reset your password</a>.
                            <br/>
                            <br/>
                            <p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">the reset link will expire in 24 hours. <p/>
                            <br/>
                            <p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">Thanks,<p/>
                            <p style=\"font-family:Helvetica; font-size: 12px; color: black; text-shadow: 1px 1px 1px rgba(0,131,255,0);\">Quiz_bot<p/>";
                    
                    $mail->Body = $str;
                }
            }
            else if ($msgType === "admin_request")
            {
                $mail->Subject = "Company Quiz Bot - Quiz Time!";
                $mail->Body = "<h1>hello, !</h1>\n\nYou requested a password reset\n
                <a href='https://ide50-warren-schmidenberg.cs50.io/login.php'>Take The Quiz</a>.\n
                <h3>RULE ONE:\n/h3>
                <h2>You only have 24 hours to complete this quiz from \n/h2>
                <h3>RULE TWO\n/h3>
                <h2>RULE ONE:\n/h2>
                <h3>RULE THREE\n/h3>
                <h2>RULE ONE:\n/h2>
                <h5>Thanks,</h5>\n\n<h5>Quiz_Bot</h5>";
            }
            if ($mail->Send() == false)
            {
                apologize("Email Issue, contact Operations.");
            }
        }
    }
    
    function login($password, $uName)
    {
        
        $rows;
        $row;
        
        // password reset uses id, standard log in uses username
        // tests if uname is a number (id) or string (username) - login changes accorgingly
        $tryInt = (int)$uName;
        if ($tryInt === 0)
            // query database for user
            $rows = CS50::query("SELECT * FROM users WHERE username = ?", $uName);
        else
            $rows = CS50::query("SELECT * FROM users WHERE id = ?", $uName);
        
        // if user row is not found, username is incorrect
        if (count($rows) == 1)
            // first (and only) row
            $row = $rows[0];
        else 
            apologize("Invalid username and/or password.");
            
        // if we found user, check password
        if (!password_verify($password, $row["password"]) && !password_verify($password, $row["password"]))
            apologize("Invalid username and/or password.");
        
        else
        {
            // remember the user's now logged in, and by type, by storing in session
            $_SESSION["id"] = $row["id"];
            $_SESSION["user_type"] = $row["user_type"];
            
            // retrieve otp as seprate query - used on each page load 
            // to check if status changes while user is logged in
            $_SESSION["otp_active"] = get_otp_status($_SESSION["id"]);
        
            // if otp is active - can't view "training", must take test
            if ($row["otp_active"] == 1)
            {
                if (password_verify($password, $row["password"]))
                {
                    // update last login date in db
                    $loginTime = CS50::query("UPDATE `users` SET `last_login`= now() WHERE `id` = ?;", $row["id"]);
    
                    // redirect to take the multiple choice quiz
                    render("quiz_form.php", ["title" => "Quiz Prep"]);
                }
            }
            
            // compare hash of user's input against hashed pwd in database
            else
            {
                if (password_verify($password, $row["password"]))
                {
                    // update last login date in db
                    $rows = CS50::query("UPDATE `users` SET `last_login`= now() WHERE `id` = ?;", $row["id"]);
                    
                    // redirect to training pages
                    redirect("/training.php");
                }
            }
        }
    }
    
    // gathers answers from db for questions that were asked during quiz
    function get_answers($eachUser)
    {
        
        $aString = array();
        foreach ($eachUser["userAnswers"] as $getKey)
        {
            // if empty - user caused an illegal page refresh
            if(empty($getKey['q_id']) || !isset($getKey['q_id']))
            {
                $keys = $getKey;
                return $keys;
            }
            else
                $aString[] = $getKey['q_id'];
        }
        // dynamically generate query - user specific
        $aString = implode("', '", $aString);
        $str = "SELECT `stem`, `distractor_one`, `distractor_two`, `distractor_three`, `distractor_four`, `q_key` FROM `questions` WHERE `q_id` IN ('";
        $str = $str . $aString;
        $str = $str . "') ORDER BY FIELD(q_id,'";
        $str = $str . $aString . "') LIMIT 400;";
        $keys = CS50::query("$str");
        return $keys;
    }
    
    // if the focus on the quiz page is lost, submit form as illegal_move - no quiz data submitted
    function illegal_move($reason) 
    {
        $test_end = date("Y-m-d h:i:s");
        $results[] = "$reason";
        $results = json_encode($results);
        
        // add results, questions answered count and correctly answered count to db
        CS50::query("INSERT INTO `results`
                    (`id`, `test_start`, `test_end`, `illegal_move`, `company_cur`, `division_cur`, `branch_manager_cur`, `team_leader_cur`, `city_cur`, `location_cur`, `grouping_cur`, `q_results`, `q_asked_count`, `q_answered_count`, `q_correct_count`) 
                    SELECT ?, ?, ?, ?, `company`, `division`, `branch_manager`, `team_leader`, `city`, `location`, `grouping`, ?, ?, 0, 0
                    FROM `user_profile`
                    WHERE `id` = ?",
                    $_SESSION["id"], $_SESSION["start_timer"], $test_end, $reason, $results, QUESTION_COUNT, $_SESSION["id"]);
                    
        // log out current user, if any
        logout();
        
        apologize("Illegal move during quiz - Quiz Terminated - Contact your Administrator");
    }
    
    // admin.php - sets sort order of array by specific key
    //https://joshtronic.com/2013/09/23/sorting-associative-array-specific-key/
    function sort_by_key($field, &$array, $direction)
    {
    	usort($array, create_function('$a, $b', '
    		$a = $a["' . $field . '"];
    		$b = $b["' . $field . '"];
    
    		if ($a == $b)
    		{
    			return 0;
    		}

    		return ($a ' . ($direction == '2' ? '>' : '<') .' $b) ? -1 : 1;
    	'));
    	return true;
    }
    
    /**
     * 
     * admin.php - if user sorts by field, determine which direction to sort by
     * 
     * the use of '1' and '2' for sort order is intentionally ambiguous.
     * the db can't return an 'ORDER BY' because of how the associative array  
     * is built - array_push, etc.
     * the workaround: determine how to sort based on previous sort - if any
     * this swaps the URl description (the URL shows the previous sort) which
     * is confusing.
     * 
     */
    function direction_for_sort($previous_page, $current_page)
    {
        $previous_page = explode('=', $previous_page);
        $current_page = explode('=', $current_page);

        // gather previous sort, if any, and current sort
        $previous_page = split_page_vars($previous_page);
        $current_page = split_page_vars($current_page);
        
        // if first sort for column - sort asc, unless name then sort desc
        if ($current_page[0] !== $previous_page[0])
            if ($current_page[0] === 'name' && $previous_page[0] === '/admin.php')
                return '2';
        else
            return '1';
        
        // if not first sort for column - if name, reverse, else normal
        if ($current_page[0] == 'name')
            if (empty($current_page[1]))
                return '1';
            else
                if ($current_page[1] == '1')
                    return '2';
                else
                    return '1';
        
        else
            if (empty($current_page[1]))
                return '2';
            else
                if ($current_page[1] == '1')
                    return '2';
                else
                    return '1';
    };
    
    // determine if page has been sorted by user by splitting URI 
    function split_page_vars($page)
    {
        // if it has - split it up to field sorted by and direction
        if (!empty($page[1]))
        {
            $page = explode('_', $page[1]);
        }
        // else return original page
        else
            $page[0] = $page[0];
        return $page;
    }
    
    // training.php has a popup for returning users who can use localStorage
    function add_training_popup()
    {
        if (basename($_SERVER['SCRIPT_NAME']) == 'training.php')
        {
            print('<div class="popup" data-popup="popup-1" style="display: none;">');
                print('<div class="popup-inner">');
                    print('<p><a data-popup-revert="popup-1" href="#">Go Back</a> to where you left off?</p>');

                    print('<a class="popup-close" data-popup-close="popup-1" href="#">x</a>');
                print('</div>');
            print('</div>');
        }
    }   
?>