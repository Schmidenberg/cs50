<form action="index.php" method="get">
<br/>
<div style="height:750px" id="questionDiv">
    <h1 id="congrats">
        <p><?= htmlspecialchars($header) ?></p>
    </h1>
    <br/>
    <h3>
        <p><?= htmlspecialchars($message) ?></p>
    </h3>
    <br/>
    <?php
        // auto redirect after 30 seconds
        header( "refresh:30;url=$url");
    ?>
    <a href=<?= htmlspecialchars($url)?>><strong><?= htmlspecialchars($linktext)?></strong></a>
</div>
<br/>
    
</form>
    </fieldset>
</form>
<br/>
