<form id="quizForm" action="questions.php" method="post">
    <fieldset>
        <h1 class="quizHeader">QUIZ TIME!</h1>
        <h2 class="quizNote">You have been selected to take a quiz. You may only continue to the quiz page - Once the quiz is 
        complete, you may return to this site to view the Training Material. Please confirm that you understand the
        following rules before continuing.
        <br/>
        </h2>
        <br/>
        <div class="form-group">
            </br>
            <h3 id = "rules1">Rule One:</h3>
            <h4 id = "rules">The quiz will begin once you accept these terms and click "Take Test" below</h4>
            <input type="checkbox" name="rule1" value="1" id="ticked1"> 
                <label for="ticked1">Tick to confirm that you understand the first rule<br></label>
        </div>
        <div class="form-group">
            </br>
            <h3 id = "rules2">Rule Two:</h3>
            <h4 id = "rules">The quiz will begin when you click "Take Test" below, you will have 30 minutes in which to finish it - no extensions will be granted</h4>
            <input type="checkbox" name="rule2" value="2" id="ticked2"> 
                <label for="ticked2">Tick to confirm that you understand the second rule<br></label>
        </div>
        <div class="form-group">
            </br>
            <h3 id = "rules3">Rule Three:</h3>
            <h4 id = "rules">You may not open additional browser tabs, change focus to a different tab, print screen, or open/change focus to other applications while taking the quiz</h4>
            <input type="checkbox" name="rule3" value="3" id="ticked3">
                <label for="ticked3">Tick to confirm that you understand the third rule<br></label>
        </div>
        <br/>
        <div class="form-group">
            </br>
            <h3 id = "rules4">Rule Four:</h3>
            <h4 id = "rules">Once in the quiz, be sure to be satisfied with all question selections <b>before</b> clicking "Submit" - there is no turning back once you click the button</h4>
            <input type="checkbox" name="rule4" value="4" id="ticked4">
                <label for="ticked4">Tick to confirm that you understand the fourth rule<br></label>
        </div>
        <br/>
        <div class="form-group">
            <br/>
            <h3 class="quizNote">Failure to obey these rules will result in the immediate termination of the quiz and neccessary action will be taken by management.
            </h2>
        </div>
          <div class="form-group">
            </br>
            
            <button class="btn btn-default" name="btnSubmit" type="submit">
                Take Test
            </button>
        </div>
    </fieldset>
</form>
