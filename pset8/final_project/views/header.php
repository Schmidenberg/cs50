<!DOCTYPE html>

<html>

    <head class="wordShadow" class="header">
    <header class="nav-down">
        <!-- http://getbootstrap.com/ -->
        <link href="/css/bootstrap.min.css" rel="stylesheet"/>

        <link href="/css/styles.css?version=7" type="text/css" rel="stylesheet"/>
        
        <?php 
            browser_title($title);
        ?>
        
        <!-- https://jquery.com/ -->
        <script src="/js/jquery-1.11.3.min.js"></script>

        <!-- http://getbootstrap.com/ -->
        <script src="/js/bootstrap.min.js"></script>
        
        <!-- my js -->
        <script src="/js/scripts.js?=7"></script>
        
        <!-- Google Charts -->
        <script src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">google.charts.load('current', {packages: ['corechart', 'table']});</script>

        <!-- google fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Arima+Madurai" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Taviraj" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">
        
        <div id="msgWrapper" class="msgWrapClass">
            <div id="leftBanner">
                <?php
                    if (!empty($_SESSION["id"]) && isset($_SESSION["id"]))
                            check_otp($_SESSION["id"]); 
                    
                    // populate the appropriate nav tabs based on user privileges
                    insert_primary_tabs();

                ?>
            </div>
            <div id="centerBanner">
                <span id="msgBanner" class="msgBannerEmpty" class="unstyled"></span>
            </div>
            <div id="rightLogin">
                <?php 
                    // welcome message in nav bar - top right, if logged in && !in quiz
                    logged_in_user_details();
                ?>
            </div>
        </div>
            <?php
                // add pop-up to trining page to revert page position
                add_training_popup()
            ?>
    </header>
        <headerRow></headerRow>
    </head>
    <body class="wordShadow">
        <div class="background">
            <div class="container">
                <div id="middle">

 