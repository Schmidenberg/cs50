                    <div id="reporting">
                        <h2 class="trainingHeader mainHeader">Reports</h2>
                        
                        <?php
                            // populate page according to privilege
                            // populate sub-header according to type
                            $userType = $_SESSION["user_type"];
                            if ($userType == "Administrator")
                                echo("<h3 class=\"trainingHeader\">As an Administrator you are able to view: All Reports, all companies, all divisions</h3>");
                            elseif ($userType == "Branch Manager")
                                echo("<h3 class=\"trainingHeader\">As a Branch Manager you are able to view: All Reports within your Company, all divisions</h3>");
                            else
                                echo("<h3 class=\"trainingHeader\">As a Team Leader you are able to view: Your Division Reports for Team Leaders and Consultants</h3>");
                                ?>
                            <?php
                            // activate all reports then populate as necessary below per user_type
                            if ($userType == "Administrator" || $userType == "Branch Manager" || $userType == "Team Leader")
                            {
                            ?>
                            <div id="reportPage">
                                <div id="passRate" style="display: none;">
                                    <?php
                                        // calculate average for 'Correct % Per Company - Based On Latest Results'
                                        // $reportDetails - 'collectCorrect/(questionsAsked * usersInCompany)'
                                        $usersTakenQuiz = $reportDetails["usersTakenQuiz"];
                                        $questionsAsked = $reportDetails["questionsAsked"];
                                        $collectCorrect = $reportDetails["collectCorrect"];
                                        $passRate = $reportDetails["passRate"];
                                        $passRateHeader = "All Companies";
                                        $fullComplement =  $reportDetails["fullStaffComplement"];
                                        $disquailified = $reportDetails["disquailfied_users"];
                                        
                                        // load each value into JSON array for Java
                                        $passFail = [];
                                        $pass_count = $taken_count = $disq_count = 0;
                                        foreach($fullComplement as $com=>$comVal)
                                        {
                                            foreach($passRate as $pass=>$passVal)
                                            {
                                                if ($com == $pass)
                                                {
                                                    $pass_count = $passVal;
                                                }
                                            }
                                            foreach($usersTakenQuiz as $taken=>$takenVal)
                                            {
                                                if ($com == $taken)
                                                {
                                                    $taken_count = $takenVal;
                                                }
                                            }
                                            foreach($disquailified as $disq=>$disqVal)
                                            {
                                                if ($com == $disq)
                                                {
                                                    $disq_count = $disqVal;
                                                }
                                            }
                                            $passFail[] = [
                                                        // change "User" to "Users" if user count > 1
                                                        "company" => $com . "\n(" . $comVal . ($comVal == 1 ? " User in Total)" : " Users in Total)"),
                                                        "noResult" => $comVal - $taken_count,
                                                        "annotationNoResult" => (($comVal - $taken_count)/$comVal * 100),
                                                        "disqualified" => $disq_count,
                                                        "annotationDisqualified" => ($disq_count/$comVal)*100,
                                                        "passed" => $pass_count,
                                                        "annotationPassed"=> ($pass_count/$comVal)*100,
                                                        "fails"=> $taken_count - $pass_count - $disqVal,
                                                        "annotationFailed"=>  (($taken_count - $pass_count - $disq_count)/$comVal) * 100
                                                    ];
                                        }
                                        // print the encoded result, in hidden div, for JavaScript to "pick-up"
                                        print_r(json_encode($passFail));
                                   ?>
                                </div>
                                    <div id="passRateHeader" style="display: none;">
                                        <?php
                                            // provide header for report
                                            print($passRateHeader);    
                                        ?>
                                    </div>
                                <div id="divisionPassFail" style="display: none; index: 0;">
                                    <?php
                                        // calculate pass fail per division by company reports
                                        if (isset($reportDetails["passRate_div"]))
                                            $passRate_div = $reportDetails["passRate_div"];
                                        else
                                            $passRate_div = NULL;
                                        
                                        if (isset($reportDetails["failRate_div"]))
                                            $failRate_div = $reportDetails["failRate_div"];
                                        else
                                            $failRate_div = NULL;
                                            
                                        // print the division JSON in the dom for JS
                                        createJsonforJS($passRate_div, $failRate_div, $usersTakenQuiz);
                                    ?>
                                </div>
                                <div id="divisionPassFailHeader"  style="display: none;">
                                    <?php
                                        // provide division header for report
                                        $divisionPassHeader = "";
                                        print_r(json_encode($divisionPassHeader));    
                                    ?>
                                </div>
                                <?php
                                if ($userType == "Administrator" || $userType == "Branch Manager" ) 
                                {
                                ?>
                                <div id="BMPassFail" style="display: none; index: 0;">
                                    <?php
                                        // calculate pass fail per BM by company reports
                                        if (isset($reportDetails["passRate_bm"]))
                                            $passRate_bm = $reportDetails["passRate_bm"];
                                        else
                                            $passRate_bm = NULL;
                                        
                                        if (isset($reportDetails["failRate_bm"]))
                                            $failRate_bm = $reportDetails["failRate_bm"];
                                        else
                                            $failRate_bm = NULL;
                                        
                                        // print branch manager the JSON in the dom for JS
                                        createJsonforJS($passRate_bm, $failRate_bm, $usersTakenQuiz);
                                    ?>
                               </div>
                                <div id="BMPassFailHeader"  style="display: none;">
                                    <?php
                                        // provide bm header for report
                                        $BMPassHeader = "";
                                        print_r(json_encode($BMPassHeader));    
                                    ?>
                                </div>
                                <?php
                                }
                                ?>
                                <div id="TLPassFail" style="display: none; index: 0;">
                                    <?php
                                        // calculate pass fail per TL by company reports
                                        if (isset($reportDetails["passRate_tl"]))
                                            $passRate_tl = $reportDetails["passRate_tl"];
                                        else
                                            $passRate_tl = NULL;
                                        
                                        if (isset($reportDetails["failRate_tl"]))
                                            $failRate_tl = $reportDetails["failRate_tl"];
                                        else
                                            $failRate_tl = NULL;
                                        
                                        // print the team leader JSON in the dom for JS
                                        createJsonforJS($passRate_tl, $failRate_tl, $usersTakenQuiz);
                                    ?>
                               </div>
                                <div id="TLPassFailHeader"  style="display: none;">
                                    <?php
                                        // provide tl header for report
                                        $TLPassHeader = "";
                                        print_r(json_encode($TLPassHeader));    
                                    ?>
                                </div>
                                <div id="usersTable" style="display: none; index: 0;">
                                    <?php
                                        // calculate pass fail per TL by company reports
                                        if (isset($userDetails))
                                            $users = $userDetails;
                                        else
                                            $users = NULL;
                                            
                                        // print the team leader JSON in the dom for JS
                                        print_r(json_encode($users));
                                    ?>
                               </div>
                                <div id="usersTableHeader"  style="display: none;">
                                    <?php
                                        // provide tl header for report
                                        $TLPassHeader = "Latest Results - User Pass Rate (" . PASS_RATE . "%) by Team Leader - ";
                                        print_r(json_encode($TLPassHeader));    
                                    ?>
                                </div>
                                    <?php
                                    // populate the BM report div for Administrators only
                                    if ($userType == "Administrator") 
                                    {
                                    ?>
                                    <h4 class="chartsHeader">Pass Rates (<?php echo(PASS_RATE) ?>%) by Company - Last Quiz Results Only</h4>
                                    <div id="chartContainer" class="chartBorder"><h3>Populating Report Data...</h3></div>
                                    <br/>
                                    <?php
                                    }
                                    ?>
                                    <h4 class="chartsHeader">Division Pass Rate (<?php echo(PASS_RATE) ?>%) by Company - Last Quiz Results Only</h4>
                                    <table id="divisionTable" class="columns">
                                    </table> 
                                    <br/>
                                    <?php
                                    // populate the BM report div for Administrators and BMs only
                                    if ($userType == "Administrator" || $userType == "Branch Manager" ) 
                                    {
                                    ?>
                                    <h4 class="chartsHeader">User Pass Rates (<?php echo(PASS_RATE) ?>%) by Branch Manager - Last Quiz Results Only</h4>
                                    <table id="branchManagerTable" class="columns">
                                    </table> 
                                    <br/>
                                    <?php
                                    }
                                    ?>
                                    <h4 class="chartsHeader">User Pass Rates (<?php echo(PASS_RATE) ?>%) by Team Leader - Last Quiz Results Only</h4>
                                    <table id="teamLeaderTable" class="columns">
                                    </table> 
                                    <br/>
                                    <div  class="chartsHeader">
                                    <h4>All Users - Last Quiz Results Only <br/><h5 class="chartsHeaderSmall">(select a user to view their detailed quiz results)<br/>(use CTRL+click to select multiple rows)</h5></h4>
                                    </div>
                                    <div id="user_table_div"  class="usersTable"></div>
                                    <br/>
                                <?php
                                }
                                ?>
                                </div>
                            </div>
                        </div>
                        