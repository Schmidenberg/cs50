<form id="request_reset_form" action="request_reset.php" method="post">
        <div id="thirdIn">
            <h5 style="font-size: 30px;" class="trainingHeader">
                the training site limits access to valid email addresses & usernames
                <br/>
                <br/>
                contact Operations if you are unsure which details to use
            </h5>
        </div>
        <div id="fourthIn">
            <h5 class="trainingHeader">
                provide your username or email address for verfication
            </h5>
            <br/>
        <fieldset style="font-family: Helvetica;">
            <div class="form-group">
                <input autocomplete="off" autofocus class="form-control" name="details" id="request_reset_details" placeholder="Enter Email or Username" style="width: 350px" type="text"/>
            </div>
            <br/>
            <button class="btn btn-default" type="submit">
                <span aria-hidden="true" class="glyphicon glyphicon-envelope"></span>
                 Request Reset Email
            </button>
        </fieldset>
    </form>
    <br/>
    <div style="font-family: Helvetica; font-size: 14px; color: rgba(0,0,0, .5); text-shadow: 1px 1px 1px rgba(0,131,255,0);">
        or click <a href="index.php">here</a> to log into your account
    </div>
</div>
