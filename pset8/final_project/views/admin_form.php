<div id="userAdministration">
<h2 class="trainingHeader mainHeader">User Administration </h2>                        
<?php
    // populate page according to privilege
    // populate sub-header according to type
    $userType = $_SESSION["user_type"];
    if ($userType == "Administrator")
        print("<h3 class=\"trainingHeader\">As an \"Administrator\" you are able to view: All Reports, all companies, all divisions</h3>");
    elseif ($userType == "Branch Manager")
        print("<h3 class=\"trainingHeader\">As a \"Branch Manager\" you are able to view: All Reports within your Company, all divisions</h3>");
    else
        print("<h3 class=\"trainingHeader\">As a \"Team Leader\" you are able to view: Your Division Reports for Team Leaders and Consultants</h3>");
        ?>
    <?php
    // activate all reports then populate as necessary below per user_type
    if ($userType == "Administrator" || $userType == "Branch Manager" || $userType == "Team Leader")
    {
    ?>
        <?php
        // when POST is submitted this message is fired in JS (msgAlert())
        if ($is_post != NULL)
            print('<div id="adminDataStored" style="display: none;">The data has been stored</div>');
        ?>
        <div class="chartsHeader">
        <h4>User Administration<br/></h4>
        </div>
        <form id="admin_form" action="admin.php" method="post">
            <?php
            // print users per company
            foreach($companyCount as $company)
            {
                $_SESSION["previous_page"] = $_SERVER["REQUEST_URI"];

                $this_company = str_replace(' ', '', $company);
                // print the table
                print('<table id="userAdminTable' . $this_company . '" class="table adminTable table-striped">');
                // print the table header (company + Submit Button) , unless TL
                if ($userType != "Team Leader")
                {
                    print("<tr class='userAdminHeader'>");
                    print("<th class='chartsHeader company" . $this_company  . "'" ."colspan='12'><h4><input type='Submit' class='btn btn-default' id='q_submit' value='Submit All Changes'>" . "  -  " . $company . "</h4></th>");
                    print("</tr>");
                    // print the header row
                    print("<tr>");
                    print("<th><strong>Count</strong></th>");
                    print("<th><a class='admin_a' href='admin.php?sort=name_" . $direction . "'><strong>Name </strong><i class='name'></i></th>");
                    print("<th><a class='admin_a' href='admin.php?sort=lastname_" . $direction . "'><strong>Last Name </strong><i class='lastname'></i></th>");
                    print("<th><a class='admin_a' href='admin.php?sort=flagged_" . $direction . "'><strong>Currently Flagged For Quiz <i></i><br/><strong style='font-size: 80%;'>Check All: 
                        <input type='checkbox' class='admin_cbox company_cbox' id='" . $this_company . "' value='" . $this_company . "_checkbox'></strong><i class='flagged'></i></th>");
                }
                // print for TLs - no checkboxes
                else
                {
                    print("<tr class='userAdminHeader'>");
                    print("<th class='chartsHeader company" . $this_company  . "'" ."colspan='12'><h4>" . $company . "</h4></th>");
                    print("</tr>");
                    // print the header row
                    print("<tr>");
                    print("<th><strong>Count</strong></th>");
                    print("<th><a class='admin_a' href='admin.php?sort=name_" . $direction . "'><strong>Name </strong><i class='name'></i></th>");
                    print("<th><a class='admin_a' href='admin.php?sort=lastname_" . $direction . "'><strong>Last Name </strong><i class='lastname'></i></th>");
                    print("<th><a class='admin_a' href='admin.php?sort=flagged_" . $direction . "'><strong>Currently Flagged For Quiz <i class='flagged'></i><br/></th>");
                }
                print("<th><a class='admin_a' href='admin.php?sort=agent_" . $direction . "'><strong>Flag Status Agent </strong><i class='agent'></i></th>");
                print("<th><a class='admin_a' href='admin.php?sort=division_" . $direction . "'><strong>Division </strong><i class='division'></i></th>");
                print("<th><a class='admin_a' href='admin.php?sort=bm_" . $direction . "'><strong>Branch Manager </strong><i class='bm'></i></th>");
                print("<th><a class='admin_a' href='admin.php?sort=tl_" . $direction . "'><strong>Team Leader </strong><i class='tl'></i></th>");
                print("<th><a class='admin_a' href='admin.php?sort=date_" . $direction . "'><strong>Last Quiz Date </strong><i class='date'></i></th>");
                print("<th><a class='admin_a' href='admin.php?sort=result_" . $direction . "'><strong>Last Quiz Result <i class='result'></i><br/><strong style='font-size: 80%;'>(Pass Rate " . PASS_RATE . "%)</strong></th>");
                print("</tr>");
                
                // count for column 'count'
                $i = 1;
                foreach($userDetails as $user=>$userVal)
                {
                    if ($userVal["company"] === $company)
                    {
                        
                        // row count
                        print("<tr>");
                            print("<td>" . $i . "</td>");
                            print("<td>" . $userVal["name"] . "</td>");
                            print("<td>" . $userVal["last_name"] . "</td>");
                            // only print checkboxes for Admin and BM
                            if ($userType != "Team Leader")
                            {
                                if ($userVal["otp_active"] == 1)
                                {
                                    // row with checkbox and attributes
                                    print("<td>" . 'Yes - ' . '<input class="admin_cbox cbox_' . $this_company . '"name="yes_' . $userVal["id"] . '" type="checkbox" id="cbox' . $userVal["id"] . '" value="' . $userVal["id"] . '_checkbox">' . 
                                    // break and html text
                                    '<br/>' . '<strong style="font-size: 80%;">(check to turn off quiz)</strong>' . "</td>");
                                }
                                else
                                {
                                    // row with checkbox and attributes
                                    print("<td>" . 'No - ' . '<input class="admin_cbox cbox_' . $this_company . '"name="no_' . $userVal["id"] . '" type="checkbox" id="cbox' . $userVal["id"] . '" value="' . $userVal["id"] . '_checkbox">' . 
                                    // break and html text
                                    '<br/>' . '<strong style="font-size: 80%;">(check to turn on quiz)</strong>' . "</td>");     
                                }
                            }
                            else
                            {
                                // for TL do not print checkboxes
                                if ($userVal["otp_active"] == 1)
                                    print("<td>" . 'Yes' . "</td>");
                                else
                                    print("<td>" . 'Yes' . "</td>");
                            }
                            // if otp details for agent and date set, if populated
                            if (!empty($userVal["otp_agent"] && $userVal["otp_date"] != '0000-00-00 00:00:00'))
                                print("<td>" . $userVal["otp_agent"] . "<br/><strong style='font-size: 80%;'>" . $userVal["otp_date"] . "</td>");
                            else
                                print("<td>" . "N/A" . "</td>");
                                
                            print("<td>" . $userVal["division"] . "</td>");
                            print("<td>" . $userVal["BM"] . "</td>");
                            print("<td>" . $userVal["TL"] . "</td>");
                            print("<td>" . $userVal["last_test_date"] . "</td>");
                            
                            // if no answers in results, check why - disqualified or not taken test
                            if (empty($userVal["illegal_move"]) && empty($userVal["q_results"]))
                                print("<td>No Results</td>");
                            else if ($userVal["illegal_move"] === '3 Events: Tab focus lost' || $userVal["illegal_move"] === 'Page Refresh')
                                print("<td>User Disqualified<br/><strong style='font-size: 80%;'>" . $userVal["illegal_move"] . "</strong></td>");
                            // else, calculate if last result was pass or fail
                            else if (($userVal["q_correct_count"] / $userVal["q_asked_count"] * 100) < PASS_RATE)
                                print("<td>Fail: " . (($userVal["q_correct_count"] / $userVal["q_asked_count"]) * 100) . "%</td>");
                            else
                                print("<td>Pass: " . (($userVal["q_correct_count"] / $userVal["q_asked_count"]) * 100) . "%</td>");
                        print("</tr>");
                        $i++;
                    }
                }
                print('</form>');
            }
            print('</table>');
        }
        ?>
        
</div>