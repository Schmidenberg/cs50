<div id="training">
    <h2 class="trainingHeader mainHeader">Training Manuals</h2>
    
    <?php
        // populate page according to privilege
        // populate sub-header according to type
        $userType = $_SESSION["user_type"];
        if ($userType == "Administrator")
            echo("<h3 class=\"trainingHeader\">As an Administrator you are able to view: all manuals, all companies, all divisions</h3>");
        elseif ($userType == "Branch Manager")
            echo("<h3 class=\"trainingHeader\">As a Branch Manager you are able to view: all manuals within your company, all divisions</h3>");
        elseif ($userType == "Team Leader")
            echo("<h3 class=\"trainingHeader\">As a Team Leader you are able to view: Your division manuals for Team Leaders and Consultants</h3>");
        else
            echo("<h3 class=\"trainingHeader\">As a Consultant you may only view: Your division & Role manuals</h3>");
        
        // local variables 
        $countCompanies = count($groupingToUse);

        // echo type-specific data for user and populate tabs
        echo ("<ul class=\"tablinks\"><li id=\"acc\" style=\"top: 326px\"><span class=\"panel\">");
        for ($i = 0; $i < $countCompanies; $i++) // loop companies
        {
            $countSubArrays = count ($groupingToUse[$i]);
            // store current company and sub tab for manual name for image insert
            $insertManual = "";
            for ($n = 0; $n < $countSubArrays; $n++) // loop all possible sub arrays for groupings
            {
                $cssGroup = ""; // used to apply css for tab collapse
                if ($n == 0) // 0 is always the company description
                {
                    // print 1st lvl of tabs (companies)
                    $liComp =  $groupingToUse[$i][$n];
                    $cssGroup = str_replace (" ", "", $liComp);
                    $liStr = "<a data-parent=\"#acc\" ";
                    // first element gets "clicked" by js
                    if ($i == 0) 
                    {
                        $liStr = $liStr . "class=\"openFirst panel-collapse subTabsOne collapse\"";
                    }
                    $liStr  = $liStr . " class=\"subTabsOne\" data-toggle=\"collapse\" href=\"#" . $cssGroup . "\">";
                    
                    // possibilites based on user type. "Admins" view all companies, for example
                    echo ($liStr . $liComp);
                    echo ("</a>");
                    echo ("<div id=\"" . $cssGroup . "\" class=\"panel-collapse subTabsTwo collapse\">");
                    $insertComp = "$liComp";
                }
                else
                {
                    // print remaining tabs (sub tabs)
                    if ($groupingToUse[$i][$n] != NULL)
                    {
                        // print 1st lvl of tabs (companies)
                        if (array_key_exists("division", $groupingToUse[$i][$n]))
                        {
                            if (!empty($groupingToUse[$i][$n]["division"]) || ($groupingToUse[$i][$n]["division"]) != "")
                            $liComp =  $groupingToUse[$i][$n]["division"];
                            else
                            continue; // if "division" is not populated - skip the creation of that tab
                        }
                        // create bm tabs, if applicable
                        elseif (array_key_exists("user_type", $groupingToUse[$i][$n]))
                        {
                            if (!empty($groupingToUse[$i][$n]["user_type"]) || ($groupingToUse[$i][$n]["user_type"]) != "")
                            $liComp =  $groupingToUse[$i][$n]["user_type"];
                            else
                            continue;
                        }
                        $liStr = "<a style=\"z-index:0\" class=\"manualImage\"";
                        if ($i == 0) 
                        {
                            $liStr = $liStr . "";
                        }
                        $insertManual = $insertComp . $liComp;
                        $insertManual = str_replace (" ", "", $insertManual . ".jpg?v=1"); // adjust version to force refresh of image
                        $liStr  = $liStr . " onclick=\"insertImage('$insertManual')\">";
                        echo ($liStr . $liComp);
                        echo ("</a>");
                    }
                }
            }
            echo("</div>");
        }
       echo("</li></ul></br><div class=\"imageIns\" id=\"imageInsert\"></div>");
    ?>
