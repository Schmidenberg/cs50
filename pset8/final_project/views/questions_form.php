<form id="questionsForm" action="results.php" method="post">
    <br/>
                <?php
                $_SESSION["questions_asked"] = QUESTION_COUNT;
                 
                $n = 0;
                foreach ($questions as $question)
                {
                    // displays the stem (question) from q_id
                    echo("<div id=\"mChoiceQ\"><h3>" . $question["stem"] . "</div></h3><div class=\"mChoiceOpt\">");
                    for ($i = 0; $i < 5; $i++)
                    {
                        // ASCII value of i variable for m-choice letter
                        $char = chr($i + 65);
                        // populates each question set and groups them - while preserving q_id for submission
                        // result e.g.: <input type="radio" id="331D)" name="answer30" value="331,q_key - 330"><label for="331D)">D) q_key - 330</label>
                        // the above example: "q_key - 330" is the potential answer, name is the grouping for this radio set, label allows click of word
                        // value is required for submittion, plus q_id for results
                        echo("<br><h4><input type=\"radio\" id=\"" . $question["q_id"] . $char . ")" . "\" name=\"answer$n\" value=\"" . $question["q_id"] . "," . $question["option" . $char] 
                        . "\"><label for=" . $question["q_id"] . $char . ")>" . $char . ") " . $question["option" . $char] . "</label></h4>");
                    }
                    echo("<br></div><br>");
                    $n++;
                }
                echo("</div>");
                
                ?>
<br/>
<div id="questionDiv">
    <h3>
        Ensure that you are satifisied with all of your selections before clicking "submit"
    </h3>
</div>
<br/>
    <input type="Submit" class="btn btn-default" id="q_submit" value="Submit">
    <br/>
</form>
    </fieldset>
</form>
<br/>
