<form id="loginForm" action="index.php" method="post">
    <fieldset>
        <div id="firstIn">
            <h2>
                regardless of the industry, for businesses to thrive and compete successfully in 
                today's ever-changing corporate arena, it is imperative to invest in employee training and development
        </div>
        <div class="form-group" >
            <input autocomplete="off" autofocus class="form-control" id="username" name="username" placeholder="Username" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="password" id="password" placeholder="Password or OTP" type="password"/>
        </div>
        <div class="form-group">
            <button class="btn btn-default" type="dsubmit">
                <span aria-hidden="true" class="glyphicon glyphicon-log-in"></span>
                Log In
            </button>
        </div>
        <br/>
            <div class="loginText">
                or <a href="request_reset.php">reset</a> your account password
            </div>
        <br/>
            <div id="secondIn">
                <h2>
                    Continuous Training Empowers Employees. 
                </h2><br/><h3> 
                    it provides the confidence that they are abreast of the new developments and have a stronger understanding of the industry. 
                    this confidence pushes them to perform better and think of new ideas to excel. increase in employee performance leads to increase in organizational productivity. 
                    a team of competent and knowledgeable employees is all that a company needs to compete successfully and hold a strong position in the industry.                
                </h3>
                <h5 class="loginText" style="text-align: right;">
                    - blog.schoox.com
                </h5>
            </div>
    </fieldset>
</form>
