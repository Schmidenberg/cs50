<form id="reset_password_form" action="reset_password.php" method="post">
        <div id="fifthIn">
            <h5 style="font-size: 30px;" class="trainingHeader">
                Password Creation
                <br/>
            </h5>
        </div>
        <br/>
        <div id="sixthIn">
            <h5 class="trainingHeader">
                Provide your desired password:
            </h5>
        <fieldset style="font-family: Helvetica;">
            <div class="form-group">
                <input autocomplete="off" autofocus class="form-control" name="password" id="pwd_reset_form" placeholder="Password" type="text"/>
            </div>
        </fieldset>
        </div>
        <div id="seventhIn">
            <h5 class="trainingHeader">
                Confirm your password:
            </h5>
        <fieldset style="font-family: Helvetica;">
            <div class="form-group">
                <input autocomplete="off" autofocus class="form-control" name="confirm" id="pwd_confirm_form" placeholder="Confirmation" type="text"/>
            </div>
            <br/>
            <br/>
            <button class="btn btn-default" type="submit">
                <span aria-hidden="true" class="glyphicon glyphicon-link"></span>
                 Update & Log In
            </button>
        </fieldset>
        </div>
    </form>
<br/>

    
