<?php
// configuration
require("../includes/config.php");

// if user reached page via GET (as by clicking a link or via redirect)
if ($_SERVER["REQUEST_METHOD"] == "GET")
{
    // go to login page
    logout();
    render("login_form.php", ["title" => "Staff Training"]);
}
// else if user reached page via POST (as by submitting a form via POST)
else if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    render("quiz_form.php", ["title" => "Quiz Prep"]);
}
?>