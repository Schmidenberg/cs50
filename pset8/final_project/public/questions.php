<?php
// configuration
require("../includes/config.php");

// if user reached page via GET (as by clicking a link or via redirect)
if ($_SERVER["REQUEST_METHOD"] == "GET")
{
    // illegal, return to login page
    logout();
    render("login_form.php", ["title" => "Staff Training"]);
}
// else if user reached page via POST, the only method allowed
else if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // illegal page refresh results in a logout
    $pageRefresh = CS50::query("SELECT `otp_active` FROM users WHERE id = ?;", $_SESSION["id"]);
    
    if ($pageRefresh[0]["otp_active"] == 0)
    {
        illegal_move("Page Refresh");
    }
    
    // remove otp from user to prevent re submission of quiz
    $_SESSION["start_timer"] = date("Y-m-d h:i:s");
    /**
     * 
     * 
     * amend the code below to allow MJ to change OTP_Active when logged in
     * 
     * 
     * 
     **/
    if ($_SESSION["id"] != 22)
    {
        CS50::query("UPDATE `users` SET `otp_active`= 0 
             WHERE `id` = ?;", $_SESSION["id"]);
    }
    // populate string to include DEFINED question count and pass to query
    // questions pulled randomly & specific to user division and type only
    $str = "SELECT DISTINCT `q_id`, `questions`.`groupings`, `stem`, `distractor_one`, `distractor_two`, `distractor_three`, `distractor_four`, `q_key`
                            FROM  `questions` ,  `users`, `user_profile`
                            WHERE ((SELECT`user_profile`.`division` FROM `user_profile` WHERE `user_profile`.`id` = ?)  = `questions`.`groupings`)
							OR ((SELECT `user_profile`.`company` FROM `user_profile` WHERE `user_profile`.`id` = ?) = `questions`.`groupings`)
							OR ((SELECT `users`.`user_type` FROM `users` WHERE `users`.`id` = ?) = `questions`.`groupings`)
                            ORDER BY RAND((SELECT `users`.`last_login` FROM `users` WHERE `users`.`id` = ?))
                            LIMIT ";
    $str = $str . QUESTION_COUNT;
    $str = $str . ";";
    $allQuestions = CS50::query("$str", $_SESSION["id"], $_SESSION["id"], $_SESSION["id"], $_SESSION["id"]);
    
    if (empty($allQuestions))
    apologize("Question population failure, contact your administrator");
    
    $question = [];
    // store each stem etc as associative array
    foreach ($allQuestions as $allQuestion)
    {
        
        // shuffle answer options before sending - extra safty
        $optionArray = array("A","B","C","D","E");
        shuffle($optionArray);
        $question[] = [
            "q_id" => $allQuestion["q_id"],
            "stem" => $allQuestion["stem"],
            "option" . "$optionArray[0]" => $allQuestion["distractor_one"],
            "option" . "$optionArray[1]" => $allQuestion["distractor_two"],
            "option" . "$optionArray[2]" => $allQuestion["distractor_three"], 
            "option" . "$optionArray[3]" => $allQuestion["distractor_four"],
            "option" . "$optionArray[4]" => $allQuestion["q_key"]
            ];
    }
    render("questions_form.php", ["questions" => $question, "questions_asked" => QUESTION_COUNT, "title" => "Multiple Choice"]);
}
?>