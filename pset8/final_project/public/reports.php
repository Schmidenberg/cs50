<?php
    // configuration
    require("../includes/config.php");
    
// if user reached page via GET
if ($_SERVER["REQUEST_METHOD"] == "GET")
{
    // gather the possible sub-grouping for the current user
    $userDetails = CS50::query("SELECT * FROM `user_profile` WHERE `id` = ?;", $_SESSION["id"]);
    
    // use first (and only) row for the current user
    $userDetails = $userDetails[0];
    $userType = $_SESSION["user_type"];

    // error-check
    if (empty($userDetails) || empty($userType))
        apologize("Error in \"Username\", try again");
    elseif (empty($userDetails["company"]))
        apologize("Error in \"Username.Company\", try again");

    // if user is admin, show default report: date = last, company = all (seperated), report = overview;
    if ($userType == "Administrator")
    {
        $allCompanies = CS50::query("SELECT DISTINCT company FROM user_profile;");
    }
    // only display the respective users company
    elseif ($userType == "Branch Manager" || $userType == "Team Leader")
    {
        $allCompanies = CS50::query("SELECT company FROM user_profile WHERE id = ?;",  $_SESSION["id"]);
    }
    // then user is a consultant - deny access to reports
    else
    {
        apologize("Restricted Access");
    }

        // vars to store specific user and report details gathered from queries
        $user = [];
        $reportDetails = [];
        $previousCompany = NULL;
        $countCompany = 0;
        
        // run each company individually
        foreach ($allCompanies as $company)
        {
            $strComp = implode($company);
            
            // retrieve latest test results
            if ($userType == "Administrator")
            {
                // pull all users last quiz results
                // includes disqualified staff
                $allUserLatestResults = CS50::query("SELECT *
                                                    FROM results AS a
                                                    LEFT JOIN (SELECT `id` AS 'user_profile_id', `name`, `middle_name`, `last_name` FROM user_profile) AS x
                                                    ON a.id = x.user_profile_id
                                                    WHERE test_start = (
                                                        SELECT MAX(test_start)
                                                        FROM results AS b
                                                        WHERE a.id = b.id
                                                        AND a.company_cur = ?)
                                                    ORDER BY a.id;", $strComp);
                                                    
                // count all users in company to find user count who have not taken quiz
                $thisCompanyComplement = CS50::query("SELECT * FROM `user_profile` WHERE `company` = ?;", $strComp);
                
            }
            elseif ($userType == "Branch Manager" )
            {
                $allUserLatestResults = CS50::query("SELECT *
                                                    FROM results AS a
                                                    LEFT JOIN (SELECT `id` AS 'user_profile_id', `name`, `middle_name`, `last_name` FROM user_profile) AS x
                                                    ON a.id = x.user_profile_id
                                                    WHERE test_start = (
                                                        SELECT MAX(test_start)
                                                        FROM results AS b
                                                        WHERE a.id = b.id
                                                        AND a.company_cur = ?)
                                                    	AND a.branch_manager_cur = ?
                                                    ORDER BY a.id;", $strComp, $userDetails["branch_manager"]);
                                                    
                $thisCompanyComplement = CS50::query("SELECT * FROM `user_profile` WHERE `company` = ?;", $strComp);
        
            }
            elseif ($userType == "Team Leader")
            {
                $allUserLatestResults = CS50::query("SELECT *
                                                    FROM results AS a
                                                    LEFT JOIN (SELECT `id` AS 'user_profile_id', `name`, `middle_name`, `last_name` FROM user_profile) AS x
                                                    ON a.id = x.user_profile_id
                                                    WHERE test_start = (
                                                        SELECT MAX(test_start)
                                                        FROM results AS b
                                                        WHERE a.id = b.id
                                                        AND a.company_cur = ?)
                                                    	AND a.team_leader_cur = ?
                                                    ORDER BY a.id;", $strComp, $userDetails["team_leader"]);
                                                    
                $thisCompanyComplement = CS50::query("SELECT * FROM `user_profile` WHERE `company` = ?;", $strComp);
        
            }
            // consolidate all details and json decode answers into one array per user                                  
            foreach ($allUserLatestResults as $userDetails)
            {
                $detailsToUse[] = [
                    "company" => $strComp,
                    "name" => $userDetails["name"],
                    "middle_name" => $userDetails["middle_name"],
                    "last_name" => $userDetails["last_name"],
                    "answers_id" => $userDetails["answers_id"],
                    "id" => $userDetails["id"],
                    "test_start" => $userDetails["test_start"],
                    "test_end" => $userDetails["test_end"],
                    "company_cur" => $userDetails["company_cur"],
                    "division_cur" => $userDetails["division_cur"],
                    "BM_cur" => $userDetails["branch_manager_cur"],
                    "TL_cur" => $userDetails["team_leader_cur"],
                    "city_cur" => $userDetails["city_cur"],
                    "location_cur" => $userDetails["location_cur"],
                    "grouping_cur" => $userDetails["grouping_cur"],
                    "questionsAnswered" => count(json_decode($userDetails["q_results"])),
                    "userAnswers" => json_decode($userDetails["q_results"], true),
                    "illegal_move" => $userDetails["illegal_move"]
                ];
                // collect the keys for each question that user was presented with
                foreach ($detailsToUse as $eachUser)
                {
                    // used to store accumulated correct answers by company
                    if (!empty($eachUser["userAnswers"]))
                    {
                        $keys = get_answers($eachUser);
                    }
                    else
                    {
                        //if users answers are empty = (boycott, error, or a submit on illegal action) by user
                        $eachUser["userAnswers"] = 0;
                    }
                    
                    // add the q_key to the user answers[] for easier retrieval/comparison later
                    // while doing so, compare answers & count correct
                    $answers = $eachUser["userAnswers"];
                    $countAnswers = count($answers);
                    $correctCount = 0;
                    
                    for ($i = 0; $i < $countAnswers; $i++)
                    {
                        // if empty - user caused an illegal page refresh
                        if(empty($keys[$i]['q_key']) || !$keys[$i]['q_key'])
                        {
                           // $eachUser["userAnswers"][$i] = "";
                            $eachUser["answeredCorrectly"] = 0;
                        }
                        else
                        {
                            if ($keys[$i]['q_key'] == $answers[$i]['user_selected'])
                            {
                                $correctCount++;
                            }
                            $eachUser["userAnswers"][$i] = array_merge($keys[$i], $answers[$i]);
                            $eachUser["answeredCorrectly"] = $correctCount;
                        }
                    }
                    
                    // also while doing so, populate report details 
                    $c = array_count_values(array_column($detailsToUse, 'company'));
                    $reportDetails["usersTakenQuiz"] = $c;
                    // returns array of companies with values - so each company has a key of the company title, again...
                    $reportDetails["fullStaffComplement"][$userDetails["company_cur"]] = array_count_values(array_column($thisCompanyComplement, 'company'));
                    // merges an array of key->values->key->values into key-values only, fixes issue above
                    $reportDetails["fullStaffComplement"] = array_replace($reportDetails["fullStaffComplement"], $reportDetails["fullStaffComplement"][$userDetails["company_cur"]]);
                    $reportDetails["questionsAsked"] = QUESTION_COUNT;
                }
                // disqualified user count
                if (isset($eachUser["illegal_move"]) || array_key_exists("illegal_move", $eachUser))
                {
                    if(isset($reportDetails["disquailfied_users"][$userDetails["company_cur"]]))
                    {
                        if ($eachUser["illegal_move"] !== "Null")
                        {
                            $reportDetails["disquailfied_users"][$userDetails["company_cur"]] += 1;
                        }
                    }
                    else
                    {
                        if ($eachUser["illegal_move"] !== "Null")
                        {
                            $reportDetails["disquailfied_users"][$userDetails["company_cur"]] = 1;
                        }
                        else
                        {
                            $reportDetails["disquailfied_users"][$userDetails["company_cur"]] = 0;
                        }
                    }
                }
                // by company: calculate the number of users who passed (80%)
                $companyCorrectCount = checkPass($correctCount);
                if (isset($reportDetails["passRate"]) || array_key_exists("passRate", $reportDetails)) // runs faster with both checks
                {
                    if (isset($reportDetails["passRate"][$strComp]) || array_key_exists($strComp, $reportDetails["passRate"]))
                    {
                        $reportDetails["passRate"][$strComp] += $companyCorrectCount;
                    }
                    else
                    {
                        $reportDetails["passRate"][$strComp] = $companyCorrectCount;
                    }
                }
                else
                {
                    $reportDetails["passRate"][$strComp] = $companyCorrectCount;
                }
                // count correct answers per company
                if ($previousCompany)
                {
                    if ($previousCompany == $eachUser['company'])
                    {
                        $reportDetails["collectCorrect"][$previousCompany] = $countCompany;
                        $countCompany += $correctCount;
                        $previousCompany = $eachUser['company'];
                    }
                    else
                    {
                        $reportDetails["collectCorrect"][$previousCompany] = $countCompany;
                        $countCompany = $correctCount;
                        $previousCompany = $eachUser['company'];
                    }
                    $reportDetails["collectCorrect"][$previousCompany] = $countCompany;
                }
                else
                {
                    $previousCompany = $eachUser['company'];
                    $reportDetails["collectCorrect"][$previousCompany] = $countCompany;
                }
                
            /**
             * The madness below:
             * Chart arrays are checked and created or populated accordingly, with a count.
             * 
             * Used to apply chart groupings for each pass / fail within: BM, TL, Division etc.
             * Each array must have a complete set of avaialable groupings.
             * 
             * The issue faced: object enumeration through one array cross-checked it's groups
             * against the counterpart array, this provides a combined figure of all pass and 
             * fails in each group to the chart. 
             * However, if every user in a group either passed or failed, then the "0" count
             * counterpart group would not be populated at all. This caused an issue with the 
             * enumeration, becuase the group exists, it's just 0 - & a group that does not exist, 
             * must not be populated at all.
             *
             * e.g.: 
             * without counterpart:
             * {passedArray{groupOne: "1", groupTwo: "2"}, failedArray{groupOne: "1"}}
             * with counterpart:
             * {passedArray{groupOne: "1", groupTwo: "2"}, failedArray{groupOne: "1", groupTwo: "0"}}
             */
            
                // by division: calculate the number of users per div, include pass/fail stats
                if ($companyCorrectCount)
                {
                    // increment division count if user passed
                    if (isset($reportDetails["passRate_div"][$previousCompany][$eachUser["division_cur"]]))
                        $reportDetails["passRate_div"][$previousCompany][$eachUser["division_cur"]] += $companyCorrectCount;
                    else
                        $reportDetails["passRate_div"][$previousCompany][$eachUser["division_cur"]] = $companyCorrectCount;
                    // increment division count counterpart for passed
                    if (!isset($reportDetails["failRate_div"][$previousCompany][$eachUser["division_cur"]]))
                        $reportDetails["failRate_div"][$previousCompany][$eachUser["division_cur"]] = 0;
                    
                    // increment branch manager count if user passed
                    if (isset($reportDetails["passRate_bm"][$previousCompany][$eachUser["BM_cur"]]))
                        $reportDetails["passRate_bm"][$previousCompany][$eachUser["BM_cur"]] += $companyCorrectCount;
                    else
                        $reportDetails["passRate_bm"][$previousCompany][$eachUser["BM_cur"]] = $companyCorrectCount;
                    // increment branch manager count counterpart for passed
                    if (!isset($reportDetails["failRate_bm"][$previousCompany][$eachUser["BM_cur"]]))
                        $reportDetails["failRate_bm"][$previousCompany][$eachUser["BM_cur"]] = 0;
                        
                    // increment team leader count if user passed
                    if (isset($reportDetails["passRate_tl"][$previousCompany][$eachUser["TL_cur"]]))
                        $reportDetails["passRate_tl"][$previousCompany][$eachUser["TL_cur"]] += $companyCorrectCount;
                    else
                        $reportDetails["passRate_tl"][$previousCompany][$eachUser["TL_cur"]] = $companyCorrectCount;
                    // increment team leader count counterpart for passed
                    if (!isset($reportDetails["failRate_tl"][$previousCompany][$eachUser["TL_cur"]]))
                        $reportDetails["failRate_tl"][$previousCompany][$eachUser["TL_cur"]] = 0;
                        
                }
                else
                {
                    // increment division count if user failed
                    $companyCorrectCount = 1;
                    if (isset($reportDetails["failRate_div"][$previousCompany][$eachUser["division_cur"]]))
                        $reportDetails["failRate_div"][$previousCompany][$eachUser["division_cur"]] += $companyCorrectCount;
                    else
                        $reportDetails["failRate_div"][$previousCompany][$eachUser["division_cur"]] = $companyCorrectCount;
                    // increment division count counterpart for failed
                    if (!isset($reportDetails["passRate_div"][$previousCompany][$eachUser["division_cur"]]))
                        $reportDetails["passRate_div"][$previousCompany][$eachUser["division_cur"]] = 0;
                    
                    // increment branch manager count if user failed
                    if (isset($reportDetails["failRate_bm"][$previousCompany][$eachUser["BM_cur"]]))
                        $reportDetails["failRate_bm"][$previousCompany][$eachUser["BM_cur"]] += $companyCorrectCount;
                    else
                        $reportDetails["failRate_bm"][$previousCompany][$eachUser["BM_cur"]] = $companyCorrectCount;
                    // increment branch manager count counterpart for failed
                    if (!isset($reportDetails["passRate_bm"][$previousCompany][$eachUser["BM_cur"]]))
                        $reportDetails["passRate_bm"][$previousCompany][$eachUser["BM_cur"]] = 0;
                    
                    // increment team leader count if user failed
                    if (isset($reportDetails["failRate_tl"][$previousCompany][$eachUser["TL_cur"]]))
                        $reportDetails["failRate_tl"][$previousCompany][$eachUser["TL_cur"]] += $companyCorrectCount;
                    else
                        $reportDetails["failRate_tl"][$previousCompany][$eachUser["TL_cur"]] = $companyCorrectCount;
                    // increment team leader count counterpart for failed
                    if (!isset($reportDetails["passRate_tl"][$previousCompany][$eachUser["TL_cur"]]))
                        $reportDetails["passRate_tl"][$previousCompany][$eachUser["TL_cur"]] = 0;
                }
                $user[] = $eachUser;
            }
        }
        // error check empty array
        if (!isset($detailsToUse))
        {
            apologize("No user information to display (Your users have not taken a quiz, as yet)");
        }
        else
        {
            // sort recursively 
            sortArrayKeys($reportDetails);
            if (!isset($reportDetails["collectCorrect"]))
                $reportDetails["collectCorrect"] = [0];
                
            $companyCount = $reportDetails["collectCorrect"];
    
            $user["companyCount"] = array_keys($companyCount);
            sortArrayKeys($user);
            // render accordingly
            render("reports_form.php", ["reportDetails" => $reportDetails, "userDetails" => $user, "title" => "Reports"]);
        }
    }
?>