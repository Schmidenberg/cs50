<?php

    // configuration
    require("../includes/config.php"); 

    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        // render email form
        render("send_email.php", ["title" => "Password Changed"]);
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // validate submission
        if (empty($_POST["new_pwd"]) || empty($_POST["pwd_again"]))
        {
            apologize("You must complete all fields.");
        }
        
        else if ($_POST["new_pwd"] != ($_POST["pwd_again"]))
        {
            apologize("Your password and confirmation do not match.");
        }
        
        // check that old password is correct
        $rows = CS50::query("SELECT * FROM users WHERE id = ?;", $_SESSION["id"]);

        // if we found user, confirm old password
        if (count($rows) == 1)
        {
            // first (and only) row
            $row = $rows[0];
            $rows = CS50::query("UPDATE `users` SET `hash`= ? WHERE `id` = ?;", password_hash($_POST["new_pwd"], PASSWORD_DEFAULT), $_SESSION["id"]);
        }
        else
        {
            apologize("There was an error. Try it again, if you dare");
        }
        // redirect confirmation - preventing form resubmission
        $message = [$title = "Changed", $header = "Password Changed!", $msg = "Nice work"];

        $_SESSION["response"] = $message;
        redirect("/response.php");
    }
?>
