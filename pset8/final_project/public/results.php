<?php
// configuration
require("../includes/config.php");

// if user reached page via GET (as by clicking a link or via redirect)
if ($_SERVER["REQUEST_METHOD"] == "GET")
{
    // illegal, return to login page
    logout();
    render("results_form.php", ["title" => "Staff Training"]);
}
// else if user reached page via POST, the only method allowed
else if ($_SERVER["REQUEST_METHOD"] == "POST")
{

    $test_end = date("Y-m-d h:i:s");
    $results = [];
    
    foreach ($_POST as $answer) {
        $split = explode(",", $answer);
        $results[] = [
            "q_id" => $split[0],
            "user_selected" => $split[1]
        ];
    }

    // get answers for users questions 
    $answers[0]["userAnswers"] = $results;
    $get_answers = get_answers($answers[0]);
    
    // count questions answered
    $countAnswers = count($answers[0]["userAnswers"]);
    $correctCount = 0;
    
    // count correctly answered questions
    for ($i = 0; $i < $countAnswers; $i++)
    {
        if ($get_answers[$i]['q_key'] == $answers[0]["userAnswers"][$i]['user_selected'])
        {
            $correctCount++;
        }
    }
    $results = json_encode($results);
    
    // add results, questions answered count and correctly answered count to db
    CS50::query("INSERT INTO `results`
                (`id`, `test_start`, `test_end`, `illegal_move`, `company_cur`, `division_cur`, `branch_manager_cur`, `team_leader_cur`, `city_cur`, `location_cur`, `grouping_cur`, `q_results`, `q_asked_count`, `q_answered_count`, `q_correct_count`) 
                SELECT ?, ?, ?, 'Null', `company`, `division`, `branch_manager`, `team_leader`, `city`, `location`, `grouping`, ?, ?, ?, ?
                FROM `user_profile`
                WHERE `id` = ?;",
                $_SESSION["id"], $_SESSION["start_timer"], $test_end, $results, $_SESSION["questions_asked"], $countAnswers, $correctCount, $_SESSION["id"]);

    logout();
    
    // render response function appropriately 
    $url = "/index.php";
    $linktext = "Home Page";
    $header = "CONGRATULATIONS!";
    $message = "your answers have been submitted for review - your results will be disclosed in due course";
    $title = "Submitted";
    response($url, $linktext, $message, $header, $title);
}
?>