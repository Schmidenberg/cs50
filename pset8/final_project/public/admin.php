<?php
    // configuration
    require("../includes/config.php");
    
// if user reached page via GET or POST
if ($_SERVER["REQUEST_METHOD"] == "GET" || $_SERVER["REQUEST_METHOD"] == "POST")
{
    // grab the pervious page if the user has already sorted by field
    if (!empty($_SESSION["previous_page"]) && isset($_SESSION["previous_page"]))
    {
        $previous_page = $_SESSION["previous_page"];
        $current_page = $_SERVER["REQUEST_URI"];
    
        // determine the direction of the sort
        $direction = direction_for_sort($previous_page, $current_page);
    }
    
    $sort_by = '';
    // if GET
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        // check if user has sorted a column
        if (!empty($_GET["sort"]))
        {
            // regex security checks for sort - only allow the following from user
            $pattern = "/\b(name|lastname|flagged|division|bm|tl|date|result)\b/";
            $sort = htmlspecialchars($_GET["sort"]);
            $sort = explode('_', $sort);
            $sort_by = $sort[0];
            
            if (preg_match($pattern, $sort_by, $match))
            {
                // only allow lower alphas and underscore
                $sort_by = preg_replace("/[^a-z_]/", '', $sort_by);
                
                // switch for sorting by table field
                switch ($sort_by)
                {
                    case 'lastname':
                        $sort_by = 'last_name';
                        break;
                    case 'bm':
                        $sort_by = 'BM';
                        break;
                    case 'tl':
                        $sort_by = 'TL';
                        break;
                    case 'date':
                        $sort_by = 'last_test_date';
                        break;
                    case 'flagged':
                        $sort_by = 'otp_active';
                        break;
                    case 'result':
                        $sort_by = 'if_sort_by_result';
                        break;
                    case 'agent':
                        $sort_by = 'agent';
                        break;
                    default:
                        $sort_by;
                }
            }
        }
    }
    // if POST then form has been submitted - update checked users in DB, set OTP to 1
    $is_post = NULL;
    // stores date and name of BM or admin who updated quiz otp
    $date_and_name = [];
    
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // add post msg - activates div creation for JS to pickup and display message of successful post
        $is_post = 1;
        
        $checked_users = $_POST;
        
        // iterate through each element in POST array and enforce security
        foreach ($checked_users as $key => $value)
        {
            // regex security checks for update string
            $pattern = "/\b(yes|no)\b/";
            
            // seperate the "answer" from the id
            $key = explode("_", $key);
            $answer = $key[0];
            $id = $key[1];
            // ensure only 'yes' or 'no' is allowed in first element
            if (preg_match($pattern, $answer, $match))
            {
                // regex security check - accept numeric only in second element
                $id = preg_replace("/[^0-9]/", '', $id);
                
                // ensure special chars are escaped, if injected
                $answer = htmlspecialchars($answer);
                $id = htmlspecialchars($id);
                
                // ensure $id is int only
                $id = intval($id);
                
                // update DB with new value
                if (gettype($id) === 'integer')
                {
                    // user currently set to 'yes' - change to no
                    if ($answer === 'yes'){
                        CS50::query("UPDATE `users` SET `otp_active`= ? WHERE `id` = ?;", '0', $id);
                    }
                    // user currenlty set to 'no' - change to 'yes'
                    else if ($answer === 'no')
                        CS50::query("UPDATE `users` SET `otp_active`= ? WHERE `id` = ?;", '1', $id);
                }
            }
            // POST current user details to DB for otp change
            $current_name = CS50::query("SELECT `name`, `last_name` FROM `user_profile` WHERE `id` = ?;", $_SESSION["id"]);
            // one and only row
            $current_name = $current_name[0];
            // concat name and last name
            $current_name = $current_name["name"] . " " . $current_name["last_name"];
            $current_datetime = date("Y-m-d H:i:s");
            // insert data into DB, include user name and current date time
            if(!empty($current_name) && isset($current_name))
            {
                CS50::query("UPDATE `users` SET `otp_agent`= ?, `otp_date_set` = ? WHERE `id` = ?;", $current_name, $current_datetime, $id);
            }
            // gather db data otp data for current $_SESSION["id"]
            
        }
    }
    // if POST or GET
    // gather the possible sub-grouping for the current user
    $userDetails = CS50::query("SELECT * FROM `user_profile` WHERE `id` = ?;", $_SESSION["id"]);
    
    // use first (and only) row for the current user
    $userDetails = $userDetails[0];
    $userType = $_SESSION["user_type"];

    // error-check
    if (empty($userDetails) || empty($userType))
        apologize("Error in \"Username\", try again");
    elseif (empty($userDetails["company"]))
        apologize("Error in \"Username.Company\", try again");

    // if user is admin, show all users in all groupings
    if ($userType == "Administrator")
    {
        $allCompanies = CS50::query("SELECT DISTINCT company FROM user_profile;");
    }
    // only display the respective users company
    elseif ($userType == "Branch Manager" || $userType == "Team Leader")
    {
        $allCompanies = CS50::query("SELECT company FROM user_profile WHERE id = ?;",  $_SESSION["id"]);
    }
    // then user is a consultant - deny access to reports
    else
    {
        apologize("Restricted Access");
    }

        // vars to store specific user and report details gathered from queries
        $user = [];
        $usersDetails = [];
        $previousCompany = NULL;
        $countCompany = 0;
        
        // run each company individually
        foreach ($allCompanies as $company)
        {
            $strComp = implode($company);
            
            // users last test results, by users respective groupings
            if ($userType == "Administrator")
            {
                $allUserDetails = CS50::query("SELECT t1.id, t1.otp_active, t1.otp_agent, t1.otp_date_set,
                                                t2.name, t2.last_name, t2.company, t2.division, t2.branch_manager, t2.team_leader, 
                                                t3.test_start, t3.illegal_move, t3.q_results, t3.q_asked_count, t3.q_answered_count, t3.q_correct_count
                                                FROM users t1
                                                
                                                LEFT JOIN user_profile t2
                                                ON t1.id = t2.id  
                                                
                                                LEFT JOIN (SELECT *
                                                		   FROM results AS a
                                                		   LEFT JOIN (SELECT `id` AS 'users_id' FROM users) AS x
                                                		   ON a.id = x.users_id
                                                		   WHERE test_start = (
                                                 		      SELECT MAX(test_start)
                                                 		      FROM results AS b
                                                 		      WHERE a.id = b.id)) t3
                                                
                                                ON t1.id = t3.id 
                                                WHERE t2.company = ?;", $strComp);
            }
            elseif ($userType == "Branch Manager" )
            {
                $allUserDetails = CS50::query("SELECT t1.id, t1.otp_active, t1.otp_agent, t1.otp_date_set,
                                                t2.name, t2.last_name, t2.company, t2.division, t2.branch_manager, t2.team_leader, 
                                                t3.test_start, t3.illegal_move, t3.q_results, t3.q_asked_count, t3.q_answered_count, t3.q_correct_count
                                                FROM users t1
                                                
                                                LEFT JOIN user_profile t2
                                                ON t1.id = t2.id  
                                                
                                                LEFT JOIN (SELECT *
                                                		   FROM results AS a
                                                		   LEFT JOIN (SELECT `id` AS 'users_id' FROM users) AS x
                                                		   ON a.id = x.users_id
                                                		   WHERE test_start = (
                                                 		      SELECT MAX(test_start)
                                                 		      FROM results AS b
                                                 		      WHERE a.id = b.id)) t3
                                                
                                                ON t1.id = t3.id 
                                                
                                                WHERE t2.company = ?
                                                AND t2.branch_manager = ?;", $strComp, $userDetails["branch_manager"]);

            }
            elseif ($userType == "Team Leader")
            {
                $allUserDetails = CS50::query("SELECT t1.id, t1.otp_active, t1.otp_agent, t1.otp_date_set,
                                                t2.name, t2.last_name, t2.company, t2.division, t2.branch_manager, t2.team_leader, 
                                                t3.test_start, t3.illegal_move, t3.q_results, t3.q_asked_count, t3.q_answered_count, t3.q_correct_count
                                                FROM users t1
                                                
                                                LEFT JOIN user_profile t2
                                                ON t1.id = t2.id  
                                                
                                                LEFT JOIN (SELECT *
                                                		   FROM results AS a
                                                		   LEFT JOIN (SELECT `id` AS 'users_id' FROM users) AS x
                                                		   ON a.id = x.users_id
                                                		   WHERE test_start = (
                                                 		      SELECT MAX(test_start)
                                                 		      FROM results AS b
                                                 		      WHERE a.id = b.id)) t3
                                                
                                                ON t1.id = t3.id 
                                                
                                                WHERE t2.company = ?
                                                AND t2.team_leader = ?;", $strComp, $userDetails["team_leader"]);

            }
            // consolidate all details and json decode answers into one array per user                                  
            $companyCount = [];
            foreach ($allUserDetails as $userDetails)
            {
                // if sorted by result - add appropriate string to sort by
                if (empty($userDetails["illegal_move"]) && empty($userDetails["q_results"]))
                        $result = "No Results";
                else if ($userDetails["illegal_move"] === '3 Events: Tab focus lost' || $userDetails["illegal_move"] === 'Page Refresh')
                    $result = "User Disqualified";
                // else, calculate if last result was pass or fail
                else if (($userDetails["q_correct_count"] / $userDetails["q_asked_count"] * 100) < PASS_RATE)
                    $result = "Fail";
                else
                    $result = "Pass";
                    
                // if sorted by 'flag status' - add appropriate string to sort by
                if (empty($userDetails["otp_agent"]))
                    $agent = NULL;
                else
                    $agent = $userDetails["otp_agent"];
                
                // otherwise, just populate the ass. array
                $detailsToUse[] = [
                    "name" => $userDetails["name"],
                    "last_name" => $userDetails["last_name"],
                    "otp_active" => $userDetails["otp_active"],
                    "otp_agent" => $userDetails["otp_agent"],
                    "otp_date" => $userDetails["otp_date_set"],
                    "id" => $userDetails["id"],
                    "company" => $userDetails["company"],
                    "division" => $userDetails["division"],
                    "BM" => $userDetails["branch_manager"],
                    "TL" => $userDetails["team_leader"],
                    "last_test_date" => $userDetails["test_start"],
                    "q_results" => $userDetails["q_results"],
                    "q_asked_count" => $userDetails["q_asked_count"],
                    "q_answered_count" => $userDetails["q_answered_count"],
                    "illegal_move" => $userDetails["illegal_move"],
                    "q_correct_count" => $userDetails["q_correct_count"],
                    "if_sort_by_result" => $result,
                    "agent" => $agent
                ];
            }
            // helpers function - 3rd party - to sort by key
            if (!empty($sort_by)) {
                //print($direction);
                sort_by_key($sort_by, $detailsToUse, $direction);
            }
            else {
                // on first page load - sort by name as default
                asort($detailsToUse);
            }
            // add company to array for company count
            foreach($detailsToUse as $each)
            {
                if(!in_array($each["company"], $companyCount, true)){
                    array_push($companyCount, $each["company"]);
                }
            }
        }
        // error check empty array
        if (!isset($detailsToUse))
        {
            apologize("No user information to display (Your users have not taken a quiz, as yet)");
        }
        else
        {
            // sort the arrays
            asort($companyCount);
            
            // on initial load - direction may not be populated
            if (empty($direction))
                $direction = 'asc';
            
            // render accordingly
            render("admin_form.php", ["userDetails" => $detailsToUse, "direction" => $direction, "sort_by" => $sort_by,  "is_post" => $is_post, "date_and_name" => $date_and_name, "companyCount" => $companyCount, "title" => "User Admin"]);
        }
    }
?>