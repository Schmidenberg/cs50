/**
 * Warren R. Schmidenberg
 * 
 * ExecuGroup Training Site - JavaScript, jQuery, Ajax
 * 
 */

'use strict';
const PASS_RATE = 80; // change constants in PHP
const QUESTION_COUNT = 40; // change constants in PHP
/*
* affects the scroll effect of the header
*/

/**
 * global variable to store the sub tab selected on training.php
 * used to return current user to page position using localStorage
 */
var store_training_subtabs;
var manual_has_loaded;

/**
 * JS to run on all pages - used for storing page position as well as header movement 
 */
$(document).ready(function () {
    // on page load for admin.php - return to page position
    // added timeout to speed up: https://mathiasbynens.be/notes/settimeout-onload
    setTimeout(function() {
        window.onload = function () {
            // if browser supports local storage
            if (typeof(Storage) !== "undefined") {
                var current_page = window.location.pathname;
                // for admin.php page
                if ((current_page) == '/admin.php') {
                    // if user has local storage value stored
                    if (localStorage[current_page]) {
                        // if column sorted or checkbox checked ispost var is 
                        // populated with /admin.php - only sort in those instances
                        var msg = $('#adminDataStored').html();
                        var search = window.location.search;
                        // return to page position when checkbox checked or column sorted only
                        if (msg != null || search != '')
                            return_page_position(current_page);
                    }
                }
            }
        };
    }, 0);
    
    // on scroll, store top of page in localStorage
    function storePageTop () {
        var page_top = document.getElementsByTagName("body")[0].scrollTop;
        var current_page = window.location.pathname;
        if (typeof(Storage) !== "undefined") {
            if (current_page !== '/reports.php' && current_page !== '/training.php') {
                // store page top in localStorage - per page - not /reports.php
                localStorage[current_page] = page_top;
            }
            else if (current_page === '/training.php') {
                localStorage[current_page + store_training_subtabs] = page_top;
            }
        }
    }
    
    /**
     *
     * remove localStorage syntax
     * localStorage.removeItem("/reports.php");
     * 
     */
    
    // disable right-click for "most" users
    document.addEventListener('contextmenu', event => event.preventDefault());
    
    // function to remove the header when scrolling
    // credit to https://medium.com/@mariusc23/hide-header-on-scroll-down-show-on-scroll-up-67bbaae9a78c#.e28pmicyb
    // for ending my sorrow...
    var scrolled;
    var lastScroll = 0;
    var scrollAmount = 1;
    var navHeight = $('header').outerHeight();

    $(window).scroll(function(event){
        scrolled = true;
    });
    
    setInterval(function() {
        if (scrolled) {
            scrollCheck();
            adjustTabs();
            // only when scrolled a fair amount from top, store new value
            // & close pop up
            var top = $(document).scrollTop();
            if (top > 2300) {
                $('[data-popup]').fadeOut(1000);
                storePageTop();
            }
            scrolled = false;
        }
    }, 250);
    
    function scrollCheck() {
        var top = $(document).scrollTop();
        
        if(Math.abs(lastScroll - top) <= scrollAmount)
            return;
        
        if (top > lastScroll && top > navHeight){
            $('header').removeClass('nav-dowsn').addClass('nav-up');
            $('headerRow').removeClass('nav-up').addClass('nav-down');
        } else {
            if(top + $(window).height() < $(document).height()) {
                $('header').removeClass('nav-up').addClass('nav-down');
                $('headerRow').removeClass('nav-down').addClass('nav-up');
            }
        }
        lastScroll = top;
    }
    
    function adjustTabs() {
        var top = $(document).scrollTop();
        if (top > 0)
        {
            if ($('header').hasClass('nav-up')) {
                $("#acc").css("top", 74);
            } else {
                $("#acc").css("top", 159);
            }
        } else
        {
             $("#acc").css("top", 326);
        }
    }
});

/*
* adds functionality to popup buttons + popup timeout
*/
$(function() {
    if (typeof(Storage) !== "undefined") {
        // when 'Go Back' is clicked
        $('[data-popup-revert]').on('click', function(e) {
            var current_page = window.location.pathname;
            // return to position with an animation
            $("html, body").animate({ scrollTop: localStorage[current_page + store_training_subtabs] + 'px' }, 1000);
            $('[data-popup]').fadeOut(1000);
     
            e.preventDefault();
        });
    
        // if over 8 sec from page load - close pop up
        setTimeout(function() {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(1000);
        }, 12000);
        
        // close pop up if close clicked
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(1000);
     
            e.preventDefault();
        });
    }
});

/*
* return current page top to vlaue stored in localStorage
*/
function return_page_position(current_page) {
    if (typeof(Storage) !== "undefined") {
        // if training.php - check for localStorage 
        // for current company and training manual sub tab
        if ((current_page) != '/admin.php') {
            if (localStorage[current_page + store_training_subtabs])
            setTimeout(function() {
                if (manual_has_loaded == 0)
                    // fade pop up onto screen once manual has loaded
                    $('[data-popup="popup-1"]').fadeIn(350);
            }, 250);
        }
        // if /admin.php return page position
        else
            $("body")[0].scrollTop = localStorage[current_page];
    }
}

/*
* flash-red-highlight the message banner top left when multiple choice 
* questions start. As well every five minutes and last sixty seconds
*/
$(document).ready(function () {  
    
     // update left banner questions left - top left of questions.php
    var n = 0;
    setInterval(function() {
        
        var blank = $("div[id=mChoiceQ]").length;
        var answered = $(':radio:checked').length;
        var answerDiv = $('#answerCount');
        
        if (answerDiv)
        {
            answerDiv.html("Answered: " + answered + " - Remaining: " + (blank - answered) );
            
            if (n == 0) {
                msgAlert(answerDiv);
                n = 1;
            }
        }
        
    }, 250);

    // highlights messages in red
    function msgAlert(divId) {
        $(divId).addClass('msgBannerFull');
        setTimeout(function() {
            $(divId).removeClass('msgBannerFull');
        }, 3250);
    }

    if ((window.location.pathname) == '/questions.php') {
        // detect if current tab is not in focus (i.e CTRL + Tab or ALT + TAB)
        var lost_focus = 0;
        function set_focus_to_window() {
            e = window;
            while (e.frameElement !== null) {e = e.parent;}
                e.parent.focus();
        }
        $(window).blur(function(){
            lost_focus++;
        });
        $(window).focus(function(){
            // minitors focus on window - as per quiz rules - 3 illegal
            // moves results in fail
            switch (lost_focus) {
                case 1:
                    alert("ILLEGAL EVENT");
                    break;
                case 3:
                    alert("SECOND ILLEGAL EVENT");
                    break;
                case 5:
                    alert("FINAL ILLEGAL EVENT");
                    break;
                default :
                    break;
            }
            if (lost_focus >= 5) {
                $.ajax({
                    type: 'POST',
                    url: 'illegal_move.php',
                    success: function (response) {
                    window.location.pathname = "illegal_move.php?";
                    }
                });
            }
        });
        
        // function to update left banner time left - top left of questions.php
        var thirtyMax = 30 * 60 * 1000;
        setInterval(function() {
            
            var timeDiv = $('#timeLeft');
            var min = Math.floor(thirtyMax / (60 * 1000));
            var sec = Math.floor((thirtyMax - (min * 60 * 1000)) / 1000);
            
            if (timeDiv)
            {
                // when the timer ends - redirect
                if (thirtyMax <= 0)
                    $('#questionsForm').submit();
                    // flash every five mins and last 60 seconds
                else if ( (((min % 5) == 0) && (sec % 60) == 0) || (((min % 30) == 0) && (sec < 60)) ) {
                    // add leading zero for < 10 seconds
                    (sec < 10) ? sec = "0" + sec : sec;
                    timeDiv.html("Time Remaining: " + min + ":" + sec);
                    msgAlert(timeDiv);
                } else {
                    // add leading zero for < 10 seconds
                    (sec < 10) ? sec = "0" + sec : sec;
                    timeDiv.html("Time Remaining: " + min + ":" + sec);
                }
            }
            thirtyMax -= 1000;
        }, 1000);
    }
});

/*
* index.php - fade in the two paragraphs above an below the logins
* Ensure quiz rules are ticked
*/
$(document).ready(function () {  
    // homepage
    $("#firstIn").delay(1000).animate({ opacity: 1 }, 700);
    $("#secondIn").delay(1000).animate({  opacity: 1 }, 3000);
    // pass reset request page
    $("#thirdIn").delay(1000).animate({ opacity: 1 }, 700);
    $("#fourthIn").delay(1000).animate({  opacity: 1 }, 1500);
    // password confirmation page
    $("#fifthIn").delay(1000).animate({ opacity: 1 }, 300);
    $("#sixthIn").delay(1000).animate({  opacity: 1 }, 1500);
    $("#seventhIn").delay(1000).animate({  opacity: 1 }, 3000);
});

// ensure index.php logins are populated before submission
$(document).ready(function () {
    // login page requires both fields to be populated
    $('#loginForm').submit(function () {
        // trim login value
        var name = $.trim($('#username').val());
        var pass = $.trim($('#password').val());
        // Check if empty of not
        if (name == '' && pass == '') {
            $('#username').attr("placeholder", " Username Required");
            $('#username').toggleClass('highlight');
            $('#password').attr("placeholder", " Password Required");
            $('#password').toggleClass('highlight');
            return false;
        } else if (name == '') {
            $('#username').attr("placeholder", " Username Required");
            $('#username').toggleClass('highlight');
            return false;
        } else if (pass == '') {
            $('#password').attr("placeholder", " Password Required");
            $('#password').toggleClass('highlight');
            return false;
        }
        
    });
    
    //reset account page requires field to be populated, and correctly
    $('#request_reset_form').submit(function () {
        // trim login value
        var details = $.trim($('#request_reset_details').val());
        // check if empty
        if (details == '') {
            $('#request_reset_details').attr("placeholder", "Either your Username or Email must be entered");
            $('#request_reset_details').toggleClass('highlight');
            return false;
        }
        // check if email and then for regex email format
        if (/@/g.test(details)) {
            if (!isValidEmailAddress( details ) ) {
                $('#request_reset_email').toggleClass('highlight');
                bannerFull('Check your Email Format');
                return false;
            }
        }
    });
    
    //new password required both fields to be populated
    $('#reset_password_form').submit(function () {
        // trim login value
        var pwd = $.trim($('#pwd_reset_form').val());
        var confirm = $.trim($('#pwd_confirm_form').val());
        // check if empty
        if (pwd == '') {
            $('#pwd_reset_form').attr("placeholder", "I need your Password...");
            $('#pwd_reset_form').toggleClass('highlight');
            return false;
        }
        if (confirm == '') {
            $('#pwd_confirm_form').attr("placeholder", "Confirmation required...");
            $('#pwd_confirm_form').toggleClass('highlight');
            return false;
        }
        if (confirm !== pwd) {
            $('#pwd_reset_form').toggleClass('highlight');
            $('#pwd_confirm_form').toggleClass('highlight');
            bannerFull('Your Password and Confirmation Do Not Match');
            return false;
        }
    });
    
    // 3 quiz checkbox rules need to be acknowledged before continuing
    $('#quizForm').submit(function (evt) {
        // Get the Login Name value
        if ($('#ticked1').is(':checked') && $('#ticked2').is(':checked') && $('#ticked3').is(':checked') && $('#ticked4').is(':checked')) {
            return true;
        } else {
            // cute prevent default event on error, else page will continue
            evt.preventDefault();
            bannerFull('Confirmation of each rule is required, or ask your Team Leader to assist you. Tick all check boxes before continuing');
        }
    });
    
    // dynamic function to pass message to message bar
    function bannerFull(string) {
        $('#msgBanner').html(string);
        $('#msgBanner').addClass('msgBannerFull');
        // fade in and out after 5 seconds
        setTimeout(function() {
             $('#msgBanner').fadeOut(string);
             $('#msgBanner').promise().done(function (){
                 $('#msgBanner').removeClass('msgBannerFull');
                 $('#msgBanner').html('');
                 // fadeout adds "display: none" - need to remove or next message won't appear
                 $("#msgBanner").removeAttr("style");
             });
        }, 5000);
    }
    
    // if in admin.php - if POST - display message
    if ((window.location.pathname) == '/admin.php') {
        var msg = $('#adminDataStored').html();
        if (msg != null)
                bannerFull(msg);
    }
    
    // regex valid email check - thanks, http://stackoverflow.com/questions/2855865 
    function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
});

/*
* populates the correct manuals associated with the user logins.
*/
function insertImage(company) {
    var imgContent, imgClass;
    // global var  page position localStorage is returned once manual loaded
    manual_has_loaded = 1;
    imgContent = $('#imageInsert');
    imgClass = $('.tightImage');
    
    // if class exists, image is present - remove before adding next image
    if (imgClass[0]) {
        imgContent.children("img:first").remove();
        imgContent.prepend($('<img class=\"tightImage\" id="'+ company + '" src="/img/' + company + '" />'));
    } else {
        imgContent.prepend($('<img class=\"tightImage\" id="'+ company + '" src="/img/' + company + '" />'));
    }
    manual_has_loaded = 0;
}

/*
* set focus to active tabs (left message bar)
*/
$('document').ready(function() {
    
    // "click" the first tab on load
    $('.openFirst').focus().css('outline', 'none'); 
    setTimeout(function() {
        $('.openFirst').trigger('click');
        $('.openFirst').addClass("active ");
    },10);
    
    // keep active tabs highlighted
    var i, priTabs;
    priTabs = $('.subTabsOne');
    
    for (i = 0; i < priTabs.length; i++) {
        priTabs[i].addEventListener('click', handlePri, false);
    }
    
    function handlePri () {
            priTabs.removeClass("active ");
            $( this ).addClass("active ");
    }
    // keep active sub tabs highlighted 
    // had to create new function, passing class into handle func. crashed
    var n, subTabs;
    subTabs = $('.manualImage');
    
    for (n = 0; n < subTabs.length; n++) {
        subTabs[n].addEventListener('click', handleSub, false);
    }

    function handleSub () {
        subTabs.removeClass("active ");
        $( this ).addClass("active ");
        
        // store page position when training sub tab selected
        var current_page = window.location.pathname;
        // no spaces in sub tab description
        store_training_subtabs = $( this ).context.parentNode.id.replace(/ /g,'');
        store_training_subtabs += $( this ).context.innerHTML.replace(/ /g,'');
        return_page_position(current_page);
    }
    
    // find current "main Tab" and highlight it to show as active
    var x, mainTabs, curPage;
    curPage = (window.location.pathname).toLowerCase().replace(/\/|.php/g,'');
    mainTabs = $('.mainTabs');
    for (x = 0; x < mainTabs.length; x++) {
        var curTab = $( mainTabs[x] ).html().toLowerCase();
        if (curPage == curTab) {
            $( mainTabs[x] ).addClass("active ");
        }
    }
});

// populate Reports company pass rate report
$('document').ready(function() {
    if ((window.location.pathname) == '/reports.php') {
        // populate for user_type
        if (document.getElementById('chartContainer') != null) {
            function drawFirstChart() {
                // first report population - Pass Rate by Company
                // gather the variables & parse to get JSON object
                
                var chartDiv = document.getElementById('passRate');
                chartDiv = chartDiv.textContent;
                var divHeader = document.getElementById('passRateHeader');
                divHeader = divHeader.textContent;
                var chartData = JSON.parse(chartDiv);
                
                // Define the chart to be drawn.
                var data = new google.visualization.DataTable(chartData);
                // add columns
                data.addColumn('string', 'Company'); 
                data.addColumn('number', 'No Results');
                data.addColumn({type: 'number', role: 'annotation'});
                data.addColumn({type: 'string', role: 'style'});
                data.addColumn('number', 'Disqualified');
                data.addColumn({type: 'number', role: 'annotation'});
                data.addColumn({type: 'string', role: 'style'});
                data.addColumn('number', 'Passed');
                data.addColumn({type: 'number', role: 'annotation'});
                data.addColumn({type: 'string', role: 'style'});
                data.addColumn('number', 'Failed');
                data.addColumn({type: 'number', role: 'annotation'});
                data.addColumn({type: 'string', role: 'style'});

                // add rows       
                for(var i in chartData) {
                    if (chartData.hasOwnProperty(i)) {
                        var currentObj = chartData[i];
                        // find company name for color change
                        var comp = currentObj.company;
                        // trim string - exclude portion after \n (paren user count)
                        comp = comp.substr(0,comp.indexOf('\n'));
                        
                        // switch function to change chart colors                                                  
                        var colors = company_color_switch(comp);
                        var colorOne = colors[0];
                        var colorTwo = colors[1];
                        
                        data.addRow([currentObj.company, currentObj.noResult, currentObj.annotationNoResult, '#000000', currentObj.disqualified, currentObj.annotationDisqualified, '#808080', currentObj.passed, currentObj.annotationPassed, colorTwo, currentObj.fails, currentObj.annotationFailed, colorOne]);
                    }
                }
                
                // add report options - title and number format for vertical
                var options = {
                    vAxis: {title: "Total Users"},
                    title: divHeader,
                    isStacked: 'percent',
                    animation: {
                        duration: 1000,
                        startup: true
                    },
                    legend: {position: 'none'},
                    hAxis: {title: 'By Company'},
                    annotations: {alwaysOutside : false, textStyle : {fontSize : 10, bold : false, opacity : 0.8}},
                };
    
                // apply the number formats for the annotations
                var disqualified_formatter = new google.visualization.NumberFormat({suffix: ' % no result'});
                disqualified_formatter.format(data, 2);
                var no_result_formatter = new google.visualization.NumberFormat({suffix: ' % disqualified'});
                no_result_formatter.format(data, 5);
                var passed_formatter = new google.visualization.NumberFormat({suffix: ' % passed'});
                passed_formatter.format(data, 8);
                var failed_formatter = new google.visualization.NumberFormat({suffix: ' % failed'});
                failed_formatter.format(data, 11);

                // Instantiate and draw the chart.            
                var chart = new google.visualization.ColumnChart(document.getElementById('chartContainer'));
                chart.draw(data, options);
            }
            google.charts.setOnLoadCallback(drawFirstChart);  
        }
    }
});

// populate division report
$('document').ready(function() {
    if ((window.location.pathname) == '/reports.php') {
        
        // gather variables from php-generated div
        var chartDiv = document.getElementById("divisionPassFail");
        
        if (chartDiv != null) {
            chartDiv = chartDiv.textContent;
            var divHeader = document.getElementById("divisionPassFailHeader");
            divHeader = divHeader.textContent.replace(/"/g,'');
            chartDiv = JSON.parse(chartDiv);
            var companyCount = chartDiv.length;
            var data;
            
            // if companies exist - activate division table
            if (companyCount)
            $('.columns').addClass("chartBorder ");
            
            // if odd number of companies - draw the last block of the container for aesthetic symmetry
            if (companyCount % 2 != 0 && companyCount != 1)
            companyCount++;

            // charts callback
            google.charts.setOnLoadCallback(drawChart);
            
            // create a chart per company    
            function drawChart() {
                for (var i = 0; i < companyCount; i++) {
                    
                    // Define the chart to be drawn
                    data = new google.visualization.DataTable();
                    
                    // add columns
                    data.addColumn('string', 'Company');
                    data.addColumn('number', 'Passed');
                    data.addColumn({type: 'number', role: 'annotation'});
                    data.addColumn({type: 'string', role: 'style'});
                    data.addColumn('number', 'Failed');
                    data.addColumn({type: 'number', role: 'annotation'});
                    data.addColumn({type: 'string', role: 'style'});
                    
                    // add rows
                    try {
                        var divName = chartDiv;
                        var comp = divName[i]["company"];
                        
                        // check if fail count is 0 (no fails) else count how many divs have fails in it
                        if (divName[i].failCount != 0) {
                            // check each division within fails
                            for (var failDiv in divName[i].failCount) {
                                if (divName[i].failCount.hasOwnProperty(failDiv)) {
                                    var divFail_count = divName[i].failCount[failDiv];
                                    
                                    // check against each division within pass
                                    for (var passDiv in divName[i].passCount) {
                                        if (divName[i].failCount.hasOwnProperty(passDiv)) {
                                            var divPass_count = divName[i].passCount[passDiv];        
                                            
                                            // add row only if descriptions match
                                            if (failDiv === passDiv) {
                                                var divPass_name = failDiv;
                                                var count_users = (divFail_count + divPass_count);
                                                var pass_percent = (divPass_count / count_users)*100;
                                                var fail_percent = (divFail_count / count_users)*100;
                                                if (divPass_name == '')
                                                    divPass_name = 'Undefined';
                                                    
                                                // switch function to change chart colors                                                  
                                                var colors = company_color_switch(comp);
                                                var colorOne = colors[0];
                                                var colorTwo = colors[1];
                                                    
                                                data.addRows([
                                                    [divPass_name + '\n (' + count_users + (count_users == 1 ? ' User in Division)' : ' Users in Division)'), divPass_count, pass_percent, colorOne, divFail_count, fail_percent, colorTwo],
                                                ]);
                                            }
                                        }
                                    } 
                                }
                            }
                        }
                        
                        // add chart options
                        var chart_options = column_chart_options(comp, 'By Team Leader \n(Count excludes users who have not taken the quiz)', 'Total Users');
                        
                        //populate php table rows - two tables per row
                        var x, y;
                        if (i % 2 == 0) {
                            x = document.createElement('tr');
                            x.setAttribute("id", "row" + i);
                            y = document.createElement("td");
                            y.setAttribute("id", "barchart_div" + i);
                            y.setAttribute("class", "chartOn");
                            x.appendChild(y);
                            document.getElementById("divisionTable").appendChild(x);
                        } else {
                            y = document.createElement("td");
                            y.setAttribute("id", "barchart_div" + i);
                            y.setAttribute("class", "chartOn");
                            var str = "row" + (i - 1);
                            document.getElementById(str).appendChild(y);
                        }
                            var chart = new google.visualization.ColumnChart(document.getElementById('barchart_div' + i));
                            // apply the number formats for the annotations
                            var passed_formatter = new google.visualization.NumberFormat({suffix: ' % passed'});
                            passed_formatter.format(data, 2);
                            var failed_formatter = new google.visualization.NumberFormat({suffix: ' % failed'});
                            failed_formatter.format(data, 5);
                            chart.draw(data, chart_options);
                        }
                    // when an odd number of companies - I populate even for aesthetic symmetry
                    // this catches the error of the undefined array index
                    catch(err) {
                    }
                }
            }
        }
    }
});

// populate branch manager report
$('document').ready(function() {
    if ((window.location.pathname) == '/reports.php') {
        
        // gather variables from php-generated div
        var chartDiv = document.getElementById("BMPassFail");
        
        if (chartDiv != null) {
            chartDiv = chartDiv.textContent;
            var divHeader = document.getElementById("BMPassFailHeader");
            divHeader = divHeader.textContent.replace(/"/g,'');
            chartDiv = JSON.parse(chartDiv);
            var companyCount = chartDiv.length;
            var data;

            // if companies exist - activate division table
            if (companyCount)
            $('.columns').addClass("chartBorder ");
            
            // if odd number of companies - draw the last block of the container for aesthetic symmetry
            if (companyCount % 2 != 0 && companyCount != 1)
            companyCount++;
            
            // charts callback
            google.charts.setOnLoadCallback(drawChart);
            
            // create a chart per company    
            function drawChart() {
                for (var i = 0; i < companyCount; i++) {
                    
                    // Define the chart to be drawn
                    data = new google.visualization.DataTable();
                    
                    // add columns
                    data.addColumn('string', 'Company');
                    data.addColumn('number', 'Passed');
                    data.addColumn({type: 'number', role: 'annotation'});
                    data.addColumn({type: 'string', role: 'style'});
                    data.addColumn('number', 'Failed');
                    data.addColumn({type: 'number', role: 'annotation'});
                    data.addColumn({type: 'string', role: 'style'});
                    
                    // add rows
                    try {
                        var divName = chartDiv;
                        var comp = divName[i]["company"];
                        var title = divHeader + comp;
                        
                        // check if fail count is 0 (no fails) else count how many divs have fails in it
                        if (divName[i].failCount != 0) {
                            // check each division within fails
                            for (var failDiv in divName[i].failCount) {
                                if (divName[i].failCount.hasOwnProperty(failDiv)) {
                                    var divFail_count = divName[i].failCount[failDiv];
                                    
                                    // check against each division within pass
                                    for (var passDiv in divName[i].passCount) {
                                        if (divName[i].failCount.hasOwnProperty(passDiv)) {
                                            var divPass_count = divName[i].passCount[passDiv];        
                                            
                                            // add row only if descriptions match
                                            if (failDiv === passDiv) {
                                                var divPass_name = failDiv;
                                                var count_users = (divFail_count + divPass_count);
                                                var pass_percent = (divPass_count / count_users)*100;
                                                var fail_percent = (divFail_count / count_users)*100;
                                                // user does not have a BM - i.e. user is an Admin - do not display data in this table
                                                if (divPass_name == '')
                                                    break;
                                                
                                                // switch function to change chart colors                                                  
                                                var colors = company_color_switch(comp);
                                                var colorOne = colors[0];
                                                var colorTwo = colors[1];       
                                                
                                                data.addRows([
                                                    [divPass_name + '\n (' + count_users + (count_users == 1 ? ' User Under BM)' : ' Users Under BM)'), divPass_count, pass_percent, colorOne, divFail_count, fail_percent, colorTwo],
                                                ]);
                                            }
                                        }
                                    } 
                                }
                            }
                        }
                        // add chart options
                        var chart_options = column_chart_options(comp, 'By Branch Manager \n(Count excludes users who have not taken the quiz)', 'Total Users');
                        
                        //populate php table rows - two tables per row
                        var x, y;
                        if (i % 2 == 0) {
                            x = document.createElement("tr");
                            x.setAttribute("id", "rows" + i);
                            y = document.createElement("td");
                            y.setAttribute("id", "barchart_divs" + i);
                            y.setAttribute("class", "chartOn");
                            x.appendChild(y);
                            document.getElementById("branchManagerTable").appendChild(x);
                        } else {
                            y = document.createElement("td");
                            y.setAttribute("id", "barchart_divs" + i);
                            y.setAttribute("class", "chartOn");
                            var str = "rows" + (i - 1);
                            document.getElementById(str).appendChild(y);
                        }
        
                        var chart = new google.visualization.ColumnChart(document.getElementById('barchart_divs' + i));
                        var passed_formatter = new google.visualization.NumberFormat({suffix: ' % passed'});
                        passed_formatter.format(data, 2);
                        var failed_formatter = new google.visualization.NumberFormat({suffix: ' % failed'});
                        failed_formatter.format(data, 5);
                        chart.draw(data, chart_options);
                    }
                    // when an odd number of companies - I populate even for aesthetic symmetry
                    // this catches the error of the undefined array index
                    catch(err) {
                    }
                }
            }
        }
    }
});

// populate team leader report
$('document').ready(function() {
    if ((window.location.pathname) == '/reports.php') {
        
        // gather variables from php-generated div
        var chartDiv = document.getElementById("TLPassFail");
        chartDiv = chartDiv.textContent;
        var divHeader = document.getElementById("TLPassFailHeader");
        divHeader = divHeader.textContent.replace(/"/g,'');
        chartDiv = JSON.parse(chartDiv);
        var companyCount = chartDiv.length;
        var data;
        
        // if companies exist - activate division table
        if (companyCount)
        $('.columns').addClass("chartBorder ");
        
        // if odd number of companies - draw the last block of the container for aesthetic symmetry
        if (companyCount % 2 != 0 && companyCount != 1)
        companyCount++;
        
        // charts callback
        google.charts.setOnLoadCallback(drawChart);
        
        //drawChart(chartDiv, divHeader, companyCount);
        
        // create a chart per company    
        function drawChart() {
            for (var i = 0; i < companyCount; i++) {
                
                // Define the chart to be drawn
                data = new google.visualization.DataTable();
                
                // add columns
                data.addColumn('string', 'Company');
                data.addColumn('number', 'Passed');
                data.addColumn({type: 'number', role: 'annotation'});
                data.addColumn({type:'string', role: 'style'});
                data.addColumn('number', 'Failed');
                data.addColumn({type: 'number', role: 'annotation'});
                data.addColumn({type:'string', role: 'style'});
                
                // add rows
                try {
                    var divName = chartDiv;
                    var comp = divName[i]["company"];

                    // check if fail count is 0 (no fails) else count how many divs have fails in it
                    if (divName[i].failCount != 0) {
                        // check each division within fails
                        for (var failDiv in divName[i].failCount) {
                            if (divName[i].failCount.hasOwnProperty(failDiv)) {
                                var divFail_count = divName[i].failCount[failDiv];
                                
                                // check against each division within pass
                                for (var passDiv in divName[i].passCount) {
                                    if (divName[i].failCount.hasOwnProperty(passDiv)) {
                                        var divPass_count = divName[i].passCount[passDiv];        
                                        
                                        // add row only if descriptions match
                                        if (failDiv === passDiv) {
                                            var divPass_name = failDiv;
                                            var count_users = (divFail_count + divPass_count);
                                            var pass_percent = (divPass_count / count_users)*100;
                                            var fail_percent = (divFail_count / count_users)*100;
                                            // user does not have a TL - i.e. user is a BM or Admin - do not display data in this table
                                            if (divPass_name == '')
                                                break;
                                                
                                            // switch function to change chart colors                                                  
                                            var colors = company_color_switch(comp);
                                            var colorOne = colors[0];
                                            var colorTwo = colors[1];

                                            data.addRows([
                                                [divPass_name + '\n (' + count_users + (count_users == 1 ? ' User Under TL)' : ' Users Under TL)'), divPass_count, pass_percent, colorOne, divFail_count, fail_percent, colorTwo],
                                            ]);
                                        }
                                    }
                                } 
                            }
                        }
                    }
                    
                    // add chart options
                    var chart_options = column_chart_options(comp, 'By Team Leader \n(Count excludes users who have not taken the quiz)', 'Total Users');
                        
                    //populate php table rows - two tables per row
                    var x, y;
                    if (i % 2 == 0) {
                        x = document.createElement("tr");
                        x.setAttribute("id", "rowss" + i);
                        y = document.createElement("td");
                        y.setAttribute("id", "barchart_divss" + i);
                        y.setAttribute("class", "chartOn");
                        x.appendChild(y);
                        document.getElementById("teamLeaderTable").appendChild(x);
                    } else {
                        y = document.createElement("td");
                        y.setAttribute("id", "barchart_divss" + i);
                        y.setAttribute("class", "chartOn");
                        var str = "rowss" + (i - 1);
                        document.getElementById(str).appendChild(y);
                    }
    
                    var chart = new google.visualization.ColumnChart(document.getElementById('barchart_divss' + i));
                    var passed_formatter = new google.visualization.NumberFormat({suffix: ' % passed'});
                    passed_formatter.format(data, 2);
                    var failed_formatter = new google.visualization.NumberFormat({suffix: ' % failed'});
                    failed_formatter.format(data, 5);
                    chart.draw(data, chart_options);
                }
                // when an odd number of companies - I populate even for aesthetic symmetry
                // this catches the error of the undefined array index
                catch(err) {
                }
            }
        }
    }
});

// populate Users Table Report - including nested answers table in each user row
$('document').ready(function() {
    if ((window.location.pathname) == '/reports.php') {

        function drawUsersTable() {
            
            // first report population - Pass Rate by Company
            // gather the variables & parse to get JSON object
            var chartDiv = document.getElementById('usersTable');
            chartDiv = chartDiv.textContent;
            var divHeader = document.getElementById('usersTableHeader');
            divHeader = divHeader.textContent;
            var chartData = JSON.parse(chartDiv);
            
            // seperate each table by company
            var companyCount = chartData.companyCount.length;
            for (var i = 0; i < companyCount; i++) {

                // chop out the necessary vars for each company and create the div
                var x = document.createElement('div');
                x.setAttribute('id', 'table_divs_header' + i);
                x.setAttribute('class', 'chartsSubHeader');

                x.innerHTML = chartData.companyCount[i];
                document.getElementById('user_table_div').appendChild(x);

                // Define the chart to be drawn.
                var data = new google.visualization.DataTable(chartData);
                
                // add columns
                data.addColumn('number', '');
                data.addColumn('string', 'Name');
                data.addColumn('string', 'Last Name');
                data.addColumn('string', 'Division');
                data.addColumn('string', 'Branch Manager');
                data.addColumn('string', 'Team Leader');
                data.addColumn('number', 'Asked');
                data.addColumn('number', 'Answered');
                data.addColumn('number', 'Correct');
                data.addColumn('string', 'Result');
                data.addColumn('string', 'Quiz Start');
                data.addColumn('string', 'Duration (mm:ss)');
                
                // declare out of scope below, on first company iteration only
                if (i == 0)
                var usersAnswers = [];
                var userCount = 0;
                // add rows       
                for(var n in chartData) {
                    if (chartData.hasOwnProperty(n)) {
                        var currentObj = chartData[n];
                        console.log("Need to show the users data who have not yet taken the quiz in the table");
                        
                        // consolidate user details for answers reports - required on first company iteration only
                        if (i == 0) {
                            if (typeof(currentObj.userAnswers) !== 'undefined') {
                                usersAnswers[n] = [];
                                usersAnswers[n]["user"] = currentObj.name + currentObj.last_name;
                                usersAnswers[n]["answers"] = currentObj.userAnswers;
                            }
                        }
                        if (currentObj.company_cur === chartData.companyCount[i]) {
                            
                            // convert (test_end - test_start) to milliseconds, then to formatted string for time duration
                            var quiz_duration =  Math.abs(new Date(currentObj.test_start) - new Date(currentObj.test_end));
                            function milliToMins(milli) {
                                var mins = Math.floor(milli / 60000);
                                var secs = ((milli % 60000) / 1000).toFixed(0);
                                return mins + ":" + (secs < 10 ? '0' : '') + secs;
                            }
                            quiz_duration = milliToMins(quiz_duration);
                            
                            // determine if passed/ failed or disqualified
                            var passed;
                            if (currentObj.illegal_move === "Null")
                                passed = (currentObj.answeredCorrectly / QUESTION_COUNT)*100 >= PASS_RATE ? '\u2714' : '\u2717';
                            else {
                                passed = 'D';
                                
                            }
                            userCount++;
                            
                            // populate rows
                            data.addRow([userCount, currentObj.name, currentObj.last_name, currentObj.division_cur, currentObj.BM_cur, currentObj.TL_cur, 
                            QUESTION_COUNT, currentObj.questionsAnswered, currentObj.answeredCorrectly, passed, currentObj.test_start, quiz_duration]);
                            // dummy row - used to populate each answer's table once the table has been created
                            data.addRow([1, currentObj.name, currentObj.last_name, '4', 'Answers', currentObj.company_cur, 5, 6, 7, currentObj.illegal_move, '9', '10']);
                        }
                    }
                }
                // create each company div chart
                var y = document.createElement('div');
                y.setAttribute('id', 'table_divs' + i);
                y.setAttribute('class', 'tablesides table' + i);
                document.getElementById('user_table_div').appendChild(y);
                
                // Instantiate and draw the chart.            
                var table = ("table" + i);
                table = new google.visualization.Table(document.getElementById('table_divs' + i));
                
                // can't take credit for this: http://stackoverflow.com/questions/20659635
                // add row id (name and surname) to table row as id
                google.visualization.events.addListener(table, 'ready', function () {
                    $('#user_table_div .google-visualization-table-tr-even').each(function (e) {
                        var idx = $('td:nth-child(2)', this).html();
                        idx = idx + $('td:nth-child(3)', this).html();
                        idx = idx.replace(/\s/g, '');
                        // only add div once - not for each company iteration
                        if($("#" + idx).length == 0) {
                            $(this).attr('id', idx);
                        }
                    });
                    // for odd rows add id for answers 
                    $('#user_table_div .google-visualization-table-tr-odd').each(function (e) {
                        var idx = $('td:nth-child(2)', this).html();
                        idx = idx + $('td:nth-child(3)', this).html();
                        idx = idx + $('td:nth-child(5)', this).html();
                        idx = idx.replace(/\s/g, '');
                        // only add div once - not for each company iteration
                        if($("#" + idx).length == 0) {
                            $(this).attr('id', idx);
                        }
                    });
                });
                
                /**
                 * add listener to row click (select)
                 * when clicked expand the answers table for that user
                 * this took a day at least... 
                 */
                var _idNames = [];
                var _idName;
                // when a table is selected, apply timeout to prevent drag select, 
                // check each row to show the answers table of selection, if applicable
                google.visualization.events.addListener(table, 'select', function () {
                    setTimeout(function() {
                        $('#user_table_div .google-visualization-table-tr-even').each(function (e) {
                            if ($(this).hasClass('google-visualization-table-tr-sel')) {
                                _idName = this.id;
                                // if the row has been selected - store id to array and unhide,   
                                // only store id to array once (.inArray returns -1 when not found)
                                if ($.inArray( _idName, _idNames) < 0) {
                                    _idNames.push(_idName);
                                    $('#' + _idName + 'AnswersInsert').fadeIn().removeClass('hideAnswers'); // fade for effect
                                }
                            // every other non-selected row is now checked
                            } else {
                                // iterate array, if element's 'sel' class no longer exists, 
                                // hide the answers table (i.e. it has been unchecked)
                                for (var i in _idNames) {
                                    if (!$('#' + _idNames[i]).hasClass('google-visualization-table-tr-sel')) {
                                        $('#' + _idNames[i] + 'AnswersInsert').fadeOut().addClass('hideAnswers');
                                        // then remove id from array
                                        _idNames = jQuery.grep(_idNames, function(value) {
                                            return value != _idNames[i];
                                        });
                                    }
                                }
                            }
                        });
                    }, 200);
                });
                table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
            }
            // remove all columns (td) except the first from dummy row - preparing row for table insert
            $('#user_table_div .google-visualization-table-tr-odd').find("td:gt(0)").remove();
            // create answers charts
            drawUsersAnswers(usersAnswers);
        }
        google.charts.setOnLoadCallback(drawUsersTable);
    }
});

// draws a chart for each user's answers - called by pass rate table report
function drawUsersAnswers(usersAnswers) {
    
    // first report population - Pass Rate by Company
    // gather the variables & parse to get JSON object

    // per company variables
    var chartData = usersAnswers;
    
    // Instantiate and draw a chart for each user
    for(var n in chartData) {
        // Define the chart to be drawn.
        var data = new google.visualization.DataTable(chartData);
        // add columns
        data.addColumn('string', 'Stem');
        data.addColumn('string', 'Key');
        data.addColumn('string', 'User Selected');
        data.addColumn('string', 'Distractor One');
        data.addColumn('string', 'Distractor Two');
        data.addColumn('string', 'Distractor Three');
        data.addColumn('string', 'Distractor Four');
        data.addColumn('boolean', 'Answered Correctly?');
        
        // add rows
        if (chartData.hasOwnProperty(n)) {
            var answers = chartData[n].answers;
            // enumerate through specific user answers
            for (var t in answers) {
                if (answers.hasOwnProperty(t)) {
                    var currentObj = answers[t];
                    if (currentObj === '3 Events: Tab focus lost' || currentObj === 'Page Refresh') {
                        data.addRow(["The user was disqualified", "-",  "reason code:", currentObj, " - ", "user's results discarded", "-", false]);
                    }
                    else {
                        // set boolean if correct
                        var correct = false;
                        if (currentObj.user_selected === currentObj.q_key) {
                            correct = true;
                        }
                        data.addRow([currentObj.stem, currentObj.q_key, currentObj.user_selected, currentObj.distractor_one, currentObj.distractor_two, currentObj.distractor_three, currentObj.distractor_four, correct]);
                    }
                }
            }
        }
        // create each company div chart to populate
        var idx = chartData[n].user + 'Answers';
        idx = idx.replace(/\s/g, '');
        
        // must be td to use colspan attr
        var y = document.createElement('td');
        y.setAttribute('id', idx + 'Insert');
        y.setAttribute('class', 'hideAnswers');
        
        // sets the answers table to cover entire "parent" row, not just one cell
        y.setAttribute('colspan', '12');
        var refNode = document.getElementById(idx);
        
        // insert the answers after the correct node
        refNode.appendChild(y);
        
        // add class to hide row using JS this time for kicks
        var firstChild = refNode.childNodes;
        firstChild[0].className += ' hideAnswers';
        
        // Instantiate and draw the chart.   
        var answersTable = ("answersTable" + n);
        answersTable = new google.visualization.Table(document.getElementById(idx + 'Insert'));
        answersTable.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

        // add the class to the answers table to outline and adjust font
        // affect the class for th etable border
        var answerTable = document.getElementById(idx + 'Insert');
        $(answerTable.childNodes[0]).addClass('answerTable');

        // affect the class for the table cells
        // please burn this into your psyche for future reference
        // $('#answersTable tbody').addClass('answersBorder');
        $('#' + idx + ' tbody').addClass('answersTable');
        
        // affect pastel red or green background of cell if checkmark or x mark
        $('.google-visualization-table-td').each(function (e) {
            if ($(this).html() == '\u2714') {
                $(this).addClass(' correct');
            } else if ($(this).html() == '\u2717' ||  $(this).html() == 'D') {
                $(this).addClass(' notCorrect');
            }
        });
    }
}

// color switch for charts
function company_color_switch(comp) {
    var colorOne;
    var colorTwo;
    var colors=[];
    
    switch (comp) {
        case 'Hire Resolve':
            colorOne = 'color: #0099FF'; //blues
            colorTwo = 'color: #0000CC';
            break;
        case 'Hire Resolve USA':
            colorOne = 'color: #08086b'; // default red and blue
            colorTwo = 'color: #e71029';
            break;
        case 'Tumaini':
            colorOne = 'color: #33CC33'; //greens
            colorTwo = 'color: #006600';
            break;   
        case 'Mass Staffing Projects':
            colorOne = 'color: #FA0000'; //reds
            colorTwo = 'color: #800000';
            break;   
        default:
            colorOne = 'color: #C761FC'; //purples
            colorTwo = 'color: #6600CC';
            break;
    }
    colors[0] = colorOne;
    colors[1] = colorTwo;
    return colors;
}

// function for duplicate chart option JSON objects
function column_chart_options(title, hTitle, vTitle) {
    var myObj = {};
        myObj["title"] = title;
        myObj["width"] = '500';
        myObj["height"] = '300';
        myObj["isStacked"] = 'true';
        
        myObj["animation"] = {};
        myObj["animation"]["duration"] = '1000';
        myObj["animation"]["startup"] = 'true';
        
        myObj["annotations"] = {};
        myObj["annotations"]["textStyle"] = {};
        myObj["annotations"]["textStyle"]["fontSize"] = '9';
        myObj["annotations"]["textStyle"]["bold"] = 'true';
        myObj["annotations"]["textStyle"]["opacity"] = '0.8';
        
        myObj["titleTextStyle"] = {};
        myObj["titleTextStyle"]["fontSize"] = '12';
        myObj["titleTextStyle"]["bold"] = '1';
        
        myObj["legend"] = {};
        myObj["legend"]["position"] = "none";
        
        myObj["tooltip"] = {};
        myObj["tooltip"]["textStyle"] = {};
        myObj["tooltip"]["textStyle"]["fontSize"] = '10';
        
        myObj["chartArea"] = {};
        myObj["chartArea"]["top"] = '55';
        myObj["chartArea"]["height"] = '50%';
        
        myObj["vAxis"] = {};
        myObj["vAxis"]["title"] = vTitle;
        
        myObj["vAxis"]["textStyle"] = {};
        myObj["vAxis"]["textStyle"]["fontSize"] = 9;
        
        myObj["hAxis"] = {};
        myObj["hAxis"]["title"] = hTitle;
        
        myObj["hAxis"]["textStyle"] = {};
        myObj["hAxis"]["textStyle"]["fontSize"] = 9;
        
    return myObj;
}

// admin page - functions to add icon to sort column & check if check all ticked
$('document').ready(function() {
    if ((window.location.pathname) == '/admin.php') {
        
        // add sort icon to column header if sorted by user
        // grab column sorted by, if any
        var sortedBy = window.location.search.split("=");
        // if sorted - split down to just column name
        if (sortedBy[1]) {
            sortedBy = sortedBy[1].split("_");
            // get next i after class "column name" to add new class 
            $("." + sortedBy).addClass('glyphicon glyphicon-sort');
        }
        
        // detect if "check all" is checked, then check all users (or uncheck)
        // add event handler to the 'th' checkbox - one 'th' per company
        $(".company_cbox").on('click', function check_all() {
            var this_company;
            // when 'th' checkbox is checked, check all 'td' check boxes
            if ($(this).prop("checked")) {
                this_company = $(this).prop('id');
                $(".cbox_" + this_company).prop('checked', true);
            // when unchecked, uncheck all 'td' checkboxes
            } else {
                this_company = $(this).prop('id');
                $(".cbox_" + this_company).prop('checked', false);
            }
        });
    }
});
