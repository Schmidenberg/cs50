<?php
    // configuration
    require("../includes/config.php");
    
// if user reached page via GET or POST
if ($_SERVER["REQUEST_METHOD"] == "GET")
{
    // gather the possible sub-grouping for the current user
    $userDetails = CS50::query("SELECT * FROM `user_profile` WHERE `id` = ?;", $_SESSION["id"]);
    // use first (and only) row for the current user
    $userDetails = $userDetails[0];
    $userType = $_SESSION["user_type"];

    // error-check
    if (empty($userDetails) || empty($userType))
        apologize("Error in \"Username\", try again");
    elseif (empty($userDetails["company"]))
        apologize("Error in \"Username.Company\", try again");

    // if user is admin, collect all data for all companies
    if ($userType == "Administrator")
    {
        $allCompanies = CS50::query("SELECT DISTINCT company FROM user_profile;");
        $countCompanies = count($allCompanies);
        $groupingToUse = [];

        foreach ($allCompanies as $company)
        {
            $strComp = implode($company);
            $div = CS50::query("SELECT DISTINCT division FROM user_profile WHERE company = ?", $strComp);
            
            $groupingToUse[] = [
            "company" => $strComp,
            "division" => CS50::query("SELECT DISTINCT division FROM user_profile WHERE company = ?", $strComp),
            "user_type" => CS50::query("SELECT DISTINCT `users`.`user_type`
                                    FROM `user_profile`
                                    LEFT JOIN `users`
                                    ON `user_profile`.`id`=`users`.`id`
                                    WHERE `company` = ?
                                    AND `users`.`user_type` != 'Administrator'
                                    ORDER BY `user_profile`.`id;", $strComp)
            ];
        }
        $groupingToUse = beautify_array($groupingToUse);
    }
    // if user is BM, gather all data for user company only
    elseif ($userType == "Branch Manager")
    {
        $specificCompany = $userDetails["company"];
        $specificBManager = $userDetails["branch_manager"];
        $specificConsultant = $userDetails["consultant"];
        
        $groupingToUse = [];
        $groupingToUse[] = [
        "company" => $specificCompany,
        "division" => CS50::query("SELECT DISTINCT division FROM user_profile WHERE company = ?", $specificCompany),
        "user_type" => CS50::query("SELECT DISTINCT `users`.`user_type`
                                FROM `user_profile`
                                LEFT JOIN `users`
                                ON `user_profile`.`id`=`users`.`id`
                                WHERE `company` = ?
                                AND `users`.`user_type` != 'Administrator'
                                ORDER BY `user_profile`.`id;", $specificCompany)
        ];
        $groupingToUse = beautify_array($groupingToUse);
    }
    // if TL, gather all data for user company, No BM, Select TL
    elseif ($userType == "Team Leader")
    {
        $specificCompany = $userDetails["company"];
        $specificTLeader = $userDetails["team_leader"];
        $specificConsultant = $userDetails["consultant"];
        
        $groupingToUse = [];
        $groupingToUse[] = [
        "company" => $specificCompany,
        "division" => CS50::query("SELECT DISTINCT division FROM user_profile WHERE company = ? AND team_leader = ?", $specificCompany, $specificTLeader),
        // do not pull BM manuals
        "user_type" => CS50::query("SELECT DISTINCT `users`.`user_type`
                                FROM `user_profile`
                                LEFT JOIN `users`
                                ON `user_profile`.`id`=`users`.`id`
                                WHERE `company` = ?
                                AND `users`.`user_type` != 'Administrator'
                                AND `users`.`user_type` != 'Branch Manager'
                                ORDER BY `user_profile`.`id;", $specificCompany)
        ];
        $groupingToUse = beautify_array($groupingToUse);
    }
    // only consultants left, only display div, per company
    else
    {
        $specificCompany = $userDetails["company"];
        $specificConsultant = $userDetails["consultant"];
        $groupingToUse = [];
        $groupingToUse[] = [
        "company" => $specificCompany,
        "division" => CS50::query("SELECT DISTINCT division FROM user_profile WHERE company = ? AND id = ?", $specificCompany, $_SESSION["id"]),
        // do not pull BM or TL manuals
        "user_type" => CS50::query("SELECT DISTINCT `users`.`user_type`
                                FROM `user_profile`
                                LEFT JOIN `users`
                                ON `user_profile`.`id`=`users`.`id`
                                WHERE `company` = ?
                                AND `users`.`user_type` != 'Administrator'
                                AND `users`.`user_type` != 'Branch Manager'
                                AND `users`.`user_type` != 'Team Leader'
                                ORDER BY `user_profile`.`id;", $specificCompany)
        ];
        $groupingToUse = beautify_array($groupingToUse);
    }
    // render accordingly
    render("training_form.php", ["groupingToUse" => $groupingToUse, "title" => "Manuals"]);
}
?>
