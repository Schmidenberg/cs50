<?php
    // configuration
    require("../includes/config.php");
    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        // else render form
        render("request_reset_form.php", ["title" => "Reset Account Logins"]);
    }
    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $userLoginDetail = [];
        
        // check user info for details entered
        if (!empty($_POST["details"]))
        {
            $userLoginDetail = $_POST["details"];
            
            // check for at sign in user entered details
            if (strpos($userLoginDetail, '@') !== false) {
                // regex $POST email name for valid email format (second time - first was via JS)
                $userLoginDetail = filter_var($userLoginDetail, FILTER_SANITIZE_EMAIL);
                // validate e-mail - x3, to be sure, again
                if (!filter_var($userLoginDetail, FILTER_VALIDATE_EMAIL))
                    apologize("Error in email format, try again");
                else
                $userLoginDetail = CS50::query("SELECT id FROM users WHERE email = ?;", $userLoginDetail);
            }
            // if user didn't use email for log in check for username
            else
            {
                // regex $POST name for only alphanumeric chars
                $userLoginDetail = preg_replace("/[^A-Za-z0-9 ]/", '', $userLoginDetail);
                $userLoginDetail = CS50::query("SELECT id FROM users WHERE username = ?;", $userLoginDetail);
            }
        }
        // user details not returned i.e. no match
        if (empty($userLoginDetail))
            apologize("Unrecognised User Details");
        // send email
        else
        {
            // select the id for the one and only row
            $userId = $userLoginDetail[0]["id"];
            $userLoginDetail = CS50::query("SELECT `users`.`email`, `users`.`last_login`, `user_profile`.`name`
                                            FROM `users`
                                            LEFT JOIN `user_profile` 
                                            ON `users`.`id` = `user_profile`.`id`
                                            WHERE `users`.`id` = ?
                                            ORDER BY `users`.`email`;", $userId);
            // send name and email to email function
            send_email($userLoginDetail, "user_request", $userId);
            
            // render response function appropriately 
            $url = "/index.php";
            $linktext = "Home Page";
            $header = "EMAIL SENT!";
            $message = "your email will arrive shortly with instuctions on how to continue the password reset process";
            $title = "Email En Route";
            response($url, $linktext, $message, $header, $title);
        }
    }
?>