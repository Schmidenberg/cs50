<?php

    // configuration
    require("../includes/config.php"); 

    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        // illegal, render login form
        logout();
        render("login_form.php", ["title" => "Staff Training"]);
    }

    // else if user reached page via POST, legal
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // log in using pwd and name
        login($_POST["password"], $_POST["username"]);
    }
    // else, it all went wrong!
    logout();
    apologize("Login failed");
?>
