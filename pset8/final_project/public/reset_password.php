<?php
    /**
     * 
     * Warren R. Schmidenberg
     * reset password - after tokened URL is clicked by user - securley 
     * check data against DB data & allow user to create new password
     * 
     **/
    
    // configuration
    require("../includes/config.php");

    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        if(isset($_GET["token"]) && !empty($_GET["token"]))
        {
            // verify the token data - sanitize (as best I know how, above CS50's sanitization)
            // ensure alphanumeric only
            $token = preg_replace("/[^A-Za-z0-9]/", '', $_GET["token"]);
            
            // if special chars are injected, escape them
            $token = htmlspecialchars($token);
            if (strlen($token) != 40)
                apologize("Invalid Page Request - Contact Operations");
            // grab correct user details if secure and valid
            else
            {
                // grab active user who selected link in their email
                $user = CS50::query("SELECT `is_active`, `user_id`, `already_used` FROM `reset_passwords` WHERE `hash` = ?;", $token);
                // update token to used token
                CS50::query("UPDATE `reset_passwords` SET `already_used`= 1 WHERE `hash` = ?;", $token);
                
                // one and only row
                if (isset($user[0]))
                {
                    $user = $user[0];
                    if ($user["is_active"] <= date("Y-m-d H:i:s"))
                    {
                        apologize("Email link Expired - Register for a new one or contact Operations");
                    }
                    else if ($user["already_used"] == 1)
                    {
                        apologize("Email link Already Used - Register for a new one or contact Operations");
                    }
                    else
                    {
                        // else render page for a new password
                        $_SESSION['user'] = $user;
                        render("create_password_form.php", ["title" => "Reset Account Logins"]); 
                    }
                }
                else
                {
                    apologize("Invalid Page Request");
                }
            }
        }
        else
        {
            apologize("Invalid Page Request - Contact Operations");
        }
    }
    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // get captures illegal page request, this is a double-check 
        if(isset($_SESSION["user"]) && !empty($_SESSION["user"]))
        {
            $user = $_SESSION["user"];
            // also, double-check pwd and comfirm if JS fails
            if (empty($_POST["password"]))
            {
                apologize("You must provide your password.");
            }
            else if (empty($_POST["confirm"]))
            {
                apologize("You must provide your password confirmation.");
            }
            // make sure the password and confirmation match
            else if ($_POST["password"] != $_POST["confirm"])
            {
                apologize("Your password and confirmation do not match.");
            }
            else
            {
                // update DB with new password
                $add = CS50::query("UPDATE `users` SET `password`= ? WHERE `id` = ?;", password_hash($_POST["password"], PASSWORD_DEFAULT), $user["user_id"]);
                
                // one and only row
                if($add == 1)
                {
                    // log in using pwd and id
                    login($_POST["password"], $user["user_id"]);
                }
                
            }
        }
        else
        {
            apologize("Invalid Page Request");
        }
    }
?>