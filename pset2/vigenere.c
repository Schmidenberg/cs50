/**
 * vigenere.c
 *
 * cryptography, slightly better
 * 
 */

#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

// accept a single cl argument alpha, as 'key'
int main(int argc, string argv[])
{
    if (argc != 2)
    {
        printf("Usage: Vigenere <keyword>\n");
        return 1;
    }
    
    int length = strlen(argv[1]);
    
    for (int i = 0; i < length; i++)
    {
        if (!isalpha(argv[1][i]))
        {
            printf("Keyword must be only A-Z and a-z\n");
            return 1;
        }
    }
    
    // prompt for plain text    
    printf("Plaintext: ");

    string plain = GetString();
    int p_len = strlen(plain);
    
    // use rollit counter if char in plain is not alpha
    int rollit = 0;
    char key;
    
    printf("Ciphertext: ");
    for (int i = 0; i < p_len; i++)
    {
        if (rollit < 0 || (i < p_len && rollit == length ))
        {
            rollit = 0;
        }
  
        // if key is upper
        if isupper (argv[1][rollit])
        {
            key = argv[1][rollit] - 'A';
            {

                // check if plain is upper, lower, neither
                if isupper (plain[i])
                {
                    printf("%c", ((plain[i] - 'A' + key) % 26 + 'A'));
                }
                else if islower (plain[i])
                {
                    printf("%c", ((plain[i] - 'a' + key) % 26 + 'a'));
                }
                else
                {
                    printf("%c", plain[i]);
                    
                    // if is neither then decrement counter
                    rollit--;
                }
            }
        }
        // if not upper, then can only be lower
        else
        {
            key = argv[1][rollit] - 'a';
            {
                // check if plain is upper, lower, neither
                if isupper (plain[i])
                {
                    printf("%c", ((plain[i] - 'A' + key) % 26 + 'A'));
                }
                else if islower (plain[i])
                {
                    printf("%c", ((plain[i] - 'a' + key) % 26 + 'a'));
                }
                else
                {
                    printf("%c", plain[i]);
                    
                    // if is neither then decrement counter 
                    rollit--;
                }
            }
        }

        // increment counter
        rollit++;
    }
    // return 0 - I suspect that this is where the issue is...
    printf("\n");
    return 0;
}