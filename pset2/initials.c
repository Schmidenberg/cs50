/**
 * by Warren Schmidenberg
 * 
 * print upper case initials for each name
 * 
 **/

#include <stdio.h>
#include <cs50.h>
#include <string.h>

int main(void)
{
    string name = GetString();
    int length = strlen(name);
    
    // capatalise the first
    for (int i = 0; i < length; i++)
    {
        if (name[i] == name[0])
        {
            if (name[i] < 123 && name[i] > 96)
            {
                printf("%c", name[i] - 32);    
            }
            else
            {
                printf("%c", name[i]);
            }
        }
        // else captitalise after the space
        else if (name[i] == 32)
        {
            if (name[i + 1] < 123 && name[i + 1] > 96)
            {
                printf("%c", name[i + 1] - 32);
            }
            else
            {
                printf("%c", name[i + 1]);
            }
        }
    }
    printf("\n");
}
