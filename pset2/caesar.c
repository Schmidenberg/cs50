/**
 * caesar.c 
 * 
 * cryptography for beginners
 * 
 */

#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, string argv[])
{
    // first, check for argc ==2
    if (argc > 2 || argc < 2)
    {
        printf("Usage: /home/cs50/pset2/caesar <key>\n");
        return 1;
    }
    
    // next, check that k !< 0
    int k = atoi(argv[1]);
    
    if (k < 0)
    {
        printf("Key must be a non-negative integer.\n");
        return 1;
    }
    
    // get the string
    string p = GetString();
    int length = strlen(p);
    {
        for (int i = 0; i < length; i++)
        {
            if (isdigit(p[i]))
            {
                printf("%c", p[i]);
            }
            else if (isalpha(p[i]))
            {
                if (p[i] >= 'a' && p[i] <= 'z')
                {
                    printf("%c", (p[i] - 'a' + k) % 26 + 'a');
                }
                else if (p[i] >= 'A' && p[i] <= 'Z')
                {
                    printf("%c", (p[i] - 'A' + k) % 26 + 'A');
                }
                
            }
            else 
            {
                printf("%c", p[i]);
            }
        }
        printf("\n");
    }
        return 0;
}
