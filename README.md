# README #

My CS50 code for the course https://cs50.harvard.edu/

### What is this repository for? ###

* A showcase of my code for the CS50 course - completed in April 2017.

## What we learned ##

* A broad and robust understanding of computer science and programming
* How to think algorithmically and solve programming problems efficiently
* Concepts like abstraction, algorithms, data structures, encapsulation, resource management, security, software engineering, and web development
* Familiarity in a number of languages, including C, PHP, SQL, and JavaScript plus CSS and HTML
* How to engage with a vibrant community of like-minded learners from all levels of experience
* How to develop and present a final programming project to your peers

## Result ##

* I completed the course and earned a Verified Certificate.

### Who do I talk to? ###

* w.schmidenberg@gmail.com
* https://www.linkedin.com/in/warren-schmidenberg/