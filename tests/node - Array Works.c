/**
 * created by WRS - this progam displays the use of a struct and  
 * the creation of the initial node to a linked list
 * 
 * incl. free malloc order
 *  
 * using a function to create the node - YES!
 * 
 **/

#include <stdio.h>
#include <stdbool.h>
#include <cs50.h>
#include <ctype.h>

#define LENGTH 45

// structs
typedef struct node
{
    bool is_word;
    struct node* children[27];
}
node;

//void tempString(FILE* fileName, node* nodeName);
//int wordCount(FILE* fileName);

int main(void)
{
    
    // bring in the chars
    FILE* file = fopen("NodeFile", "r");
    if (file == NULL)
    {
        printf("Input File Error\n");
        return INT_MAX;
    }

    // check each letter & store into array lower with '\n'
    for (int c = fgetc(file); c != EOF; c = fgetc(file))
    {
        char whole_word[LENGTH];
        
        int itt = 0;
        // need to store words in array to check for \n
        if (c != '\n')
        {
             // if is a good letter
             if (isalpha(c) || (c == '\''))
            {
                 // convert character to lower
                 if (isupper(c))
                {
                    c = tolower(c);
                }
                // store it
                whole_word[itt] = c;
            }
            itt++;
        }
        else
        {
            whole_word[itt] = '\n';
            itt++;
        }
        // the below is used for troubleshooting - DELETE
         printf("whole_word: %c\n", whole_word[0]);
        
        // create the root
        node* root = malloc(sizeof(node));
        root->is_word = 0;
        
        
    }
}
 
 /*
    
    



    //tempString(file, root);
    
    printf("Word: %s\n", root->children);
*/
 //   fclose(file);
 //   free(root->children);
 //   free(root->is_word);
 //   free(root);



/** function to create temp buffer & memory for word member
void tempString(FILE* fileName, node* nodeName)
{
    char buffer[15];

    fscanf(fileName, "%14s", buffer);

    // create pointer to node root
    nodeName->next = NULL;
    
    int len = strlen(buffer);
    nodeName->word = malloc((len + 1) * sizeof(char));
    strcpy(nodeName->word, buffer);
    
    
    // calls wordCount to count words in dictionary
    wordCount(fileName);
}

// function to count words in file
int wordCount(FILE* fileName)
{
    unsigned int count = 0;
    char ch;
    
    while((ch = fgetc(fileName)) != EOF)
    {
        if(ch == '\n')
            count++;
    }
    printf("Word Count: %i\n", count);
    return count;
}
*/