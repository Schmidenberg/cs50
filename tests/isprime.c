// Program to generate a table of prime numbers
#include <stdio.h>
#include <stdbool.h>

int main (void)
{
    int i, j;
    _Bool isprime;
    
    for (i = 2; i <= 50; i++)
    {
        isprime = true;
        for (j = 2; j < i; j++)
        {
            if (i % j == 0)
            {
                isprime = false;
            }
        }
        if (isprime)
        {
            printf("%i ", i);
        }
    }
    printf("\n");
}