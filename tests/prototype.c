#include <stdio.h>
/**
 * 
 * shows what a prototype is and how it is called
 * 
 */

// prototype: function prototype for add
float add (float n,float m);

// below shows the function protoype in use
int main(void)
{
    printf("%f\n",add(3.5,2.5));
}

// function: this is the function - how the function works
float add(float i, float j)
{
    return i+j;
}

