
/**
 * water.c 
 * 
 * calculates the number of bottles of water from a n minute shower
 * 1 minute = 12 bottles
 * uses a prototype
 * 
 */

#include <stdio.h>
#include <cs50.h>

// this the function prototype
float bottleCalc(float n);

int main(void)
{
    printf("minutes: ");
    bottleCalc(GetFloat());
}

// below is the function 
float bottleCalc(float n)
{
    n = n * 192.00 / 16.00;
    return printf("bottles: %2.2f\n", n);
}