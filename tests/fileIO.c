// Program to copy one file to another
#include <stdio.h>
#include <ctype.h>

int main (void)
{
    char inName[10], outName[10];
    FILE *in, *out;
    int c;
    
    printf("What is the name of the input file?\n");
    scanf("%s", inName);
    printf("What is the name of the input file?\n");
    scanf("%s", outName);
    
    if ((in = fopen (inName, "r")) == NULL)
    {
        printf("Cannot open %s file", inName);
        return 1;
    }
    if ((out = fopen (outName, "w")) == NULL)
    {
        printf("Cannot open %s file", outName);
        return 1;
    }
    
    int lineTaken = 0;
    while ((c = getc (in)) != EOF)
    {
        if (lineTaken == 0)
        {
            if (islower(c))
            {
                c -= 32;
            }
            fprintf (stdout, out);
            if (c == '\n')
            {
                lineTaken = 1;
            }
        }
        else if (lineTaken == 1)
        {
            if (c == '\n')
            {
                fprintf (stdout, out);
                lineTaken = 0;
            }
        }
        
    }    
    fclose(in);
    fclose(out);
    printf ("File has been copied.\n");
    return 0;
}