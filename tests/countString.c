#include <stdio.h>

int stringLength (char characterString[]);

int main(void)
{
    char str1[] = {'C', 'a', 't', '\0'};
    char str2[] = {'i', 'n', '\0'};
    char str3[] = {'H', 'a', 't', '\0'};
    char myString = getc();

    printf(" STR1: %i\n STR2: %i\n STR3: %i\n", stringLength(str1), stringLength(str2), stringLength(str3), myString);
}

int stringLength (char characterString[])
{
    int count = 0;
    
    while (characterString[count] != '\0')
    {
        count++;
    }
    return count;
}