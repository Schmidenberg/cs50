#include<stdio.h>

void readline(char buffer[]);

int main(void)
{
    char line[81];
    
    for (int i = 0; i < 3; i++)
    {
        readline(line);
        printf("%s\n", line);
    }
    return 0;
}

void readline(char buffer[])
{
    char letter;
    int i =0;
    do
    {
        letter = getchar();
        buffer[i] = letter;
        i++;
    }
    while(letter != '\n');
    
    buffer[i - 1] = '\0';
    
}