#include <cs50.h>
#include <stdio.h>

#define SIZE 8

bool search(int needle, int haystack[], int size)
{
    
    int middle = 0;
    int bottom = 0;
    int top = size -1;
	    
    while (bottom <= top)
    {
        for (int i = 0; i < size -1; i++)
    	{
        	middle = (top + bottom) / 2;

            if (needle < haystack[middle])
            {
            	top =  middle -1;
            }
            else if (needle > haystack[middle])
            {
                bottom = middle +1;
            }
            else if (needle == haystack[middle])
            {
                return true;
            }
    	}
    }
    return false; 
}

int main(void)
{
    int numbers[SIZE] = { 4, 8, 15, 16, 23, 42, 50, 108 };
    printf("> ");
    int n = GetInt();
        
    }
    if (search(n, numbers, SIZE))
    {
        printf("YES\n");
    }
    else 
    {
        printf("NO\n");
    }
}
