/**
 * Program to illustrate a recursive function to calculate the factorial of a positive integer
 * 
 * Factorial:
 * factorial (3)    = 3 * factorial (2)
 *                  = 3 * 2 * factorial (1)
 *                  = 3 * 2 * 1 * factorial (0)
 *                  = 3 * 2 * 1 * 1
 *                  = 6
 * 
 **/
 
#include <stdio.h>
long int factorial (long int n);

int main (void)
{
    long int number = 0;
    
    printf("Please provide a positive int: \n");
    
    scanf("%li", &number);
    
    for (int i = 0 ; i <= number ; i++)
    {
        
        printf("the fatcorial for %2i! is: %li\n",i, factorial(i));
    }
}
// Recursive function to calculate the factorial of a positive integer

long int factorial (long int n)
{
    long int result;
    
    if (n == 0)
    {
        result = 1;
        return result;
    }
    else
    {
        result = n * factorial (n - 1);
        return result;
    }
}