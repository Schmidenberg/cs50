#include <cs50.h>
#include <stdio.h>

#define SIZE 8

// boolean search prototye
bool search(int needle, int haystack[], int size);
void sort(int array[], int size);

int main(void)
{
    int numbers[SIZE];
    printf("Provide %i Numbers to store:\n", SIZE - 1);
    
    for (int i = 0; i < SIZE - 1; i ++)
    {
        numbers[i] = GetInt();
    }
    
    // sort array function
    sort(numbers, SIZE);
    
    // number to search for
    printf("Provide numbers to search for:\n");
    int n = GetInt();
        
    if (search(n, numbers, SIZE))
    {
        printf("YES\n");
    }
    else 
    {
        printf("NO\n");
    }
}
/******************************************************************/

// sort the array
void sort(int array[], int size)
{
    int swap;
    
    do
    {
        swap = 0;
        for (int i = 0; i < size-1; i++)
        {
            if (array[i] > array[i+1])
            {
                int temp = array[i];
                array[i] = array[i+1];
                array[i+1] = temp;
                swap++;
            }
        }
    }
    while (swap != 0);
}

/******************************************************************/

// boolean search function
bool search(int needle, int haystack[], int size)
{
    
    int middle = 0;
    int bottom = 0;
    int top = size -1;
	    
    while (bottom <= top)
    {
        for (int i = 0; i < size -1; i++)
    	{
        	middle = (top + bottom) / 2;

            if (needle < haystack[middle])
            {
            	top =  middle -1;
            }
            else if (needle > haystack[middle])
            {
                bottom = middle +1;
            }
            else if (needle == haystack[middle])
            {
                return true;
            }
    	}
    }
    return false; 
}