/**
 * tests the 'conditional operator' (ternary operator) expression
 *
 * i.e. = ? :
 * 
 * again, trying to implement as prototype and function
**/
 
#include <stdio.h>
#include <stdbool.h>

//
int bool_res(int i, int j);

int main(void)
{
    printf("Type in two integers to determine if");
    printf("the first is evenly divisible by the second\n");
    
    int first, second;
    
    scanf("%i %i", &first, &second);
    
    bool_res(first, second); 

}

// function to return result
int bool_res(int i, int j)
{
    bool n = (i % j == 0) ? 1 : 0;
    
    if (n)
    {
        return printf("YES, it is\n");
    }
    else
    {
        return printf("NO, it is not\n");
    }
}