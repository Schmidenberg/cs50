#include <stdio.h>

int fact(int i)
{ 
    int j,k; 
    j = 1; 
    
    for (k=2; k<=i; k++) 
    {
        j = j * k; 
    }
    return j; 
}

int main(void)
{
    int j = 10;
    printf("%i\n",fact(j));
}