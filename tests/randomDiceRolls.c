#include<stdio.h>
#include <stdlib.h>
#include<time.h>

int main(void)
{
    int array[6];
    for ( int i = 0; i < 6; i++)
    {
        array[i] = 0;
    }
    
    time_t t;
    srand(time(&t));
    int sum = 0;
    
    
    for ( int i = 1; i <= 1000000000; i++, sum++)
    {
        int dice = (rand() % 6) + 1; /* From 1 to 6 */
        ++array[dice];
    }
    
    printf ("\nRating Number of Responses\n");
    printf ("------ -------------------\n");
    
    for (int i = 1; i <= 6; i++)
    {
        printf("%i\t%i\n",i , array[i]);
    }
    printf("Total\t%i\n", sum);
}
