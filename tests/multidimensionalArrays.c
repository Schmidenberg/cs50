#include<stdio.h>

#define ROW 3
#define COLUMN 5
#define MULTI_X 2

void displayMatrix(int matrix[ROW][COLUMN]);
void alteredMatrix(int matrix[ROW][COLUMN], int multi);

int main(void)
{
    int originalArray[ROW][COLUMN] = 
    {
        { 7, 16, 55, 13, 12 },
        { 12, 10, 52, 0, 7 },
        { -2, 1, 2, 4, 9 }
    };
    
    printf ("Original matrix:\n");
    displayMatrix(originalArray);
    printf ("Altered matrix:\n");
    alteredMatrix(originalArray, MULTI_X);
}

void displayMatrix(int matrix[ROW][COLUMN])
{
    for (int i = 0; i < ROW; i++)
    {
        for (int j = 0; j < COLUMN; j++)
        {
            printf("%10i", matrix[i][j]);
        }
        printf("\n");
    }
}

void alteredMatrix(int matrix[ROW][COLUMN], int multi)
{
    for (int i = 0; i < ROW; i++)
    {
        for (int j = 0; j < COLUMN; j++)
        {
            printf("%10i", matrix[i][j] *= multi);
        }
        printf("\n");
    }
}

