#include <stdio.h>
/**
 * a test of a variable length array
 * 
 * 
 **/
 
int main(void)
{
    int variableArray[] = {1, 2, [9] = 3};
    
    printf("int variableArray[] = {1, 2, [9] = 3};\n\n");
    for (int i = 0; i < 10; i++)
    printf("Element Postition: %2i\t=\t%i\n",i, variableArray[i]);
}