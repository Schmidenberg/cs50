// Program to convert a positive integer to another base
#include <stdio.h>

void getbase(void);
void convert(void);
void displayNumber(void);


int main (void)
{
int nextDigit, base, index = 0;

    getbase ();
    convert ();
    displayNumber ();
    
    return 0;
}    
 
    // get the number and the base
void getBase(void)
{
    long int numberToConvert;
    printf ("Number to be converted? ");
    scanf ("%ld", &numberToConvert);
    printf ("Base? ");
    scanf ("%i", &base);
}
    
    // convert to the indicated base
void convert(void)
{
    int convertedNumber[64];
    do 
    {
        convertedNumber[index] = numberToConvert % base;
        ++index;
        numberToConvert = numberToConvert / base;
    }
    while ( numberToConvert != 0 );
}    
    // display the results in reverse order
void displayNumber(void)
{
    const char baseDigits[16] = {'0', '1', '2', '3', '4', '5', '6',
                                    '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    
    printf ("Converted number = ");
    for (--index; index >= 0; --index ) 
    {
        nextDigit = convertedNumber[index];
        printf ("%c", baseDigits[nextDigit]);
    }
    printf ("\n");
}