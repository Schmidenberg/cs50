#include <stdio.h>

int main(void)
{
    printf("\tn\t-\tn^2");
    printf("\n\t-\t-\t---");
    
    for (int n = 1; n <=10; n++)
    {
        printf("\n\t%i\t-\t%i", n, n*n);
    }
    printf("\n");
}