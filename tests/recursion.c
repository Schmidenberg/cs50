/**
 * A recursive funtion using a protoype and function
 * 
 * By Wazza
 * 
 * To define a recursive function, you have to do three things: 
 * Define what the function does. 
 * In this case it is printing numbers from 1 up to n.
 * 
 * Define what the recursive call is. What happens the next time around? 
 * The easiest way is to think from the bottom up; in this case, on each earlier line, it is printing numbers up to one less than the previous. 
 * Therefore, every time you call the function again, you want to call it with one less than the previous number.
 * 
 * Define your stop condition. When should I stop recursing? 
 * In this case, once you hit the number 1, this will be your last iteration. This means, we want to call the recursive function until this stop condition is reached - or in other words, while n is greater than 1.
 */

#include <stdio.h>

unsigned long int factorial(unsigned long int n);

int main(void)
{
    unsigned long int i;
    unsigned long int number = 0;
    
    printf("give positive number:\n");
    scanf("%lu!", &number);
    printf("  int   =   Factorial\n---------------------\n");
    
    for (i = 0; i <= number; i++)
    {
        printf("%4lu!   =   %lu\n", i, factorial(i));
    }
}

//recursive function
unsigned long int factorial(unsigned long int n)
{
    int result;
    
    if (n == 0)
    {
        result = 1;
    }
    else
    {
        result = n * factorial(n - 1);
    }
    return result;
}