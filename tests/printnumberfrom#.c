#include <stdio.h>

int main(void)
{
    // ask for an int
    int num;
    int remain = 0;

    printf("Give me an int: \n");
    scanf("%i", &num);
    
    // reverse numbers before switch (so as to not print backwards)
    int reverse = 0;

    for (int i = 0; num > 0; i++)
    {
        // problem when using 0 in number
        remain = num % 10;
        
        if (remain == 0)
        {
            
        }
        else
        {
            num /= 10;
            reverse = reverse * 10 + remain;   
        }
    }

    if (reverse == 0)
    {
        printf("Zero ");
    }
    else 
    {
        while (reverse > 0)
        {
            remain = reverse % 10;
            reverse /= 10;

            switch (remain)
            {
                case (9) : printf("Nine ");
                break;
                
                case (8) : printf("Eight ");
                break;
                
                case (7) : printf("Seven ");
                break;
                
                case (6) : printf("Six ");
                break;
                
                case (5) : printf("Five ");
                break;
                
                case (4) : printf("Four ");
                break;
                
                case (3) : printf("Three ");
                break;
                
                case (2) : printf("Two ");
                break;
                
                case (1) : printf("One ");
                break;
    
                default : printf("Zero ");
            }
        }
    }
    printf("\n");
}

