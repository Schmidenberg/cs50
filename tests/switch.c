/**
 * Write a program that prompts the user to input the names of five Pokemon. Store those Pokemon in an array, and randomly select one to print out.
 * HINT: Don’t reinvent the wheel—a function already exists that will return a random number!
 * 
 * jharvard@run.cs50.net (~): ./a.out
 * 
 * Give me a Pokemon: Butterfree
 * Give me a Pokemon: Clefairy
 * Give me a Pokemon: Diglett
 * Give me a Pokemon: Growlithe
 * Give me a Pokemon: Rapidash
 * Clefairy, I choose you!
 * 
 */

#include <cs50.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define MAX 5

int main(int argc, string argv[])
{
    string name[MAX] = {"Warren", "Taryn", "Scotch", "Sarge", "Mumskie"};
    
    printf("Gimme names: \n ... Wow, that was fast\n");

    srand(time(NULL));
    int warren, taryn, scotch, sarge, mumskie;
    warren = taryn = scotch = sarge = mumskie = 0;

    do
    {
        int count = rand() % 5;
        printf("names back: %s\n", name[count]);
        
        switch(count)
        {
            case (0) : taryn = 1;
            break;
            
            case (1) : scotch = 1;
            break;
            
            case (2) : sarge = 1;
            break;
            
            case (3) : mumskie = 1;
            break;
            
            case (4) : warren = 1;
            break;
            
            default : break;
        }
    }
    while (taryn == 0 || scotch == 0 || sarge == 0 || mumskie == 0 || warren == 0);
}

