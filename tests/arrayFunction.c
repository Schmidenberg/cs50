#include <stdio.h>

#define MAX 10

int main(void)
{
    int minimum(int values[MAX]);

    int takeNumber[MAX];
    printf("Provide 10 ints: \n");

    for (int i = 0; i < MAX; i++)
    {
        scanf("%i", &takeNumber[i]);
    }
    
    int minValue = minimum(takeNumber);
    printf("The minimun value is: %i\n", minValue);
}

int minimum(int values[MAX])
{
    int min = values[0];
    
    for (int i = 1; i < MAX; i++)
    {
        if (values[i] <= min)
        {
            min = values[i];
        }
    }
    return min;
}