/**
 * 
 * Calculates the differnece between two dates - uses Structs
 *  
 **/
#include <stdio.h>
int main (void)
{
    struct dates
    {
        int month;
        int day;
        int year;
    };
    
    struct dates dateRange[2];
    
    // populate the two dates
    for (int i = 0 ; i < 2; i++)
    {
        printf("Please provide date number %i (format: m d yyyy):\n", i + 1);
        scanf("%i%i%i", &dateRange[i].month, &dateRange[i].day, &dateRange[i].year);
        
        if (dateRange[i].month <= 2)
        {
            dateRange[i].year -= 1;
            dateRange[i].month += 13;
        }
        else
        {
            dateRange[i].month += 1;
        }
        
        //printf("%i %i %i\n", dateRange[i].month, dateRange[i].day, dateRange[i].year);
    }
    
    // calculate the result
    long int dateOneCalc = 0;
    long int dateTwoCalc = 0;
    
    //for (int i = 0 ; i < 2; i++)
    {
        dateOneCalc = (1461 * dateRange[0].year / 4) + 
                                (153 * dateRange[0].month / 5 + 3);
        dateTwoCalc = (1461 * dateRange[1].year / 4) + 
                                (153 * dateRange[1].month / 5 + 21);                    
    }
    
    printf("Days elapsed: %li\n", dateTwoCalc - dateOneCalc);

}