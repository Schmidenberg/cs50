#include <stdio.h>
long int x_to_the_n (int x, int n);

int main (void)
{
    int init = 0;
    int raise_num = 0;
    
    printf("Please provide an inital int: \n");
    scanf("%i", &init);
    printf("Please provide a number to raise intial to the power of: \n");
    scanf("%i", &raise_num);
    
    long int result = x_to_the_n(init, raise_num);
    printf("The number %i raised to the power of %i is: %li\n",init, raise_num, result);
}
// Recursive function to calculate the factorial of a positive integer

long int x_to_the_n (int x, int n)
{
    long int result = 1;
    
    for (int j = 0 ; j < n; j++)
    {
        result *= x;
    }
    return result;
}