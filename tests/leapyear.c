// Program to determine if a year is a leap year
#include <stdio.h>
int main (void)
{
int year, rem_4, rem_100, rem_400;
printf ("Enter the year to be tested: ");
scanf ("%i", &year);
rem_4 = year % 4;
rem_100 = year % 100;
rem_400 = year % 400;
if ( (rem_4 == 0 && rem_100 != 0) || rem_400 == 0 )
printf ("It's a leap year.\nREM_4: %i\nREM_100: %i\nREM_400: %i\n", rem_4, rem_100, rem_400);
else
printf ("Nope, it's not a leap year.\n\nREM_4: %i\nREM_100: %i\nREM_400: %i\n", rem_4, rem_100, rem_400);
return 0;
}