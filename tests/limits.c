#include <stdio.h>
#include <limits.h>

int main(void)
{
    unsigned long long int ullm = ULLONG_MAX;
    
    printf("An Unsigned Long Long has a Max value of: %llu\n", ullm);
}