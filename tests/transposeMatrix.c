/**
 * This particular program took me a while, but it works..
 * 
 * Items to note:
 * passing variables into arrays: variable need to be constants - 
 * this seems to be in particualr with multis
 * 
 * array functions - call to function, note syntax
 * 
 * by Warren R. Schimdenberg
 * 
 **/

#include <stdio.h>

#define S 4
#define B 5

void transpose(int currentMatrix[S][B], int nextMatrix[B][S]);

int main(void)
{
    int originalMatrix[S][B] =
    {
        { 1, 2, 3, 4, 5},
        { 6, 7, 8, 9, 10},
        { 11, 12, 13, 14, 15},
        { 16, 17, 18, 19, 20}
    };
    int nextMatrix[B][S];
    
    printf("Original Matrix: \n\n");
    for (int i = 0; i < S; i++)
    {
        for (int j = 0; j < B; j++)
        {
            printf("%3i ", originalMatrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    
    printf("----------------------\n");
    
    transpose(originalMatrix, nextMatrix);

    printf("New Matrix: \n\n");
    for (int i = 0; i < B; i++)
    {
        for (int j = 0; j < S; j++)
        {
            printf("%3i ", nextMatrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    
    printf("----------------------\n");
    
}

void transpose(int currentMatrix[S][B], int nextMatrix[B][S])
{
    for (int i = 0; i < B; i++)
    {
        for (int j = 0; j < S; j++)
        {
            nextMatrix[i][j] = currentMatrix[j][i];
        }
    }
}