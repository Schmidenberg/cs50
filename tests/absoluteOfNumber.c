// Program to calculate the absolute value of a float - using a function
#include <stdio.h>

float absoluteResult(float i);

int main (void)
{
    float number;
    printf ("Type in your floating point number: ");
    scanf ("%f", &number);
    
    float result = absoluteResult(number);

    printf ("The absolute value is %0.2f\n", result);
}

/************************************/

float absoluteResult(float i)
{
    if ( i < 0 )
    {
        i = -i;
    }
    return i;
}