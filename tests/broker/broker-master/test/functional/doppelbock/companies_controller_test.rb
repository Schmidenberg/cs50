require 'test_helper'

class Doppelbock::CompaniesControllerTest < ActionController::TestCase
  setup do
    @doppelbock_company = doppelbock_companies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:doppelbock_companies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create doppelbock_company" do
    assert_difference('Doppelbock::Company.count') do
      post :create, doppelbock_company: { annual_net_profit: @doppelbock_company.annual_net_profit, annual_turnover: @doppelbock_company.annual_turnover, current_owner: @doppelbock_company.current_owner, description: @doppelbock_company.description, email: @doppelbock_company.email, location_id: @doppelbock_company.location_id, mobile: @doppelbock_company.mobile, name: @doppelbock_company.name, price: @doppelbock_company.price, primary_sector: @doppelbock_company.primary_sector, primary_vertical_id: @doppelbock_company.primary_vertical_id, registration_number: @doppelbock_company.registration_number, secondary_vertical_id: @doppelbock_company.secondary_vertical_id, stock: @doppelbock_company.stock, total_assets: @doppelbock_company.total_assets, trading_as: @doppelbock_company.trading_as }
    end

    assert_redirected_to doppelbock_company_path(assigns(:doppelbock_company))
  end

  test "should show doppelbock_company" do
    get :show, id: @doppelbock_company
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @doppelbock_company
    assert_response :success
  end

  test "should update doppelbock_company" do
    put :update, id: @doppelbock_company, doppelbock_company: { annual_net_profit: @doppelbock_company.annual_net_profit, annual_turnover: @doppelbock_company.annual_turnover, current_owner: @doppelbock_company.current_owner, description: @doppelbock_company.description, email: @doppelbock_company.email, location_id: @doppelbock_company.location_id, mobile: @doppelbock_company.mobile, name: @doppelbock_company.name, price: @doppelbock_company.price, primary_sector: @doppelbock_company.primary_sector, primary_vertical_id: @doppelbock_company.primary_vertical_id, registration_number: @doppelbock_company.registration_number, secondary_vertical_id: @doppelbock_company.secondary_vertical_id, stock: @doppelbock_company.stock, total_assets: @doppelbock_company.total_assets, trading_as: @doppelbock_company.trading_as }
    assert_redirected_to doppelbock_company_path(assigns(:doppelbock_company))
  end

  test "should destroy doppelbock_company" do
    assert_difference('Doppelbock::Company.count', -1) do
      delete :destroy, id: @doppelbock_company
    end

    assert_redirected_to doppelbock_companies_path
  end
end
