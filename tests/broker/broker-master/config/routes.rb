AllianceBrokers::Application.routes.draw do

  resources :adverts, :only => [:index, :show]

  Doppelbock::Engine.routes.draw do
    resources :adverts, :companies
    match 'advertdata' => 'adverts#jsondata', :via => [:get], :as => 'adverts_data'
  end

  mount Doppelbock::Engine, :at => "admin"

  # This line mounts Refinery's routes at the root of your application.
  # This means, any requests to the root URL of your application will go to Refinery::PagesController#home.
  # If you would like to change where this extension is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Refinery relies on it being the default of "refinery"
  mount Refinery::Core::Engine, :at => '/'

end
