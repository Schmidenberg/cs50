require "rvm/capistrano"
require "bundler/capistrano"

load "config/recipes/base"
load "config/recipes/nginx"
load "config/recipes/unicorn"
load "config/recipes/check"

server "151.236.221.213", :web, :app, :db, primary: true

set :user, "admin"
set :application, "alliancecapital"
set :deploy_to, "/control/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, "git"
set :repository, "git@github.com:SDC2012/broker.git"
set :branch, "master"

default_run_options[:pty] = true

ssh_options[:forward_agent] = true
ssh_options[:port] = 25000

after "deploy", "deploy:cleanup" # keep only the last 5 releases
