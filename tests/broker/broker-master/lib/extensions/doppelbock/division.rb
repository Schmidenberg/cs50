module Extensions
  module Doppelbock
    module Division
      extend ActiveSupport::Concern

      # Class methods
      included do

      end

      # *** INSTANCE METHODS *** #

      def top_adverts(count)
        ::Doppelbock::Advert.top_by_division(self.id, count)
      end

    end
  end
end