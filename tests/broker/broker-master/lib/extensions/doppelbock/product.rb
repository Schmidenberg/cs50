module Extensions
  module Doppelbock
    module Product
      extend ActiveSupport::Concern

      # Class methods
      included do
        attr_accessible :name, :trading_as, :registration_number, :price, :current_owner, :primary_sector,
                        :description, :summary, :annual_net_profit, :total_assets, :stock, :annual_turnover,
                        :document, :company_type

        has_many :adverts, :class_name => "::Doppelbock::Advert", :dependent => :destroy

        validates_presence_of :name, :trading_as, :mobile, :email, :company_type, :summary

        mount_uploader :document, ::Doppelbock::DocumentUploader do

          include CarrierWave::MiniMagick

          process :resize_to_fill => [130,130] # See http://carrierwave.rubyforge.org/rdoc/classes/CarrierWave/MiniMagick.html for more options

          #this is how it normally works but
          #versions :thumb do
          #  process :resize_to_fill => [130,130] # See http://carrierwave.rubyforge.org/rdoc/classes/CarrierWave/MiniMagick.html for more options
          #end


          def store_dir
            ::Doppelbock::Settings.product_image_path || 'product_image_documents/'
          end

          def extension_white_list
            %w(jpg jpeg gif png bmp)
          end
        end

        class << self
        end
      end

    end
  end
end
