module Extensions
  module Doppelbock
    module Advert
      extend ActiveSupport::Concern

      # Class methods
      included do
        attr_accessible :product_id
        validates_presence_of :product_id

        belongs_to :product, :class_name => "::Doppelbock::Product"
        scope :with_product, includes(:product)
        scope :join_vertical, joins(:product).joins('INNER JOIN doppelbock_verticals ON doppelbock_verticals.id = doppelbock_products.vertical_id')
        scope :join_location, joins(:product).joins('INNER JOIN doppelbock_locations ON doppelbock_locations.id = doppelbock_products.location_id')
        scope :top, lambda { |count| active.latest.limit(count) }
        scope :top_by_division, lambda {|division_id, count| by_division(division_id).top(count) }
        scope :by_price_range, lambda { |upper, lower| where('price >= ? AND price <= ?', upper, lower)}
        scope :by_vertical, lambda { |vertical_id| joins(:product).where(:doppelbock_products => {:vertical_id => vertical_id}) }
        scope :by_location, lambda { |location_id| joins(:product).where(:doppelbock_products => {:location_id => location_id}) }
        scope :by_division, lambda { |division_id|
          joins(:product).joins('INNER JOIN doppelbock_verticals ON doppelbock_verticals.id = doppelbock_products.vertical_id').where(:doppelbock_verticals => {:division_id => division_id})
        }

        delegate :name, :email, :mobile, :location, :trading_as, :registration_number, :price, :current_owner, :image,
                 :primary_sector, :description, :annual_net_profit, :total_assets, :stock, :annual_turnover, :vertical,:company_type,
                 :to => :product, :prefix => 'product'

        class << self

          def search(params)
            query = self.with_product.join_vertical.join_location.active

            if params[:vertical_id].present?
              query = query.by_vertical(params[:vertical_id])
            elsif params[:division_id].present?
              query = query.by_division(params[:division_id])
            end

            query = query.by_location(params[:location_id]) if params[:location_id].present?

            if params[:price_range].present?
              ranges = params[:price_range].split('..')
              query = query.by_price_range(ranges.first.try(:to_i), ranges.last.try(:to_i))
            end

            order_by = (params[:order_by].present?) ? "doppelbock_#{params[:order_by]}" : 'doppelbock_products.name'
            order_dir = (params[:order_dir].present?) ? params[:order_dir] : 'asc'

            query.order("#{order_by} #{order_dir}").paginate(:page => params[:page])
          end

          def per_page
            5
          end

          def for_admin_user(admin_user)
            by_division(admin_user.divisions.collect {|d| d.id})
          end
        end
      end

      # *** INSTANCE METHODS *** #

      # URL Rewriting
      def to_param
        "#{id}-#{product_company_type}"
      end

    end
  end
end