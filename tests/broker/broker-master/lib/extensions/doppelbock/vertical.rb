module Extensions
  module Doppelbock
    module Vertical
      extend ActiveSupport::Concern

      # Class methods
      included do

        has_many :products, :class_name => "::Doppelbock::Product"
        has_many :adverts, :class_name => "::Doppelbock::Advert", :through => :products

        class << self
        end
      end

      # *** INSTANCE METHODS *** #

      def name_with_advert_count
        "#{name} (#{adverts.active.count})"
      end

    end
  end
end