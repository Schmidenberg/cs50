class CustomFailure < Devise::FailureApp

  # Redirect to root path (/admin) which will ask for signin
  def redirect_url
     doppelbock.root_path
  end

  # Override respond method to eliminate recall
  def respond
    http_auth? ? http_auth : redirect
  end

end
