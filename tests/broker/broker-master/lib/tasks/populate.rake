namespace :db do

  # call this task by running: rake db:populate
  desc "Dummy data for Companies, Adverts etc."
  task :populate => :environment do
    require 'populator'

    LINETERM  = "-----------------------------------------------------"

    # Remove configured table data
    [Doppelbock::Product, Doppelbock::Advert].each(&:delete_all)

    Doppelbock::Product.populate 10 do |company|
      company.name = Populator.words(1..3).titleize
      company.trading_as = Populator.words(1..3).titleize
      company.mobile = (100000..200000).to_s
      company.email = "#{Populator.words(1)}@example.com"
      company.registration_number = Populator.words(1)
      company.price = 100000..900000
      company.current_owner = Populator.words(1..2).titleize
      company.primary_sector = Populator.words(1..3).titleize

      offset = rand(::Doppelbock::AdminUser.count)
      company.admin_user_id = ::Doppelbock::AdminUser.first(:offset => offset)

      offset = rand(::Doppelbock::Location.count)
      company.location_id = ::Doppelbock::Location.first(:offset => offset)

      offset = rand(::Doppelbock::Vertical.count)
      company.vertical_id = ::Doppelbock::Vertical.first(:offset => offset)

      company.description = Populator.sentences(2..3)
      company.annual_net_profit = 100000..900000
      company.total_assets = 100000..900000
      company.stock = 100000..900000
      company.annual_turnover = 100000..900000
    end

    puts "Companies populated."
    puts LINETERM

    ::Doppelbock::Product.all.each do |c|
      ::Doppelbock::Advert.populate 1 do |a|
        a.product_id = c.id
        a.admin_user_id = ::Doppelbock::AdminUser.first.id
        a.ref_number = Populator.words(1)
        a.active = [true, false]
      end
    end

    puts "Adverts populated."
    puts LINETERM

  end
end