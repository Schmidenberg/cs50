class AdvertsController < ApplicationController
  before_filter :find_page

  def index
    @adverts = ::Doppelbock::Advert.search(params)
    @page.title = "Businesses for sale"
    present(@page)
  end

  def show
    @advert = ::Doppelbock::Advert.find(params[:id])
    @page.title = @advert.product.company_type
    present(@page)
  end

  def find_page
    @page = ::Refinery::Page.where(:link_url => "/adverts").first
  end

end
