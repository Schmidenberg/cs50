require_dependency "doppelbock/application_controller"

module Doppelbock
  class AdvertsController < ApplicationController
    include ::Doppelbock::InheritedResources::Defaults
    load_and_authorize_resource :class => "Doppelbock::Advert"

    # This should be in index method, keep until resolved.
    def jsondata
      respond_to do |format|
        format.json {
          query = Advert.with_product.with_admin_user.for_admin_user(current_doppelbock_user)
          query = query.by_vertical(params[:vertical_id]) if params[:vertical_id]
          render :json => AdvertsDatatable.new(
            view_context, query, { :admin_user => current_doppelbock_user }
          )
        }
      end
    end

    def create
      @advert = Advert.new(params[:advert])
      @advert.admin_user_id = current_doppelbock_user.id
      create!
    end
  end
end
