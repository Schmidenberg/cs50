# Handle json requests from Divisions Controller which originate from
# jQuery datatable via Ajax.
module Doppelbock
  class AdvertsDatatable < CoreDatatable

    def initialize(view, object, options={})
      @user = options[:admin_user]
      super(view, object)
    end

    protected

    def field_setup
      @fields = %w[doppelbock_products.name doppelbock_admin_users.name doppelbock_adverts.ref_number doppelbock_adverts.active]
    end

    def data
      dataset.map do |advert|
        [
            advert.product.name,
            advert.admin_user.name,
            advert.ref_number,
            status_label(advert.active),
            tooltip_text(_summary(advert.product.summary), advert.product.summary),
            modal_dialog(
              advert.id,
              raw(advert.product.description),
              'Description',
              'icon-align-justify'
            ),
            @user.has_role?(:admin) ? view_edit_del([advert]) : view_edit([advert])
        ]
      end
    end

    private

    # Truncate summary to specified length (place ellipses at end)
    def _summary(text)
      truncate(text, :length => 30)
    end
  end
end
