# Handle json requests from Candidates Controller which originate from
# jQuery datatable via Ajax.
module Doppelbock
  class ShotgunsDatatable < CoreDatatable

    delegate :number_to_currency, :button_to, :image_tag, to: :@view

    def initialize(view, object)
      super(view, object)
    end

    protected

    def field_setup
      @fields = %w[
        doppelbock_products.name
        doppelbock_products.price
        doppelbock_verticals.name
        doppelbock_admin_users.name
        doppelbock_products.shotgun_date
      ]
    end

    def data
      dataset.map do |product|
        [
            product.name,
            number_to_currency(product.price, :precision => 2, :unit => 'R', :format => "%u %n"),
            product.vertical.name,
            product.shotgun_admin_user.try(:name),
            product.shotgun_date.present? ? datetime(product.shotgun_date) : shotgun_button(product)
        ]
      end
    end

    private

    def shotgun_button(product)
      text = 'Shotgun Company'
      link_to(
        text,
        {
          :controller => 'products',
          :action => 'shotgun',
          :id => product
        },
        :method  => :post,
        :confirm => "#{text} #{product.name}?",
        :remote  => true,
        :rel => 'tooltip',
        :title => product.shotgun_release_date.present? ? 'Expired: ' + datetime(product.shotgun_release_date).to_s : nil,
        :style => product.shotgun_release_date.present? ? 'font-weight: bold;' : nil
      )
    end

  end
end
