# Handle json requests from Divisions Controller which originate from
# jQuery datatable via Ajax.
module Doppelbock
  class ProductsDatatable < CoreDatatable

    def initialize(view, object, options={})
      @user = options[:admin_user]
      super(view, object)
    end

    protected

    def field_setup
      @fields = %w[
        doppelbock_products.name
        doppelbock_products.primary_sector
        doppelbock_products.price
        doppelbock_locations.name
      ]
    end

    def data
      dataset.map do |product|
        [
            product.name,
            product.primary_sector,
            number_currency(product.price),
            product.location.name,
            @user.has_role?(:admin) ? view_edit_del([product]) : view_edit([product])
        ]
      end
    end

  end
end
