# jQuery ready function called when DOM loaded and initialized
#   - Setup jQuery DataTable for Branch listing (http://www.datatables.net/api)
jQuery ->

  selector = '#products'

  # Configure and initialize  datatable using ajax json datasource
  # @see http://www.datatables.net/usage/options
  products_table =
    iDisplayLength: $(selector).data('pagesize')
    sAjaxSource: $(selector).data('source')
    fnServerParams: (aoData) ->
      vertical_id = $.getUrlVar('vertical_id')
      aoData.push( { "name": "vertical_id", "value" : vertical_id } ) if vertical_id
    aoColumns: [
      null
      null
      null
      null
      {
        bSortable : false
        sClass : 'center'
      }
    ]

  # Add datatable implementation to global list of datatables to be rendered
  datatables.append
    selector: selector
    datagrid: products_table
