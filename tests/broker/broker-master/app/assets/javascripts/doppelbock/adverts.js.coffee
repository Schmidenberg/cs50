# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->

  selector = '#adverts_intranet'

  # Configure and initialize  datatable using ajax json datasource
  # @see http://www.datatables.net/usage/options
  adverts_table =
    iDisplayLength: $(selector).data('pagesize')
    sAjaxSource: $(selector).data('source')
    fnServerParams: (aoData) ->
      vertical_id = $.getUrlVar('vertical_id')
      aoData.push( { "name": "vertical_id", "value" : vertical_id } ) if vertical_id
    aoColumns: [
      null
      null
      null
      { sClass: 'center' }
      {
        bSortable: false
      }
      {
        bSortable: false
        sClass: 'center'
      }
      {
        bSortable: false
        sClass: 'center'
      }
    ]

  # Add datatable implementation to global list of datatables to be rendered
  datatables.append
    selector: selector
    datagrid: adverts_table
