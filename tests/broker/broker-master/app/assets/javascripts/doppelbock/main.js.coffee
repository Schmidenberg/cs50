# jQuery ready function called when DOM loaded and initialized
#   - Initialize all user interface controls
#   - Render all jQuery DataTables

jQuery ->

  # Initialize all user interface controls
  uicontrols.initialize()

  # Render all jQuery DataTables that are specific to a page (not all at once)
  datatables.render()
