module ApplicationHelper

  # Return a footer sub menu item, based on a given page
  def footer_sub_menu(title)
    page = Refinery::Page.where(:parent_id => nil).find_by_title(title)

    if page.present?
      list_items = []

      if page.children.present?
        page.children.where(:show_in_menu => true).each do |child|
          list_items << content_tag('li', link_to(child.title, refinery.url_for(child.url)))
        end
      end

      content_tag('div', content_tag('h3', page.title) + content_tag('ul', raw(list_items.join)), :class => 'span3')
    end

  end

  def active_divisions
    @divisions ||= ::Doppelbock::Division.active.with_verticals.order(:name)
  end

  def top_adverts(count)
    @top_adverts ||= ::Doppelbock::Advert.active.with_product.top(count)
  end

  def tab_name(division)
    division.name.gsub(' ', '_')
  end

  def rands(amount)
    number_to_currency(amount, :unit => 'R')
  end

  def rands_without_cents(amount)
    number_to_currency(amount, :unit => 'R', :precision => 0)
  end

  def vertical_options(divisions)
    options = []
    divisions.each do |d|
      d.verticals.order(:name).each do |v|
        selected_hash = (params[:vertical_id] == v.id.to_s) ? {:selected => 'selected'} : {}
        options << content_tag('option', v.name, {:value => v.id, :class => d.id}.merge(selected_hash))
      end
    end
    options.join
  end

  def price_range_options
    options_for_select([['< R100 000', '0..100000'],
                       ['R100 000 - R500 000', '100000..500000'],
                       ['R500 000 - R1 000 000', '500000..1000000'],
                       ['R1 000 000 - R5 000 000', '1000000..5000000'],
                       ['> R5 000 000', '5000000..999000000']], params[:price_range])
  end

  def location_options
    option_groups_from_collection_for_select(::Doppelbock::Region.order(:name), :locations, :name, :id, :name, params[:location_id])
  end

end
