module Doppelbock
  module AdvertsHelper
      def adverts_page_title(vertical_id=nil)
        vertical_id.present? ? "View All Adverts In #{Vertical.find(vertical_id).name}" : "View All Adverts"
      end
  end
end
