module AdvertsHelper

  # Returns a link allowing the sorting of the job results
  def adverts_column_header(name, display_name)

    parameters = Hash.new

    [:vertical_id, :location_id, :division_id, :price_range].each do |p|
      parameters[p] = params[p] if params[p].present?
    end

    parameters[:order_by] = name
    parameters[:order_dir] = (params[:order_dir].present? and params[:order_dir] == 'asc') ? 'desc' : 'asc'

    link_to(display_name, main_app.adverts_path(parameters))
  end

  def advert_table_row(label, value)
    if value.present?
      content_tag('tr', content_tag('td', label) + content_tag('td', value))
    end
  end


end
