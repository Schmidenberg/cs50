# W A R N I N G
#::Refinery::Page.delete_all

home_page = ::Refinerycms::PageSeeder::Seeder.create_page('Home', :deletable => false, :link_url => "/")
if home_page.present?
  page_not_found_page = home_page.children.create(:title => "Page not found", :menu_match => "^/404$", :show_in_menu => false, :deletable => false)
  page_not_found_page.parts.create({:title => "Body", :body => "<h2>Sorry, there was a problem...</h2><p>The page you requested was not found.</p><p><a href='/'>Return to the home page</a></p>", :position => 0 })
end

::Refinerycms::PageSeeder::Seeder.create_parent_and_children('Buying', ['Preparing to Buy', 'Consolidation Strategy', 'Growing Your National Presence', 'Creating a Regional Presence', 'BBBEE / BEE Strategy', 'Taxation'])
::Refinerycms::PageSeeder::Seeder.create_parent_and_children('Selling', ['Preparing to Sell', 'Valuation', 'Mining Rights', 'Taxation'])
::Refinerycms::PageSeeder::Seeder.create_parent_and_children('About Us', ['Our Service', 'EAAB Certificate', 'BEE Certificate'])
::Refinerycms::PageSeeder::Seeder.create_page('Portfolio', :link_url => "/portfolio", :deletable => false, :show_in_menu => false)

contact_us_page = ::Refinerycms::PageSeeder::Seeder.create_page('Contact', :link_url => "/contact", :menu_match => "^/(inquiries|contact).*$", :deletable => false, :show_in_menu => true)
if contact_us_page.present?
  thank_you_page = contact_us_page.children.create({:title => "Thank You", :link_url => "/contact/thank_you", :menu_match => "^/(inquiries|contact)/thank_you$", :show_in_menu => false, :deletable => false})
  thank_you_page.parts.create({:title => "Body", :body => "<p>We've received your inquiry and will get back to you with a response shortly.</p><p><a href='/'>Return to the home page</a></p>"})
end

::Refinerycms::PageSeeder::Seeder.create_page('Adverts', :link_url => "/adverts", :deletable => false, :show_in_menu => false)