::Refinery::User.all.each do |user|
  if user.plugins.where(:name => 'refinery_inquiries').blank?
    user.plugins.create(:name => "refinery_inquiries")
  end
end if defined?(::Refinery::User)

  if defined?(::Refinery::User)
    ::Refinery::User.all.each do |user|
      if user.plugins.where(:name => 'refinerycms_blog').blank?
        user.plugins.create(:name => 'refinerycms_blog')
      end
    end
  end