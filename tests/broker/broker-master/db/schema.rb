# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130315085448) do

  create_table "doppelbock_admin_users", :force => true do |t|
    t.string   "name"
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "branch_id"
    t.string   "username"
  end

  add_index "doppelbock_admin_users", ["email"], :name => "index_doppelbock_admin_users_on_email", :unique => true
  add_index "doppelbock_admin_users", ["reset_password_token"], :name => "index_doppelbock_admin_users_on_reset_password_token", :unique => true

  create_table "doppelbock_admin_users_doppelbock_divisions", :id => false, :force => true do |t|
    t.integer "admin_user_id"
    t.integer "division_id"
  end

  create_table "doppelbock_admin_users_doppelbock_roles", :id => false, :force => true do |t|
    t.integer "admin_user_id"
    t.integer "role_id"
  end

  add_index "doppelbock_admin_users_doppelbock_roles", ["admin_user_id", "role_id"], :name => "index_doppelbock_admin_users_doppelbock_roles_admin_role"

  create_table "doppelbock_admin_users_doppelbock_teams", :id => false, :force => true do |t|
    t.integer "team_id"
    t.integer "admin_user_id"
  end

  create_table "doppelbock_adverts", :force => true do |t|
    t.integer  "admin_user_id"
    t.string   "ref_number"
    t.boolean  "active",        :default => true
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "product_id"
  end

  add_index "doppelbock_adverts", ["product_id"], :name => "index_doppelbock_adverts_on_product_id"

  create_table "doppelbock_branches", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "doppelbock_buyers", :force => true do |t|
    t.string   "name"
    t.string   "contact_fname"
    t.string   "contact_sname"
    t.string   "contact_email"
    t.string   "contact_number"
    t.string   "title"
    t.string   "website"
    t.string   "category_one"
    t.string   "category_two"
    t.text     "client_instruction"
    t.integer  "vertical_id"
    t.integer  "location_id"
    t.integer  "admin_user_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "doppelbock_companies", :force => true do |t|
    t.string   "name"
    t.string   "trading_as"
    t.string   "mobile"
    t.string   "email"
    t.string   "registration_number"
    t.decimal  "price",                 :precision => 10, :scale => 2
    t.string   "current_owner"
    t.string   "primary_sector"
    t.integer  "location_id"
    t.integer  "primary_vertical_id"
    t.integer  "secondary_vertical_id"
    t.text     "description"
    t.decimal  "annual_net_profit",     :precision => 10, :scale => 2
    t.decimal  "total_assets",          :precision => 10, :scale => 2
    t.decimal  "stock",                 :precision => 10, :scale => 2
    t.decimal  "annual_turnover",       :precision => 10, :scale => 2
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
  end

  create_table "doppelbock_consultant_reports", :force => true do |t|
    t.integer  "year"
    t.integer  "month"
    t.integer  "week"
    t.decimal  "average_candidates",               :precision => 5,  :scale => 1
    t.integer  "week_cvsends"
    t.integer  "week_interviews"
    t.decimal  "week_placements",                  :precision => 10, :scale => 2
    t.string   "quality_candidates", :limit => 15
    t.string   "quality_companies",  :limit => 15
    t.integer  "adverts_written"
    t.text     "summary"
    t.integer  "team_id"
    t.integer  "admin_user_id"
    t.datetime "created_at",                                                      :null => false
    t.datetime "updated_at",                                                      :null => false
  end

  create_table "doppelbock_countries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "doppelbock_cvlogs", :force => true do |t|
    t.string   "address_from"
    t.string   "address_to"
    t.datetime "date"
    t.text     "subject"
    t.text     "body"
    t.integer  "admin_user_id"
    t.integer  "division_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "doppelbock_cvlogs_2013", :force => true do |t|
    t.string   "address_from"
    t.string   "address_to"
    t.datetime "date"
    t.text     "subject"
    t.text     "body"
    t.integer  "admin_user_id"
    t.integer  "division_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "doppelbock_cvlogs_2013", ["admin_user_id"], :name => "admin_user_id"
  add_index "doppelbock_cvlogs_2013", ["date"], :name => "date"
  add_index "doppelbock_cvlogs_2013", ["division_id"], :name => "division_id"
  add_index "doppelbock_cvlogs_2013", ["subject"], :name => "subject"

  create_table "doppelbock_cvlogs_doppelbock_documents", :id => false, :force => true do |t|
    t.integer "cvlog_id"
    t.integer "document_id"
  end

  create_table "doppelbock_divisions", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "advert_email"
    t.string   "spec_email"
    t.string   "tac_email"
    t.string   "advert_fax"
    t.boolean  "active",       :default => true
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.string   "tab_name"
  end

  create_table "doppelbock_documents", :force => true do |t|
    t.string   "filename_original"
    t.string   "filename_modified"
    t.string   "public_url",        :limit => 500
    t.string   "md5sum"
    t.string   "mimetype"
    t.integer  "size"
    t.boolean  "stored",                                :default => false
    t.boolean  "indexed",                               :default => false
    t.string   "category"
    t.integer  "division_id"
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
    t.text     "content",           :limit => 16777215
  end

  create_table "doppelbock_interviews", :force => true do |t|
    t.datetime "date"
    t.integer  "product_id"
    t.integer  "buyer_id"
    t.integer  "admin_user_id"
    t.integer  "division_id"
    t.boolean  "complete",      :default => false
    t.boolean  "caf_signed",    :default => false
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "doppelbock_locations", :force => true do |t|
    t.string   "name"
    t.integer  "region_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "doppelbock_policies", :force => true do |t|
    t.string   "title"
    t.string   "document"
    t.string   "document_original"
    t.string   "document_type"
    t.integer  "document_size"
    t.integer  "admin_user_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "doppelbock_products", :force => true do |t|
    t.string   "email"
    t.string   "mobile"
    t.integer  "location_id"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "name"
    t.string   "trading_as"
    t.string   "registration_number"
    t.decimal  "price",                 :precision => 13, :scale => 2
    t.string   "current_owner"
    t.string   "primary_sector"
    t.text     "description"
    t.decimal  "annual_net_profit",     :precision => 13, :scale => 2
    t.decimal  "total_assets",          :precision => 13, :scale => 2
    t.decimal  "stock",                 :precision => 13, :scale => 2
    t.decimal  "annual_turnover",       :precision => 13, :scale => 2
    t.integer  "admin_user_id"
    t.integer  "vertical_id"
    t.string   "document"
    t.string   "document_original"
    t.string   "document_type"
    t.integer  "document_size"
    t.integer  "shotgun_admin_user_id"
    t.datetime "shotgun_date"
    t.datetime "shotgun_release_date"
    t.string   "company_type"
    t.text     "summary"
  end

  add_index "doppelbock_products", ["admin_user_id"], :name => "index_doppelbock_products_on_admin_user_id"
  add_index "doppelbock_products", ["vertical_id"], :name => "index_doppelbock_products_on_vertical_id"

  create_table "doppelbock_regions", :force => true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "doppelbock_roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.integer  "position"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "title"
  end

  add_index "doppelbock_roles", ["name", "resource_type", "resource_id"], :name => "index_doppelbock_roles_on_name_and_resource_type_and_resource_id"
  add_index "doppelbock_roles", ["name"], :name => "index_doppelbock_roles_on_name"

  create_table "doppelbock_sales", :force => true do |t|
    t.integer  "product_id"
    t.integer  "buyer_id"
    t.decimal  "sale_value",              :precision => 12, :scale => 2
    t.string   "invoice_number"
    t.decimal  "invoice_value",           :precision => 12, :scale => 2
    t.decimal  "vat",                     :precision => 12, :scale => 2
    t.decimal  "fee",                     :precision => 12, :scale => 2
    t.boolean  "offer_signed_by_product",                                :default => false
    t.boolean  "offer_signed_by_buyer",                                  :default => false
    t.boolean  "verified",                                               :default => false
    t.boolean  "checks_done",                                            :default => false
    t.boolean  "caf_signed",                                             :default => false
    t.text     "comments"
    t.integer  "division_id"
    t.integer  "admin_user_id"
    t.text     "buyer_details"
    t.integer  "location_id"
    t.datetime "created_at",                                                                :null => false
    t.datetime "updated_at",                                                                :null => false
  end

  create_table "doppelbock_shotguns", :force => true do |t|
    t.integer  "admin_user_id"
    t.integer  "product_id"
    t.datetime "shotgun_date"
    t.datetime "release_date"
  end

  add_index "doppelbock_shotguns", ["admin_user_id"], :name => "index_doppelbock_shotguns_on_admin_user_id"
  add_index "doppelbock_shotguns", ["product_id"], :name => "index_doppelbock_shotguns_on_product_id"

  create_table "doppelbock_specifications", :force => true do |t|
    t.string   "address_from"
    t.string   "address_to"
    t.datetime "date"
    t.text     "subject"
    t.text     "body"
    t.integer  "admin_user_id"
    t.integer  "vertical_id"
    t.integer  "buyer_id"
    t.boolean  "open",          :default => true
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "doppelbock_specifications", ["admin_user_id"], :name => "admin_user_id"
  add_index "doppelbock_specifications", ["date"], :name => "date"
  add_index "doppelbock_specifications", ["subject"], :name => "subject"
  add_index "doppelbock_specifications", ["vertical_id"], :name => "vertical_id"

  create_table "doppelbock_specifications_worked", :force => true do |t|
    t.string   "company"
    t.string   "position"
    t.integer  "candidates"
    t.integer  "consultant_report_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "doppelbock_tacs", :force => true do |t|
    t.string   "address_from"
    t.string   "address_to"
    t.datetime "date"
    t.text     "subject"
    t.text     "body"
    t.integer  "admin_user_id"
    t.integer  "division_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "buyer_id"
  end

  create_table "doppelbock_tacs_doppelbock_documents", :id => false, :force => true do |t|
    t.integer "tac_id"
    t.integer "document_id"
  end

  create_table "doppelbock_targeted_companies", :force => true do |t|
    t.string   "name"
    t.text     "comment"
    t.string   "ctype",                :limit => 5
    t.integer  "consultant_report_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  create_table "doppelbock_teams", :force => true do |t|
    t.string   "name"
    t.integer  "admin_user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "doppelbock_verticals", :force => true do |t|
    t.string   "name"
    t.integer  "division_id"
    t.boolean  "active",      :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "refinery_images", :force => true do |t|
    t.string   "image_mime_type"
    t.string   "image_name"
    t.integer  "image_size"
    t.integer  "image_width"
    t.integer  "image_height"
    t.string   "image_uid"
    t.string   "image_ext"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "refinery_inquiries_inquiries", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "message"
    t.boolean  "spam",       :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "refinery_inquiries_inquiries", ["id"], :name => "index_refinery_inquiries_inquiries_on_id"

  create_table "refinery_page_part_translations", :force => true do |t|
    t.integer  "refinery_page_part_id"
    t.string   "locale"
    t.text     "body"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "refinery_page_part_translations", ["locale"], :name => "index_refinery_page_part_translations_on_locale"
  add_index "refinery_page_part_translations", ["refinery_page_part_id"], :name => "index_f9716c4215584edbca2557e32706a5ae084a15ef"

  create_table "refinery_page_parts", :force => true do |t|
    t.integer  "refinery_page_id"
    t.string   "title"
    t.text     "body"
    t.integer  "position"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "refinery_page_parts", ["id"], :name => "index_refinery_page_parts_on_id"
  add_index "refinery_page_parts", ["refinery_page_id"], :name => "index_refinery_page_parts_on_refinery_page_id"

  create_table "refinery_page_translations", :force => true do |t|
    t.integer  "refinery_page_id"
    t.string   "locale"
    t.string   "title"
    t.string   "custom_slug"
    t.string   "menu_title"
    t.string   "slug"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "refinery_page_translations", ["locale"], :name => "index_refinery_page_translations_on_locale"
  add_index "refinery_page_translations", ["refinery_page_id"], :name => "index_d079468f88bff1c6ea81573a0d019ba8bf5c2902"

  create_table "refinery_pages", :force => true do |t|
    t.integer  "parent_id"
    t.string   "path"
    t.string   "slug"
    t.boolean  "show_in_menu",        :default => true
    t.string   "link_url"
    t.string   "menu_match"
    t.boolean  "deletable",           :default => true
    t.boolean  "draft",               :default => false
    t.boolean  "skip_to_first_child", :default => false
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.string   "view_template"
    t.string   "layout_template"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "refinery_pages", ["depth"], :name => "index_refinery_pages_on_depth"
  add_index "refinery_pages", ["id"], :name => "index_refinery_pages_on_id"
  add_index "refinery_pages", ["lft"], :name => "index_refinery_pages_on_lft"
  add_index "refinery_pages", ["parent_id"], :name => "index_refinery_pages_on_parent_id"
  add_index "refinery_pages", ["rgt"], :name => "index_refinery_pages_on_rgt"

  create_table "refinery_pages_pods", :id => false, :force => true do |t|
    t.integer "page_id"
    t.integer "pod_id"
  end

  add_index "refinery_pages_pods", ["page_id"], :name => "index_refinery_pages_pods_on_page_id"
  add_index "refinery_pages_pods", ["pod_id"], :name => "index_refinery_pages_pods_on_pod_id"

  create_table "refinery_pods", :force => true do |t|
    t.string   "name"
    t.text     "body"
    t.string   "url"
    t.integer  "image_id"
    t.string   "pod_type"
    t.integer  "portfolio_entry_id"
    t.integer  "video_id"
    t.integer  "position"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "refinery_pods", ["image_id"], :name => "index_refinery_pods_on_image_id"
  add_index "refinery_pods", ["pod_type"], :name => "index_refinery_pods_on_pod_type"
  add_index "refinery_pods", ["portfolio_entry_id"], :name => "index_refinery_pods_on_portfolio_entry_id"
  add_index "refinery_pods", ["video_id"], :name => "index_refinery_pods_on_video_id"

  create_table "refinery_portfolio_galleries", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.string   "slug"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "refinery_portfolio_gallery_translations", :force => true do |t|
    t.integer  "refinery_portfolio_gallery_id"
    t.string   "locale"
    t.string   "title"
    t.text     "body"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "refinery_portfolio_gallery_translations", ["locale"], :name => "index_refinery_portfolio_gallery_translations_on_locale"
  add_index "refinery_portfolio_gallery_translations", ["refinery_portfolio_gallery_id"], :name => "index_dacf6685c3221de568049c599f2a69d1c1f9dd25"

  create_table "refinery_portfolio_item_translations", :force => true do |t|
    t.integer  "refinery_portfolio_item_id"
    t.string   "locale"
    t.string   "title"
    t.text     "caption"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "refinery_portfolio_item_translations", ["locale"], :name => "index_refinery_portfolio_item_translations_on_locale"
  add_index "refinery_portfolio_item_translations", ["refinery_portfolio_item_id"], :name => "index_2f72df747b84672dbcc6cb153c8031486c5de521"

  create_table "refinery_portfolio_items", :force => true do |t|
    t.string   "title"
    t.string   "caption"
    t.integer  "image_id",   :null => false
    t.integer  "gallery_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "position"
  end

  create_table "refinery_resources", :force => true do |t|
    t.string   "file_mime_type"
    t.string   "file_name"
    t.integer  "file_size"
    t.string   "file_uid"
    t.string   "file_ext"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "refinery_roles", :force => true do |t|
    t.string "title"
  end

  create_table "refinery_roles_users", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "refinery_roles_users", ["role_id", "user_id"], :name => "index_refinery_roles_users_on_role_id_and_user_id"
  add_index "refinery_roles_users", ["user_id", "role_id"], :name => "index_refinery_roles_users_on_user_id_and_role_id"

  create_table "refinery_settings", :force => true do |t|
    t.string   "name"
    t.text     "value"
    t.boolean  "destroyable",     :default => true
    t.string   "scoping"
    t.boolean  "restricted",      :default => false
    t.string   "form_value_type"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "refinery_settings", ["name"], :name => "index_refinery_settings_on_name"

  create_table "refinery_user_plugins", :force => true do |t|
    t.integer "user_id"
    t.string  "name"
    t.integer "position"
  end

  add_index "refinery_user_plugins", ["name"], :name => "index_refinery_user_plugins_on_name"
  add_index "refinery_user_plugins", ["user_id", "name"], :name => "index_refinery_user_plugins_on_user_id_and_name", :unique => true

  create_table "refinery_users", :force => true do |t|
    t.string   "username",               :null => false
    t.string   "email",                  :null => false
    t.string   "encrypted_password",     :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "sign_in_count"
    t.datetime "remember_created_at"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "refinery_users", ["id"], :name => "index_refinery_users_on_id"

  create_table "refinery_videos", :force => true do |t|
    t.string   "name"
    t.string   "youtube_url"
    t.text     "body"
    t.integer  "position"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "seo_meta", :force => true do |t|
    t.integer  "seo_meta_id"
    t.string   "seo_meta_type"
    t.string   "browser_title"
    t.string   "meta_keywords"
    t.text     "meta_description"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "seo_meta", ["id"], :name => "index_seo_meta_on_id"
  add_index "seo_meta", ["seo_meta_id", "seo_meta_type"], :name => "index_seo_meta_on_seo_meta_id_and_seo_meta_type"

  create_table "versions", :force => true do |t|
    t.string   "item_type",      :null => false
    t.integer  "item_id",        :null => false
    t.string   "event",          :null => false
    t.string   "whodunnit"
    t.text     "object"
    t.text     "object_changes"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], :name => "index_versions_on_item_type_and_item_id"

end
