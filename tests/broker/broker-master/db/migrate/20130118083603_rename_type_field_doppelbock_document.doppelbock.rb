# This migration comes from doppelbock (originally 20121214100125)
class RenameTypeFieldDoppelbockDocument < ActiveRecord::Migration
  def change
    rename_column :doppelbock_documents, :type, :mimetype
  end
end
