# This migration comes from doppelbock (originally 20121017142546)
class CreateDoppelbockDocuments < ActiveRecord::Migration
  def change
    create_table :doppelbock_documents do |t|
      t.string :filename_original
      t.string :filename_modified
      t.string :public_url
      t.string :md5sum, :length => 32
      t.string :type
      t.integer :size
      t.boolean :stored, :default => false
      t.boolean :indexed, :default => false
      t.string :category
      t.integer :division_id

      t.timestamps
    end
  end
end
