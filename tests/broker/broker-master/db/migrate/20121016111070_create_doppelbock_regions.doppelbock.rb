# This migration comes from doppelbock (originally 20121004163902)
class CreateDoppelbockRegions < ActiveRecord::Migration
  def change
    create_table :doppelbock_regions do |t|
      t.string :name, :nil => false
      t.integer :country_id, :nil => false

      t.timestamps
    end
  end
end
