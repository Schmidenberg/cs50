# This migration comes from doppelbock (originally 20121113080504)
class AddUsernameToDoppelbockAdminUsers < ActiveRecord::Migration
  def change
    add_column :doppelbock_admin_users, :username, :string
  end
end
