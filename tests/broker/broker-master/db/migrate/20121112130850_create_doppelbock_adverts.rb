class CreateDoppelbockAdverts < ActiveRecord::Migration
  def change
    create_table :doppelbock_adverts do |t|
      t.integer :company_id, :nil => false
      t.integer :admin_user_id, :nil => false
      t.string :ref_number
      t.boolean :active, :nil => false, :default => true

      t.timestamps
    end
  end
end
