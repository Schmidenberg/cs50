# This migration comes from doppelbock (originally 20130301090224)
class AddSummaryToDoppelbockProducts < ActiveRecord::Migration
  def change
    add_column :doppelbock_products, :summary, :text
  end
end
