# This migration comes from doppelbock (originally 20121002101901)
class CreateDoppelbockVerticals < ActiveRecord::Migration
  def change
    create_table :doppelbock_verticals do |t|
      t.string :name
      t.integer :division_id
      t.boolean :active, :default => true, :nil => false

      t.timestamps
    end
  end
end
