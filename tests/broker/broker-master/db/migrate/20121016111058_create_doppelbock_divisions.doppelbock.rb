# This migration comes from doppelbock (originally 20120926100801)
class CreateDoppelbockDivisions < ActiveRecord::Migration
  def change
    create_table :doppelbock_divisions do |t|
      t.string :name
      t.string  :email
      t.string  :advert_email
      t.string  :spec_email
      t.string  :tac_email
      t.string  :advert_fax
      t.boolean :active, :default => true, :nil => false
      t.timestamps
    end
  end
end
