# This migration comes from doppelbock (originally 20121002162101)
class CreateDoppelbockConsultantReports < ActiveRecord::Migration
  def change
    create_table :doppelbock_consultant_reports do |t|
      t.integer :year
      t.integer :month
      t.integer :week
      t.decimal :average_candidates, :precision => 5, :scale => 1
      t.integer :week_cvsends
      t.integer :week_interviews
      t.decimal :week_placements, :precision => 10, :scale => 2
      t.string :quality_candidates, :limit => 15
      t.string :quality_companies, :limit => 15
      t.integer :adverts_written
      t.text :summary
      t.integer :team_id
      t.integer :admin_user_id

      t.timestamps
    end
  end
end
