# This migration comes from doppelbock (originally 20130114142712)
class AddBuyerIdToDoppelbockTacs < ActiveRecord::Migration
  def change
    add_column :doppelbock_tacs, :buyer_id, :integer
  end
end
