# This migration comes from doppelbock (originally 20130213100113)
class CreateDoppelbockCvlogs2013 < ActiveRecord::Migration
  def up
    create_table "doppelbock_cvlogs_2013" do |t|
      t.string   "address_from"
      t.string   "address_to"
      t.datetime "date"
      t.text     "subject"
      t.text     "body"
      t.integer  "admin_user_id"
      t.integer  "division_id"
      t.datetime "created_at",    :null => false
      t.datetime "updated_at",    :null => false
    end

    add_index "doppelbock_cvlogs_2013", ["admin_user_id"], :name => "admin_user_id"
    add_index "doppelbock_cvlogs_2013", ["date"], :name => "date"
    add_index "doppelbock_cvlogs_2013", ["division_id"], :name => "division_id"

    execute("ALTER TABLE doppelbock_cvlogs_2013 ENGINE = MYISAM;")
    execute("ALTER TABLE doppelbock_cvlogs_2013 ADD FULLTEXT (subject);")
  end

  def down
    drop_table :doppelbock_cvlogs_2013
  end
end
