# This migration comes from doppelbock (originally 20121001162001)
class CreateDoppelbockAdminUsersDoppelbockDivisions < ActiveRecord::Migration

  def change
    create_table(:doppelbock_admin_users_doppelbock_divisions, :id => false) do |t|
      t.references :admin_user
      t.references :division
    end
  end

end