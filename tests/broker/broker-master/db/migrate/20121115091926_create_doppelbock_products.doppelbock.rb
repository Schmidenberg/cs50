# This migration comes from doppelbock (originally 20121115081422)
class CreateDoppelbockProducts < ActiveRecord::Migration
  def change
    create_table :doppelbock_products do |t|
      t.string :email
      t.string :mobile
      t.integer :location_id, :nil => false
      t.integer :primary_vertical_id, :nil => false
      t.integer :secondary_vertical_id

      t.timestamps
    end
  end
end
