# This migration comes from doppelbock (originally 20121119132318)
class AddTabNameToDivision < ActiveRecord::Migration
  def change
    add_column :doppelbock_divisions, :tab_name, :string
  end
end
