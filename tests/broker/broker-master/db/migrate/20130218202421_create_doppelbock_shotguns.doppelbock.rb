# This migration comes from doppelbock (originally 20130218202233)
class CreateDoppelbockShotguns < ActiveRecord::Migration
  def change
    create_table :doppelbock_shotguns do |t|
      t.references :admin_user
      t.references :product
      t.datetime :shotgun_date
      t.datetime :release_date
    end
    add_index :doppelbock_shotguns, :admin_user_id
    add_index :doppelbock_shotguns, :product_id
  end
end
