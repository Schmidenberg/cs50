# This migration comes from doppelbock (originally 20130118084811)
class AddShotgunFieldsToDoppelbockProducts < ActiveRecord::Migration
  def change
    add_column :doppelbock_products, :shotgun_admin_user_id, :integer
    add_column :doppelbock_products, :shotgun_date, :datetime
  end
end
