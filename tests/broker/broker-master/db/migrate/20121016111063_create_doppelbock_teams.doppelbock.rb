# This migration comes from doppelbock (originally 20121002151701)
class CreateDoppelbockTeams < ActiveRecord::Migration
  def change
    create_table :doppelbock_teams do |t|
      t.string :name
      t.integer :admin_user_id

      t.timestamps
    end
  end
end
