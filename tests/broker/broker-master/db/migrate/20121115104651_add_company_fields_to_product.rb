class AddCompanyFieldsToProduct < ActiveRecord::Migration
  def change
    add_column :doppelbock_products, :name, :string
    add_column :doppelbock_products, :trading_as, :string
    add_column :doppelbock_products, :registration_number, :string
    add_column :doppelbock_products, :price, :decimal, :precision => 10, :scale => 2
    add_column :doppelbock_products, :current_owner, :string
    add_column :doppelbock_products, :primary_sector, :string
    add_column :doppelbock_products, :description, :text
    add_column :doppelbock_products, :annual_net_profit, :decimal, :precision => 10, :scale => 2
    add_column :doppelbock_products, :total_assets, :decimal, :precision => 10, :scale => 2
    add_column :doppelbock_products, :stock, :decimal, :precision => 10, :scale => 2
    add_column :doppelbock_products, :annual_turnover, :decimal, :precision => 10, :scale => 2
  end
end
