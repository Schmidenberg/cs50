# This migration comes from doppelbock (originally 20121109082124)
class AddBranchIdToDoppelbockAdminUsers < ActiveRecord::Migration
  def change
    add_column :doppelbock_admin_users, :branch_id, :integer
  end
end
