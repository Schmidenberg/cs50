# This migration comes from doppelbock (originally 20121004120801)
class CreateDoppelbockPolicies < ActiveRecord::Migration
  def change
    create_table :doppelbock_policies do |t|
      t.string :title
      t.string :document
      t.string :document_original
      t.string :document_type
      t.integer :document_size
      t.integer :admin_user_id

      t.timestamps
    end
  end
end
