# This migration comes from doppelbock (originally 20121025073952)
class ChangeDocumentPublicUrlLength < ActiveRecord::Migration

  change_table :doppelbock_documents do |t|
    t.change :public_url, :string, :limit => 500
  end

end
