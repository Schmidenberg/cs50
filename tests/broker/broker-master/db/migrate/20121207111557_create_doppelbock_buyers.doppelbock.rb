# This migration comes from doppelbock (originally 20121207103851)
class CreateDoppelbockBuyers < ActiveRecord::Migration
  def change
    create_table :doppelbock_buyers do |t|
      t.string :name, :nil => false
      t.string :contact_fname
      t.string :contact_sname
      t.string :contact_email
      t.string :contact_number
      t.string :title
      t.string :website
      t.string :category_one
      t.string :category_two
      t.text :client_instruction
      t.integer :vertical_id
      t.integer :location_id
      t.integer :admin_user_id

      t.timestamps
    end
  end
end
