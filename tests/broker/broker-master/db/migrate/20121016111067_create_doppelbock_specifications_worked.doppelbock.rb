# This migration comes from doppelbock (originally 20121002164201)
class CreateDoppelbockSpecificationsWorked < ActiveRecord::Migration
  def change
    create_table :doppelbock_specifications_worked do |t|
      t.string :company
      t.string :position
      t.integer :candidates
      t.integer :consultant_report_id

      t.timestamps
    end
  end
end
