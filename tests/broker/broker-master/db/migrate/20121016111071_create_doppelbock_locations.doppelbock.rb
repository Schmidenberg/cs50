# This migration comes from doppelbock (originally 20121004164001)
class CreateDoppelbockLocations < ActiveRecord::Migration
  def change
    create_table :doppelbock_locations do |t|
      t.string :name, :nil => false
      t.integer :region_id, :nil => false

      t.timestamps
    end
  end
end
