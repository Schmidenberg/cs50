# This migration comes from doppelbock (originally 20121212135140)
class CreateDoppelbockInterviews < ActiveRecord::Migration
  def change
    create_table :doppelbock_interviews do |t|
      t.datetime :date, :nil => false
      t.integer :product_id, :nil => false
      t.integer :buyer_id, :nil => false
      t.integer :admin_user_id
      t.integer :division_id
      t.boolean :complete, :nil => false, :default => false
      t.boolean :caf_signed, :nil => false, :default => false

      t.timestamps
    end
  end
end
