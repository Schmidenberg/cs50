# This migration comes from doppelbock (originally 20121109081429)
class CreateDoppelbockBranches < ActiveRecord::Migration
  def change
    create_table :doppelbock_branches do |t|
      t.string :name

      t.timestamps
    end
  end
end
