class AddImageFieldsToProduct < ActiveRecord::Migration
  def change
    add_column :doppelbock_products, :document, :string
    add_column :doppelbock_products, :document_original, :string
    add_column :doppelbock_products, :document_type, :string
    add_column :doppelbock_products, :document_size, :integer
  end
end
