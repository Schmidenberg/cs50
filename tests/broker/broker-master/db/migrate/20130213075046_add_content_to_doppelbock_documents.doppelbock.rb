# This migration comes from doppelbock (originally 20130213074211)
class AddContentToDoppelbockDocuments < ActiveRecord::Migration
  def change
    add_column :doppelbock_documents, :content, :mediumtext
  end
end
