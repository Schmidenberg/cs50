class AddProductIdToAdverts < ActiveRecord::Migration
  def up
    add_column :doppelbock_adverts, :product_id, :integer
    add_index :doppelbock_adverts, :product_id
    remove_column :doppelbock_adverts, :company_id
  end

  def down
    remove_column :doppelbock_adverts, :product_id
    add_column :doppelbock_adverts, :company_id, :integer
  end
end
