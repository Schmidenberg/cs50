class AddProductIdBackIntoAdverts < ActiveRecord::Migration
  def up
    add_column :doppelbock_adverts, :product_id, :integer
    add_index :doppelbock_adverts, :product_id
  end

  def down
    remove_column :doppelbock_adverts, :product_id
  end
end
