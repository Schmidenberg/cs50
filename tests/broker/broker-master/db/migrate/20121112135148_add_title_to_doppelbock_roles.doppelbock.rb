# This migration comes from doppelbock (originally 20121112135130)
class AddTitleToDoppelbockRoles < ActiveRecord::Migration
  def change
    add_column :doppelbock_roles, :title, :string
  end
end
