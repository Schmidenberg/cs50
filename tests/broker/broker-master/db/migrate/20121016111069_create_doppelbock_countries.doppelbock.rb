# This migration comes from doppelbock (originally 20121004163901)
class CreateDoppelbockCountries < ActiveRecord::Migration
  def change
    create_table :doppelbock_countries do |t|
      t.string :name, :nil => false

      t.timestamps
    end
  end
end