# This migration comes from doppelbock (originally 20121017142307)
class CreateDoppelbockCvlogs < ActiveRecord::Migration
  def change
    create_table :doppelbock_cvlogs do |t|
      t.string :address_from
      t.string :address_to
      t.datetime :date
      t.text :subject
      t.text :body
      t.integer :admin_user_id
      t.integer :division_id

      t.timestamps
    end
  end
end
