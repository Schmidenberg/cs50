# This migration comes from doppelbock (originally 20130114083137)
class CreateDoppelbockTacs < ActiveRecord::Migration
  def change
    create_table :doppelbock_tacs do |t|
      t.string :address_from
      t.string :address_to
      t.datetime :date
      t.text :subject
      t.text :body
      t.integer :admin_user_id
      t.integer :division_id

      t.timestamps
    end
  end
end
