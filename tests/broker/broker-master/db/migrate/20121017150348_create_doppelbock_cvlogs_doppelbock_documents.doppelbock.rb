# This migration comes from doppelbock (originally 20121017142609)
class CreateDoppelbockCvlogsDoppelbockDocuments < ActiveRecord::Migration

  def change
    create_table(:doppelbock_cvlogs_doppelbock_documents, :id => false) do |t|
      t.references :cvlog
      t.references :document
    end
  end

end
