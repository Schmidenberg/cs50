# This migration comes from doppelbock (originally 20130114084332)
class CreateDoppelbockTacsDoppelbockDocuments < ActiveRecord::Migration

  def change
    create_table(:doppelbock_tacs_doppelbock_documents, :id => false) do |t|
      t.references :tac
      t.references :document
    end
  end

end
