# This migration comes from doppelbock (originally 20121119122420)
class ModifyFieldsForDoppelbockProduct < ActiveRecord::Migration

  def up
    add_column :doppelbock_products, :admin_user_id, :integer
    add_column :doppelbock_products, :vertical_id, :integer

    remove_column :doppelbock_products, :primary_vertical_id
    remove_column :doppelbock_products, :secondary_vertical_id

    add_index :doppelbock_products, :admin_user_id
    add_index :doppelbock_products, :vertical_id
  end

  def down
    remove_column :doppelbock_products, :admin_user_id
    remove_column :doppelbock_products, :vertical_id

    add_column :doppelbock_products, :primary_vertical_id, :integer
    add_column :doppelbock_products, :secondary_vertical_id, :integer
  end

end
