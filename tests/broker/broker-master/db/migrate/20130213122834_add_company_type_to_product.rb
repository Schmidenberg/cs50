class AddCompanyTypeToProduct < ActiveRecord::Migration
  def change
    add_column :doppelbock_products, :company_type, :string
  end
end
