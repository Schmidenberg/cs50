# This migration comes from doppelbock (originally 20121002151801)
class CreateDoppelbockAdminUsersDoppelbockTeams < ActiveRecord::Migration
  def change
    create_table :doppelbock_admin_users_doppelbock_teams, :id => false do |t|
      t.integer :team_id
      t.integer :admin_user_id
    end
  end
end
