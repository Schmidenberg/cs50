# This migration comes from doppelbock (originally 20130121095456)
class AddShotgunReleaseDateToDoppelbockProducts < ActiveRecord::Migration
  def change
    add_column :doppelbock_products, :shotgun_release_date, :datetime
  end
end
