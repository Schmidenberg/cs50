# This migration comes from doppelbock (originally 20121212120444)
class CreateDoppelbockSales < ActiveRecord::Migration
  def change
    create_table :doppelbock_sales do |t|
      t.integer :product_id, :nil => false
      t.integer :buyer_id, :nil => false
      t.decimal :sale_value, :precision => 12, :scale => 2
      t.string :invoice_number
      t.decimal :invoice_value, :precision => 12, :scale => 2
      t.decimal :vat, :precision => 12, :scale => 2
      t.decimal :fee, :precision => 12, :scale => 2
      t.boolean :offer_signed_by_product, :nil => false, :default => false
      t.boolean :offer_signed_by_buyer, :nil => false, :default => false
      t.boolean :verified, :nil => false, :default => false
      t.boolean :checks_done, :nil => false, :default => false
      t.boolean :caf_signed, :nil => false, :default => false
      t.text :comments
      t.integer :division_id
      t.integer :admin_user_id
      t.text :buyer_details
      t.integer :location_id

      t.timestamps
    end
  end
end
