# This migration comes from doppelbock (originally 20130214132432)
class CreateDoppelbockSpecifications < ActiveRecord::Migration
  def change
    create_table :doppelbock_specifications do |t|
      t.string   "address_from"
      t.string   "address_to"
      t.datetime "date"
      t.text     "subject"
      t.text     "body"
      t.integer  "admin_user_id"
      t.integer  "vertical_id"
      t.integer  "buyer_id"
      t.boolean "open", :default => true, :nil => false
      t.timestamps
    end

    add_index "doppelbock_specifications", ["admin_user_id"], :name => "admin_user_id"
    add_index "doppelbock_specifications", ["date"], :name => "date"
    add_index "doppelbock_specifications", ["vertical_id"], :name => "vertical_id"

    execute("ALTER TABLE doppelbock_specifications ENGINE = MYISAM;")
    execute("ALTER TABLE doppelbock_specifications ADD FULLTEXT (subject);")
  end
end
