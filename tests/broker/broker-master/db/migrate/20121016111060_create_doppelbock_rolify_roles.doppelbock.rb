# This migration comes from doppelbock (originally 20120927162830)
class CreateDoppelbockRolifyRoles < ActiveRecord::Migration
  def change
    create_table :doppelbock_roles do |t|
      t.string :name
      t.references :resource, :polymorphic => true
      t.integer :position

      t.timestamps
    end

    create_table(:doppelbock_admin_users_doppelbock_roles, :id => false) do |t|
      t.references :admin_user
      t.references :role
    end

    add_index(:doppelbock_roles, :name)
    add_index(:doppelbock_roles, [ :name, :resource_type, :resource_id ])
    add_index(:doppelbock_admin_users_doppelbock_roles, [ :admin_user_id, :role_id ], :name => 'index_doppelbock_admin_users_doppelbock_roles_admin_role')
  end
end
