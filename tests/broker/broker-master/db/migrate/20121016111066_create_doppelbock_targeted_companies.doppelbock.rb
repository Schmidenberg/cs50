# This migration comes from doppelbock (originally 20121002163301)
class CreateDoppelbockTargetedCompanies < ActiveRecord::Migration
  def change
    create_table :doppelbock_targeted_companies do |t|
      t.string :name
      t.text :comment
      t.string :ctype, :limit => 5
      t.integer :consultant_report_id

      t.timestamps
    end
  end
end
