class CreateDoppelbockCompanies < ActiveRecord::Migration
  def change
    create_table :doppelbock_companies do |t|
      t.string :name, :nil => false
      t.string :trading_as
      t.string :mobile
      t.string :email
      t.string :registration_number
      t.decimal :price, :precision => 10, :scale => 2
      t.string :current_owner
      t.string :primary_sector
      t.integer :location_id
      t.integer :primary_vertical_id
      t.integer :secondary_vertical_id
      t.text :description
      t.decimal :annual_net_profit, :precision => 10, :scale => 2
      t.decimal :total_assets, :precision => 10, :scale => 2
      t.decimal :stock, :precision => 10, :scale => 2
      t.decimal :annual_turnover, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
