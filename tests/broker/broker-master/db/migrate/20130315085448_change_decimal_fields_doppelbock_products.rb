class ChangeDecimalFieldsDoppelbockProducts < ActiveRecord::Migration

  change_table :doppelbock_products do |t|
    t.change :price, :decimal, :precision => 13, :scale => 2
    t.change :annual_net_profit, :decimal, :precision => 13, :scale => 2
    t.change :total_assets, :decimal, :precision => 13, :scale => 2
    t.change :stock, :decimal, :precision => 13, :scale => 2
    t.change :annual_turnover, :decimal, :precision => 13, :scale => 2
  end

end
