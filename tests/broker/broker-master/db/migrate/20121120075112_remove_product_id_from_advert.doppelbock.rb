# This migration comes from doppelbock (originally 20121120074746)
class RemoveProductIdFromAdvert < ActiveRecord::Migration
  def up
    remove_column :doppelbock_adverts, :product_id
  end

  def down
    add_column :doppelbock_adverts, :product_id, :integer
  end
end
