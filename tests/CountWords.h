/* function to store chars into buffer */
void readline(char buffer[])
{
    char letter;
    int i = 0;
    do
    {
        letter = getchar();
        buffer[i] = letter;
        i++;
    }
    while(letter != '\n');
    
    buffer[i - 1] = '\0';
}

/* Function used to check if is alpha - why not use isalpha..? */
bool alphabetic (const char c)
{
    if ( (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') )
    return true;
    else
    return false;
}

/* Function to count the number of letters in a string */
int countLetters (const char string[])
{
    int i, letters = 0;
    bool alphabetic (const char c);
    for ( i = 0; string[i] != '\0'; ++i )
    if ( alphabetic(string[i]) )
    {
        letters++;
    }
    return letters;
}

/* Function to count the number of words in a string */
int countWords (const char string[])
{
    int i, wordCount = 0;
    int letters = 0;
    bool lookingForWord = true, alphabetic (const char c);
    for ( i = 0; string[i] != '\0'; ++i )
    if ( alphabetic(string[i]) )
    {
        letters++;
        if ( lookingForWord )
        {
            ++wordCount;
            lookingForWord = false;
        }
    }
    else
    lookingForWord = true;
    return wordCount;
}