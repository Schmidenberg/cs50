// Sums numbers of int
#include <stdio.h>


// testing prototypes
int give_num();
int sum_it(int i);

// shortest main i can
int main (void)
{
    int i = give_num();
    sum_it(i);
}


// functions hither
// asks for the number
int give_num()
{
    int j = 0;
    
    printf("Please provide a number:\n");
    scanf("%i", &j);
    
    return j;
}

//sums it up and prints
int sum_it(int i)
{
    int remainder = 0;
    int count = 0;
    
    while (i != 0)
    {
        remainder = i % 10;
        count += remainder;
        i /= 10;
    }
    return printf("%i\n", count);
}