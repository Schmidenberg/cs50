#include <stdio.h>

int main(void)
{
    int age; 
    int *pAge;
    char *stringLit = "This is the string, and it is 61 , including the null pointer";
    age = 10; /* Stores a 10 in age */
    pAge= &age; /* Links up the pointer */
    printf("age: %i, *pAge: %i\n", age, *pAge);
    // omitting the * causes the address to change
    *pAge = 20; // this changes the value where the pointer points
    printf("age: %i, *pAge: %i\n", age, *pAge);
    printf("String Literal: %s\n", stringLit);
    stringLit = "How's this";
    printf("String Literal: %s\n", stringLit);
    stringLit = "Fine, How about I write an incredibly long string, one that is far greater than the original - which was, as you might recall, 61 characters, inclusive. This appears to be OK (although Style may not be happy) as it is as string literal.";
    printf("String Literal: %s\n", stringLit);
    printf("The obvious drawback is that a string literal is not user input, use fgets:\n");
    char strLitTwo[10];
    char *pstrLitTwo = strLitTwo;
    fgets(pstrLitTwo, 11, stdin);
    printf("Using fgets: %s\nit truncates the string to the specified size.\n", pstrLitTwo);
}