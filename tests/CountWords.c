// counts words and letters
#include <stdio.h>
#include <stdbool.h>
#include "CountWords.h"

void readline(char buffer[]);
bool alphabetic (const char c);
int countLetters (const char string[]);
int countWords (const char string[]);

int main (void)
{
    int howManyLines = 0;
    printf("How many lines would you like to read in? \n");
    /* the trailing space is NB! Without it the %i leads 
    straight into the \n. With it C knows that %i is on it's own */
    scanf ("%i ", &howManyLines);
    
    printf("Enter phrases:\n");
    
    char line[1];
    for (int i = 0; i < howManyLines; i++)
    {
        readline(line);
        printf ("words = %i\n - letters = %i\n", countWords (line), countLetters (line));
    }
    return 0;
}
