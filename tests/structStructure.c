/**
 * 
 * This program shows that structs do not initialise to zero, like arrays
 * Also, %.2i in printf show leading zero
 *  
 **/
#include <stdio.h>
int main (void)
{
    struct timeof
    {
        int hour;
        int minute;
        int second;
    } anotherTime = { 1, 11, 11}; //one can also define variable during structure def
                                    // as well as variable values
    
    struct timeof now;
    
    now.hour = 9; // values can be omitted, but are then not initialised
    //now.minute = 42;
    //now.second = 59; 
    printf("%.2i:%.2i:%.2i\n", now.hour, now.minute, now.second);

    struct timeof next = (struct timeof) { 9, 42, }; //require compund literals (values)
                                                    // except last value
    printf("%.2i:%.2i:%.2i\n", next.hour, next.minute, next.second);
    
    printf("%.2i:%.2i:%.2i\n", anotherTime.hour, anotherTime.minute, anotherTime.second);
    
    // another level
    // struct containing array
    
    struct month
    {
        int numberOfDays;
        char name[3];
    };
    
    const struct month months[3] =
    {   { 31, {'J', 'a', 'n'} }, { 28, {'F', 'e', 'b'} },
        { 31, {'M', 'a', 'r'} } };
        
    printf ("\nMonth Number of Days\n");
    printf ("----- --------------\n");
    
    for ( int i = 0; i < 3; ++i )
        printf (" %c%c%c %i\n",
            months[i].name[0], months[i].name[1],
                months[i].name[2], months[i].numberOfDays);
    
}