/**
 * created by WRS - this progam displays the use of a struct and  
 * the creation of the initial node to a linked list
 * 
 * incl. free malloc order
 *  
 * using a function to create the node - YES!
 * 
 **/

#include <stdio.h>
#include <stdbool.h>
#include <cs50.h>
#include <ctype.h>

#define LENGTH 45



// protypes
node* create(node* root);

//void tempString(FILE* fileName, node* nodeName);
//int wordCount(FILE* fileName);

int main(void)
{
    
    // bring in the chars
    FILE* file = fopen("NodeFile", "r");
    if (file == NULL)
    {
        printf("Input File Error\n");
        return INT_MAX;
    }
    

    
        // create the crwaler
    node* crawler = malloc(sizeof(node));
    if (crawler == NULL)
    {
        printf("crawler Node Error\n");
        return INT_MAX-2;
    }
    
    // init crawler
    crawler -> is_word = NULL;
    for (int i = 0; i < 27; i++)
    {
        crawler->children[i] = NULL;
    }
    
    // index to use for c
    int index = 0;
    bool new_word;
    
    // check each letter & store into array lower with '\'' and '\n'
    for (int c = fgetc(file); c != EOF; c = fgetc(file))
    {
        
        
        
        
        
        
        // I NEED TO DO A WHILE != '\N' I NEED TO BRING A STRING IN AT A TIME AND THEN START AGAIN
        
        
        
        
        
        
        
        // if it's in a current string - carry on
        new_word = false;
        
        if (c == '\n')
        {
            new_word = true;
            crawler->is_word = true;
            printf("is new line\n");
        }
        else if (!isalpha(c) || (c == '\''))
        {
            printf("not alpha\n");
        }
        else if (isalpha(c) || (c == '\''))
        {
            // convert character to lower
            if (isupper(c))
            {
                index = tolower(c);
            }
            else if (c == '\'')
            {
                index = 27;
            }
            else
            {
                // store it
                index = c;
            }
            
            // convert index from c (or char) to index value
            if (index == 27)
            {
                // do nothing
            }
            else 
            {
                index = tolower(index) - 'a';
            }
        }
        
        // if the letter is good - traverse list
        if (new_word == false)
        {
            if (crawler->children[index] == NULL)
            {
                node* new_node = create(crawler);
                crawler->children[index] = new_node;
            }
            else
            {
                crawler = crawler->children[index];
            }
        }
        printf("Node - Bool: %i\n", crawler->is_word);
        printf("Node - Index: %d\n", index);
    }
}


node* create(node* crawler)
{
    node* new_node = malloc(sizeof(node));
    if (new_node == NULL)
    {
        printf("New_Node Error\n");
    }
    new_node->is_word = false;
    
    //not sure if this is redundant;
    for (int i = 0; i < 27; i++)
    {
        new_node->children[i] = NULL;
    }
    
    return new_node;
}


/*
    //tempString(file, root);
    
    printf("Word: %s\n", root->children);
*/
 //   fclose(file);
 //   free(root->children);
 //   free(root->is_word);
 //   free(root);



/** function to create temp buffer & memory for word member
void tempString(FILE* fileName, node* nodeName)
{
    char buffer[15];

    fscanf(fileName, "%14s", buffer);

    // create pointer to node root
    nodeName->next = NULL;
    
    int len = strlen(buffer);
    nodeName->word = malloc((len + 1) * sizeof(char));
    strcpy(nodeName->word, buffer);
    
    
    // calls wordCount to count words in dictionary
    wordCount(fileName);
}

// function to count words in file
int wordCount(FILE* fileName)
{
    unsigned int count = 0;
    char ch;
    
    while((ch = fgetc(fileName)) != EOF)
    {
        if(ch == '\n')
            count++;
    }
    printf("Word Count: %i\n", count);
    return count;
}
*/