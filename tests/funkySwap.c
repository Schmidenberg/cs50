#include <stdio.h>
int main (void)
{
    // Bitwise Exclusive-OR swap
    int i1 = 5000000;
    int i2 = 05;
    printf ("Original Values:\ni1: %d\ni2: %d\n", i1, i2);
    i1 ^= i2;
    i2 ^= i1;
    i1 ^= i2;
    printf ("Bitwise Exclusive-OR Swap Values:\ni1: %d\ni2: %d\n", i1, i2);
    
    //no Temp math swap
    int a = 0;
    int b = 10;
    printf ("Original Values:\na: %d\nb: %d\n", a, b);
    a = a + b;
    b = a - b;
    a = a - b;
    printf ("New Values:\na: %d\nb: %d\n", a, b);
}