/**
 * created by WRS - this progam displays the use of a struct and  
 * the creation of the initial node to a linked list
 * 
 * incl. free malloc order
 *  
 * using a function to create the node - YES!
 * 
 **/

#include <stdio.h>
#include <cs50.h>
#include <string.h>

typedef struct nodeList
{
    char* word;
    struct nodeList* next;
}
node;

void tempString(FILE* fileName, node* nodeName);
int wordCount(FILE* fileName);

int main(void)
{
    
    // read in file with error check
    FILE* file = fopen("NodeFile", "r");
    if (file == NULL)
    {
        printf("File Error\n");
        return 1;
    }
    
    // create node;
    node* root = malloc(sizeof(node));
    
    tempString(file, root);
    
    printf("Word: %s\n", root->word);

    fclose(file);
    free(root->word);
    free(root->next);
    free(root);
}

// function to create temp buffer & memory for word member
void tempString(FILE* fileName, node* nodeName)
{
    char buffer[15];

    fscanf(fileName, "%14s", buffer);

    // create pointer to node root
    nodeName->next = NULL;
    
    int len = strlen(buffer);
    nodeName->word = malloc((len + 1) * sizeof(char));
    strcpy(nodeName->word, buffer);
    
    
    // calls wordCount to count words in dictionary
    wordCount(fileName);
}

// function to count words
int wordCount(FILE* fileName)
{
    int count = 0;
    char ch;
    
    while((ch = fgetc(fileName)) != EOF)
    {
        if(ch == '\n')
            count++;
    }
    printf("Word Count: %i\n", count);
    return count;
}