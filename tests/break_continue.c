#include <stdio.h>

int main(void)
{
    for (int i = 1; i < 100; i++)
    {
        printf("Testing %d\n", i);
        
        if ((i % 3 == 0) && (i % 4) == 0)
        {
            printf("Found it!\n");
            break;
        }
        if (i % 3 == 0)
        {
            printf("I am divisible by 3.\n");
            printf("But that is only half the test!\n");
            continue;
        }
        if (i % 4 == 0)
        {
            printf("I am divisible by 4.\n");
            printf("One out of two isn't bad!\n");
            continue;
        }
        printf("I am not divisible by 3 or 4!\n");
    }
    return 0;
}
    
