

#include <stdio.h>
#include <stdbool.h>
#include <cs50.h>
#include <ctype.h>
#include <string.h>

#define LENGTH 45

// prototypes: load
typedef struct _node node;
void insert_nodes(node* root, char* storage, int max);
node create_node(void);

// structs
typedef struct _node
{
    bool is_word;
    struct _node* children[27];
}
node;

// prototypes: size 
unsigned int size(void);

int main(void)
{
    // bring in the chars - with error-checking
    int c;
    
    FILE* file = fopen("large", "r");
    if (file == NULL)
    {
        printf("Input File Error\n");
        return INT_MAX;
    }
    else
    {
        c = fgetc(file);
        if (c == EOF)
        {
            printf("File Error 5: Input File Blank\n");
            return 5;
        }
    }
    
    // create the root
    node* root = create_node();
    if (root == NULL)
    {
        printf("Node Creation Error");
        return INT_MAX-1;
    }

    // create index to use instead of c, for temp array
    int index = 0;

    
    char storage[LENGTH] = {0};
    // count used to store a whole word in array, "between" \n, that is
    int count = 0;

    // check each letter & store into array lower with '\'' and '\n'
    for ( ; c != EOF; c = fgetc(file))
    {
        if (c != '\n')
        {
            if (isalpha(c) || (c == '\''))
            {
                // convert character to lower or take apostrophe - store
                if (c == '\'')
                {
                    index = '\'';
                }
                else if (isupper(c))
                {
                    index = tolower(c);
                }
                else
                {
                    index = c;
                }
            }
            // store each file word into array - helps with checking each char
            storage[count] = index;
            count++;
            
            
        }
        // this is where the magic starts; try the trie, baby!
        else if (c == '\n')
        {
            // Restart count in anticipation of next word
            count = 0;
 
            int max = strlen(storage);
            insert_nodes(root, storage, max);
        }
    }
    printf("EOF reached\n");
}

// create node init to NULLS
void insert_nodes(node* root, char* storage, int max)
{
    node* crawler = create_node();
    crawler = root;
    int i;
    
    for (i = 0; i < max; i++)
    {
        // if null terminator before ASCII conversion - do nothing
        if (storage[i] != '\0')
        {
            int itt = (storage[i] == '\'') ? (storage[i] = 26) : (storage[i] -= 'a');;
        
            if (crawler->children[itt] == NULL)
            {
                node* new_node = create_node();
                crawler->children[itt] = new_node;
                crawler = crawler->children[itt];
            }
            else
            {
                crawler = crawler->children[itt];
            }
        }
        else
        {
            // do nothing
        }
    }
    crawler->is_word = true;
    // re-init storage to NULL for next word
    for (int n = 0; n < LENGTH; n++)
    {
        storage[n] = 0;
    }
    //free(crawler);
}

node* create_node(void)
{
    node* new_node = malloc(sizeof(node));
    
    if (new_node == NULL)
    {
        printf("Create new_node Error\n");
        return NULL;
    }
    // init root - dunno if this is neccesary;
    new_node -> is_word = NULL;
    for (int i = 0; i < 26; i++)
    {
        new_node->children[i] = NULL;
    }
    return new_node;
}

unsigned int size(void)
{
    unsigned int count_words = 0;
    count_words++;
    
    return count_words;
}

