#include <stdio.h>

#define MAXIMUM 16

void sortit(int array[], int max);

int main(void)
{
    printf("Provide %i ints, please:\n", MAXIMUM);
    
    int unsorted[MAXIMUM];
    for (int i = 0; i < MAXIMUM; i ++)
    {
        scanf("%i", &unsorted[i]);
    }
    
    sortit(unsorted, MAXIMUM);
    
    printf ("\n\nThe array after the sort:\n");
    
    for (int i = 0; i < MAXIMUM; i++)
    {
        printf("%i ", unsorted[i]);
    }
    printf ("\n");
    return 0;
}

void sortit(int array[], int max)
{
    int i, j, temp;
    
    /**
     * Array: 3 2 1
     * first itteration
     * loop 1 is:       i = 3
     *      loop 2 is:      j = 2, swap
     *                  i = 3
     *                      j = 1, swap
     * second itteration
     * loop 1 is:       i = 2
     *      loop 2 is       j = 1, swap
     * 
     * third itteration
     * loop 1 is:       i = 1
     *      loop 2 is       j = 1, swap                                 
     * 
     * */
    
    for (i = 0; i < max - 1; i++)
    {
        for (j = i + 1; j < max; j++)
        {
        if (array[i] > array[j])
            {
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            }
        }
    }
}