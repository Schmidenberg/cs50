#include <stdio.h>

int main(void)
{
    unsigned int j = 4294967295; // an example of the largest 'unsigned int'
    int k = 2147483647; // an example of the largest + 'int'
    long int i = 9223367638808264703; // an example of the largest 'long int'
    int ans = 5 + 2 * 4 / 2 % 3 + 10 - 3;
    int h = 2, g = 5, f;
    f = h++ * g;
    int l = sizeof(int);
    float flo = 1.2;

    printf("SIZEOFINT\t:\t%d\nTRY\t\t:\t%i\nANSWER\t\t:\t%i\nLARGEST OF:\nINT\t\t:\t%i\nUNSIGNED\t:\t%u\nLONG INT\t:\t%li\n",l,f,ans,k,j,i);
    printf("FLOAT:%.2f\n", flo);
}

