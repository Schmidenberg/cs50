/**
 * working with arrays
 * 
 **/
 

#include <stdio.h> 
#include <string.h>

int main(int argc, char *argv[]) 
{ 
char x[5] = "kite"; 
int j = strlen(x-1);

for (int i = 0 ; i < j; i++)

// shows how to print the array as a string or each char
printf("%i:%s      ,%c\n",i,x,x[i]); 

printf("add a new string, this uses strcpy()\n");

char h;

scanf("%s", &h);

strcpy(x , &h);
printf("%s\n",x); 
}