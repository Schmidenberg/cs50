/**
 * Program to calculate the absolute value of a float - using a function
 *
 * Here, the function calls a function - not main()
 * 
 * includes error checking for negative input. The "return"  is strange...
 * 
 **/

 
#include <stdio.h>

float absoluteResult(float);
void squareRootIt(float);

int main (void)
{
    float number;
    printf ("Type in your floating point number to find it's square root: ");
    scanf ("%f", &number);

    squareRootIt(number);
    // error checking for -float
    /**
     * the "return"  is strange... 
     * the function returns a non 0 - but main then needs to check for the 
     * non zero return, i thought it would be equivalnet to "break;"
     * 
     * FYI: I ran the code form the textbook: "Programming in C" page 140, and it, 
     * too, returns the -1.0 but the rest of main runs thereafter. I.e. it's wrong - 
     * it should break;...
     * 
     * */
   
}

/************************************/

//function to provide absolute of float
float absoluteResult(float i)
{
    if ( i < 0 )
    {
        i = -i;
    }
    return i;
}

// function that uses absolute and returns to main - -1 returned for negative input
void squareRootIt(float i)
{
    float guess = 1.0;
    float e = 0.00001;
    
    if (i < 0)
    {
        printf("Negative argument to squareRoot not allowed.\n");
    }
    else
    {
        while (absoluteResult (guess * guess - i) >= e)
        {
            guess = (i / guess + guess) / 2.0;
            printf("Guess is %f\n", guess);
        }
        printf("The sqaure root of %f is %f\n", i, guess);
    }
}
