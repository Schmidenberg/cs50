/**
 * A program to sum user-filled n sized array.
 * 
 * makes particular use of usage after assigment
 *   
 **/

#include <stdio.h>
int arraySum (int n, int j[n]);

int main (void)
{
    int number;
    
    printf("Please provide an int for array size: \n");
    scanf("%i", &number);
    
    int array[number];
    printf("Please provide %i numbers to fill the array: \n", number);
    
    for (int i = 0 ; i < number; i ++)
    {
        scanf("%i", &array[i]);
    }
    
    int result = arraySum(number, array);
    printf("The sum of the array is: %i\n", result);
}

// Recursive function to calculate the factorial of a positive integer

int arraySum (int n, int j[n])
{
    int result = 0;
    
    for (int i = 0 ; i < n; i++)
    {
        result += j[i];
    }
    return result;
}