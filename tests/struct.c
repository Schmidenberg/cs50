#include <stdio.h>

typedef struct rec rec_type;

int main(void)
{
    struct rec 
    { 
        int a,b,c; 
        float d,e,f; 
    } r;

r.a = 5;
r.b = 3;
r.c = 1;
r.d = 6;
r.e = 2;
r.f = 0;

printf("a:%i, b: %i, c: %i, d %f, e %f, f %f\n", r.a,r.b,r.c,r.d,r.e,r.f);
    
}
