#include <stdio.h>
/**
 * 
 * shows what a function is, and how a function is called
 * 
 */

void print_header();

// below that shows the function call
int main(void)
{
    print_header(); 
}

// function:
void print_header()
{
    printf("Program Number 1\n");
    printf("by WAZZA\n");
    printf("Version 1.0, released 06/30/16\n");
}
