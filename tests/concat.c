#include <stdio.h>

void concat (char result[], char str1[], int l1, char str2[], int l2);

int main(void)
{
    char str1[6] = {'S', 'u', 'p', 'e', 'r', ' '};
    char str2[4] = {'T', 'e', 'a', 'm'};
    char str3[10];
    int l1 = 6;
    int l2 = 4;
    
    concat (str3, str1, l1, str2, l2);
    
    for (int i = 0; i < 10; i++)
    {
        printf("%c",str3[i]);
    }
    printf("\n");
    
}

void concat (char result[], char str1[], int l1, char str2[], int l2)
{
    int i, j;
    
    for (i = 0; i < l1; i++)
    {
        result[i] = str1[i];
    }
    
    for (j = 0; j < l2; j++)
    {
        result[j + i] = str2[j];
    }
}