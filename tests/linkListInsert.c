#include <stdio.h>

struct link
{
    int value;
    struct link *next;
};

void insertEntry(struct link *newEntry, struct link *current)
{
    (*newEntry).next = (*current).next;
    (*current).next = newEntry;
}

int main (void)
{
    // struct def for links + 3 links
    struct link link0, link1, link3;
    
    // struct to follow list 
    struct link *find = &link0;
    
    //sconst struct link *start = &link0;
    
    // links in chain
    link0.value = 0;
    link0.next = &link1;
    
    link1.value = 1;
    link1.next = &link3;
    
    link3.value = 3;
    link3.next = (struct link *) 0; // end of list
    
    // missing link
    struct link link2;
    link2.value = 2;

    while ( find != (struct link *) 0 )
    {
        if (find->value == link1.value)
        {
            printf("%i\n", find->value);
            insertEntry ( &link2, find);
            find = find->next;
        }
        else
        {
            printf("%i\n", find->value);
            find = find->next;
        }
    }
}
