/* Program to find the greatest common divisor
of two nonnegative integer values */
#include <stdio.h>

int gcd(int i, int j);

int main (void)
{
    
    int u, v;
    printf ("Please type in two nonnegative integers.\n");
    scanf ("%i%i", &u, &v);
    
    // functions return one value - therfeore, this is how to use returned value
    int result = gcd(u, v);
    
    printf ("Their greatest common divisor is %i\n", result);
    // btw, this also worked, but felt wrong: printf ("Their greatest common divisor is %i\n", gcd(u, v));
    // btw x 2: printf ("Their greatest common divisor is %i\n", gcd(u, v)); is actually accepted :D -well done
    
    return 0;
}

/*************************************/
// functions
int gcd(int i, int j)
{
     int temp = 0;
     while ( j != 0 ) 
    {
        temp = i % j;
        i = j;
        j = temp;
    }
    return i;
}
