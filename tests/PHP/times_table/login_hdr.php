<div id="login">
    <?php if(($_SESSION['name']) == ""): ?>
        <?php header("Location: login.php"); ?>
    <?php elseif(isset($_SESSION['name'])): ?>
        <?= $_SESSION['name'] ?>
         - <a style="color: white;" href="logout.php">Logout</a>  <!--inline styling -->
    <?php else: ?>
        <a style="color: white;" href="login.php">Login</a> <!--inline styling -->
    <?php endif; ?>
</div>
