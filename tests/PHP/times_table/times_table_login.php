<?php

session_start();

?>

<!DOCTYPE html>

<html>
    <head>
        <link href="times_table.css" rel="stylesheet"/>
        <title>Multiplication form</title>
    </head>
    <body>
        <div id = "main">
            <?php require(__DIR__.("/login_hdr.php")); ?>
            <form action="times_table.php" method="post">
                Give me a number: <input name="int" type="text" placeholder = "integer..." />
                <input type="submit" />
            </form>
        </div>
    </body>
</html>
