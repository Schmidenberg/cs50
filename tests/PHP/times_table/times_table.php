<?php

session_start();

?>

<!DOCTYPE html>

<html>
    <head>
        <title>Results</title>
       <link href="times_table.css" rel="stylesheet"/>
    </head>
    <body>
        <div id="tbl_layout">
            <?php require(__DIR__.("/login_hdr.php")); ?>
            <table>
                <?php 
                
                    $int = $_POST["int"];
    
                    for ($n = 1; $n <= $int; $n++)
                    {
                        for($i = 1; $i <= $int; $i++)
                        {
                            $result = ($n * $i);
                             echo ("<td>".$result."</td>");
                        }
                        echo ("<tr>\n</tr>");
                    }
                ?>
            </div>
        </table>
    </body>
</html>
