#include<stdio.h>
#include<cs50.h>

bool valid_triangle(int a, int b, int c);

int main(void)
{
    //take the arguments 
    printf("provide the sides of the triangle: \n");
    
    int sides[3];
    
    for(int i = 0; i < 3; i++)
    {
        printf("Side %i: \n", i);
        scanf("%i", &sides[i]);
    }
    
    // provide values
    bool result = valid_triangle(sides);
    
    // print result
    if(result)
    {
        printf("That is a valid triangle\n");
    }
    else
    {
        printf("That is NOT a valid triangle\n");
    }
}

/****************************************/

//function declaration
bool valid_triangle(int a, int b, int c)
{
    if (a <= 0 || b <= 0 || c <= 0)
    {
        return false;
    }
    else if (a + b >= c || a + c >= b || b + c >= a)
    {
        return true;
    }
    else
    {
        return false;
    }
}
