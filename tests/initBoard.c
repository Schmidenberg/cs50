#include <stdio.h>

void init(void);
void print(void);
void add(void);

int board[4][4];

int d = 4;

int main(void)
{
    init();
    print();
    
}


void print(void)
{
    for (int i = 0; i < d; i++)
    {
        for (int j = 0; j < d; j++)
        {
            printf("board%i\n", board[i][j]);
        }
    }
}

void init(void)
{
    int row = 0;
    int column = 0;
    int board_number = d * d - 1;
    
    // need to check if board tiles are even or odd - swap 1 and 2 if odd
    if (d % 2 == 1)
    {
        for (row = 0 ; row < d ; row++)
        {
            for (column = 0 ; column < d; column++)
            {
                board[row][column] = board_number;
                board_number--;
            }
        }
    }
    else
    {
        for (row = 0 ; row < d ; row++)
        {
            for (column = 0 ; column < d; column++)
            {
                // with a board size even - 1 and 2 swap
                if (board_number == 2)
                {
                    board[row][column] = 1;
                    board_number--;
                }
                else if (board_number == 1)
                {
                    board[row][column] = 2;
                    board_number--;
                }
                else
                {
                    board[row][column] = board_number;
                    board_number--;
                }
            }
        }
    }
}