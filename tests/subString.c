#include <stdio.h>

void substring(char source[], int start, int count, char result[])
{
    int x = start + count;
    int j = 0;
    
    for (int i = start; i < x; i++)
    {
        result[j] = source[i];
        j++;
    }
    result[x] = '\0';
}


int main(void)
{
    char whole[] = "Two Words";
    int startNum = 4;
    int countNum = 20;
    char result[countNum + 1];
    
    substring(whole, startNum, countNum, result);
    
    printf("%s\n", result);
}

// 0 1 2 3 4 5 6 7 8 9 
// C h a r a c t e r \0