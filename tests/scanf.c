// Program to illustrate the %s scanf format characters
#include <stdio.h>
int main (void)
{
    char s1[9], s2[9], s3[9];
    printf ("Enter text:\n");
    scanf ("%8s%8s%8s", s1, s2, s3);
    printf ("\ns1 = %s\ns2 = %s\ns3 = %s\n", s1, s2, s3);
}