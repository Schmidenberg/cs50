/**
 * recover.c
 *
 * Computer Science 50
 * Problem Set 4
 * Warren R. Schmidenberg
 *
 * Recovers 16 JPEGs from a forensic image.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef uint8_t EACHBYTE;

int main(int argc, char* argv[])
{
    char* infile = "card.raw";
  
    // open the sd card file
    FILE* inptr = fopen(infile, "r");
    if (inptr == NULL)
    {
        printf("Could not open 'card.raw'\n");
        return 1;
    }
    
    // open output file - not sure if i need to open it now or after each read...
    FILE* outptr = NULL;
    
    // read each block of 512
    EACHBYTE block[512];
    
    // counter used for .jpg description, are we in a jpg currenlty?
    int countjpg = 0, isinjpg = 0;
     
    // array of 8 for description
    char outfile [8];
    
    // while not at end of file
    while (fread(block, sizeof(block), 1, inptr) == 1)
    {
        // read in the block + check if is jpg
		// fread(block, sizeof(block), 1, inptr);
        {
            if (block[0] == 0xff && block[1] == 0xd8 && block[2] == 0xff && (block[3] == 0xe0 || block[3] == 0xe1))
            {
                // close current jpg
                if (isinjpg == 1)
                fclose(outptr);

                // if it is jpg then buffer and start to write to file
                sprintf(outfile, "%03d.jpg", countjpg);
                countjpg++;
                outptr = fopen(outfile, "w");
                    if (outptr == NULL)
                    {		
                        fclose(inptr);
                        fprintf(stderr, "Could not create %s.\n", outfile);
                        return 3;
                    }
                fwrite(block, sizeof(block), 1, outptr);
                isinjpg = 1;
            }
                
            // if not jpg or if already in jpg
            else 
            {
                if (isinjpg == 1)
                {
                    fwrite(block, sizeof(block), 1, outptr);
                }
                else if (block[0] == 0xff && block[1] == 0xd8 && block[2] == 0xff && (block[3] == 0xe0 || block[3] == 0xe1))
                    {
                    if (isinjpg == 1)
                    fclose(outptr);

                    // if it is jpg then buffer and start to write to file
                    sprintf(outfile, "%03d.jpg", countjpg);
                    countjpg++;
                    outptr = fopen(outfile, "w");
                        if (outptr == NULL)
                        {
                            fclose(inptr);
                            fprintf(stderr, "Could not create %s.\n", outfile);
                            return 4;
                        }
                    fwrite(block, sizeof(block), 1, outptr);
                    isinjpg = 1;
                }
            }
        }
    }
	fclose(inptr);
    fclose(outptr);   
    return 0;
}
