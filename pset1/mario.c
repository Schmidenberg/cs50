/**
 * mario.c
 * 
 * pset 1
 * 
 * genrate mario pyramid using given input
 * 
 * 
 */

#include <stdio.h>
#include <cs50.h>

int main(void)
{
// request height

    int height = 0;

// if height worng, repeat
    do
    {
        printf("Height: ");
        height = GetInt();
    }
    while (height > 23 || height < 0);

// get spacing and pound correct    

    for (int j = height; j > 0; j--)
    {
        for (int i = j; i > 1; i--)
        {
            printf(" ");
        }
        for (int k = 0; k < (height - j); k++)
        {
            printf("#");
        }
        printf("##");
        printf("\n");
    }
}
