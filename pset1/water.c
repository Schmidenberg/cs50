
/**
 * water.c 
 * 
 * calculates the number of bottles of water from a n minute shower
 * 
 */

#include <stdio.h>
#include <cs50.h>

int main(void)
{
    printf("minutes: ");
    {
        int n = GetInt();
        {
            n = n * 192 / 16;
            printf("bottles: %i\n", n);
        }
    }
}