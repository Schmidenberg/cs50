/**
 * greedy.c
 *
 * counts number of coind needed from change required.
 * 
 * 
 */
 
#include <stdio.h>
#include <cs50.h>
#include <math.h>

int main(void)
{
    // get change needed
    printf("O hai! How much change is owed?\n");
    float f_change = GetFloat();
    
    // correct figure input
    while (f_change < 0.0) 
    {
        printf("How much change is owed?\n");
        f_change = GetFloat();
    }
    
    // convert from float to int - while preserving decimals
    f_change = roundf(f_change * 100);
    int change = (int) f_change;

    // count coins
    int quarter = 25;
    int dime = 10;
    int nickel = 5;
    int penny = 1;
    int count = 0;
    
    do
    {
        while (change >= quarter)
        {
            change -= quarter;
            count++;
        }
        while (change >= dime)
        {
            change -= dime;
            count++;
        }
        while (change >= nickel)
        {
            change -= nickel;
            count++;
        }
        while (change >= penny)
        {
            change -= penny;
            count++;
        }
    }
    while (change > 0);
    printf("%i\n", count);
}