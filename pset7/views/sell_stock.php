<form action="sell.php" method="post">
    <fieldset>
        <div class="form-group">
            <select class="form-control"  name="symbol">
                <option disabled selected value>Select Symbol Ticker</option>
                <?PHP
                    foreach($list_stock as $list_stocks)
                    {
                        print("<option>".strtoupper($list_stocks['symbol'])."</option>");
                    }
                    endfor;
                ?>
            </select>
        </div>
        <br/>
        <div class="form-group">
            <button class="btn btn-default" type="submit">
                Sell It!
            </button>
        </div>
    </fieldset>
</form>
