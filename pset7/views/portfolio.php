<form action="index.php" method="post">
    <fieldset>
        <table class = "table table-striped">
             
                <?php
                    print("<tr>");
                        print("<td><strong>Ticker Symbol</strong></td>");
                        print("<td><strong>Stock Name</strong></td>");
                        print("<td><strong>Shares Owned</strong></td>");
                        print("<td><strong>Cost / Share</strong></td>");
                        print("<td><strong>TOTAL</strong></td>");
                    print("</tr>");

                foreach ($positions as $position)
                {
                    print("<tr>");
                        print("<td>" . strtoupper($position["symbol"]) . "</td>");
                        print("<td>" . $position["name"] . "</td>");
                        print("<td>" . $position["shares"] . "</td>");
                        print("<td>" . "$". number_format($position["price"], 2, '.', '') . "</td>");
                        print("<td>" . "$".  number_format($position["price"]*$position["shares"], 2, '.', '') . "</td>");
                    print("</tr>");
                }
                ?>
            
        </table>
        <br/>
        <table class = "table table-striped">
            
             <?php
            print("<tr>");
            print("<td>" . "<strong>Current Balance :   </strong>" . "</td>");
            print("<td>" . "<strong>" . "$". number_format($balance[0]["cash"], 2, '.', '') . "</strong>" . "</td>");
            print("</tr>");
            ?>
            
        </table>
        <br/>
        <br/>
        <br/>
    </fieldset>
</form>
