<form action="register.php" method="post">
    <fieldset>
        <div class="form-group">
            <input autocomplete="off" autofocus class="form-control" name="username" placeholder="Enter Username" type="text"/>
        </div>
        <div class="form-group">
            <input autocomplete="off" input class="form-control" name="email" placeholder="Email" type="text"/>
            <input autocomplete="off"input class="form-control" name="confirmail" placeholder="Email Confirmation" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="password" placeholder="Password" type="password"/>
            <input class="form-control" name="confirmation" placeholder="Password Confirmation" type="password"/>
        </div>
            <button class="btn btn-default" type="submit">
                <span aria-hidden="true" class="glyphicon glyphicon-log-in"></span>
                Register
            </button>
    </fieldset>
</form>
<div>
    or <a href="login.php">log in</a>
</div>
