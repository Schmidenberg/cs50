<form action="change.php" method="post">
    <fieldset>
        <div class="form-group">
            <input class="form-control" name="new_pwd" placeholder="New Password" type="password"/>
        </div>
         <div class="form-group">
            <input class="form-control" name="pwd_again" placeholder="Retype New Password" type="password"/>
        </div>
        <div class="form-group">
            <button class="btn btn-default" type="submit">
                <span aria-hidden="true" class="glyphicon glyphicon-wrench"></span>
                Change Password
            </button>
        </div>
    </fieldset>
</form>
