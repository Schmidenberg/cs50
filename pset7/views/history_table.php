<form action="history.php" method="get">
    <fieldset>
        <table class = "table table-striped">
            <?php
                print("<tr>");
                print("<td>" . "<strong>Current Balance :   </strong>" . "</td>");
                print("<td>" . "<strong>" . "$". number_format($balance[0]["cash"], 2, '.', '') . "</strong>" . "</td>");
                print("</tr>");
            ?>
        </table>
        <br/>
        <table class = "table table-striped">
            <?php
                print("<tr>");
                    print("<td><strong>Transaction</strong></td>");
                    print("<td><sorted><strong>Date Time (Descending - NASDAQ EST)</strong></sorted></td>");
                    print("<td><strong>Ticker Symbol</strong></td>");
                    print("<td><strong>Company Name</strong></td>");
                    print("<td><strong>Number of Shares</strong></td>");
                    print("<td><strong>Cost</strong></td>");
                print("</tr>");
    
            foreach (array_reverse($positions) as $position)
            {
                print("<tr>");
                    print("<td>" . $position["transaction"]. "</td>");
                    print("<td>" . $position["date_time"]. "</td>");
                    print("<td>" . strtoupper($position["symbol"]) . "</td>");
                    print("<td>" . $position["name"] . "</td>");
                    print("<td>" . $position["shares"] . "</td>");
                    print("<td>" . "$". number_format($position["bought_sold"], 2, '.', '') . "</td>");
                print("</tr>");
            }
            ?>
        </table>
        <br/>
        <br/>
        <br/>
    </fieldset>
</form>
