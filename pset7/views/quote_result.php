<?php
    // used for money_format()
    setlocale(LC_MONETARY, 'en_US.UTF-8');
    
    echo("A share of ".$stock["name"]." (".$stock["symbol"].")"." costs "."<strong>$".number_format($stock["price"], 2, '.', '')."</strong>\n");
?>
