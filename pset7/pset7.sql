-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: pset7
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `transaction` varchar(6) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `date_time` datetime NOT NULL,
  `stock_symbol` varchar(5) NOT NULL,
  `stock_amount` varchar(255) NOT NULL,
  `bought_sold` decimal(65,4) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=latin1 COMMENT='Audit Trail';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` VALUES (22,'BOUGHT','57','2016-11-11 15:35:41','FB','10',119.4500),(23,'BOUGHT','57','2016-11-11 15:35:58','GOOGL','9',772.9050),(24,'BOUGHT','57','2016-11-11 15:36:04','TWTR','2',18.3288),(25,'BOUGHT','57','2016-11-11 15:36:10','FREE','11',1.1500),(26,'BOUGHT','57','2016-11-11 15:36:37','LB','1',67.8800),(27,'BOUGHT','57','2016-11-11 15:37:12','BUD','1',107.6300),(28,'BOUGHT','57','2016-11-11 15:38:37','AMS','1',3.2000),(29,'SOLD','57','2016-11-11 15:38:45','AMS','1',3.2000),(30,'BOUGHT','57','2016-11-11 15:38:54','HEIA','1',62.2500),(31,'SOLD','57','2016-11-11 15:39:06','HEIA','1',62.2500),(32,'BOUGHT','57','2016-11-11 15:39:34','HINKF','1',76.9300),(33,'SOLD','57','2016-11-11 15:40:11','HINKF','2',76.9300),(34,'BOUGHT','57','2016-11-11 16:11:38','FB','1',118.5102),(35,'BOUGHT','57','2016-11-11 16:35:38','FB','1',118.4100),(36,'BOUGHT','57','2016-11-11 16:44:13','FB','1',118.5550),(37,'BOUGHT','57','2016-11-11 17:05:48','FB','1',118.4800),(38,'BOUGHT','57','2016-11-11 17:06:58','FB','1',118.5200),(39,'BOUGHT','57','2016-11-11 17:07:24','FB','1',118.5127),(40,'BOUGHT','57','2016-11-11 17:08:13','FREE','1',1.1500),(41,'BOUGHT','57','2016-11-11 17:08:48','FREE','1',1.1500),(47,'BOUGHT','57','2016-11-11 17:48:33','FB','1',119.4400),(48,'SOLD','57','2016-11-11 17:49:39','FB','18',119.4300),(49,'BOUGHT','57','2016-11-11 17:57:20','FREE','1',1.1500),(50,'BOUGHT','57','2016-11-11 18:12:48','FREE','1',1.1500),(53,'BOUGHT','57','2016-11-11 18:39:48','FB','1',119.0550),(55,'BOUGHT','57','2016-11-11 14:46:15','FREE','12',1.1500),(56,'SOLD','57','2016-11-11 13:47:48','FB','2',119.0500),(57,'BOUGHT','62','2016-11-11 17:24:20','FB','10',119.0200),(58,'BOUGHT','62','2016-11-11 17:55:41','FB','1',119.0200),(59,'BOUGHT','57','2016-11-15 12:27:22','FREE','1',1.1500),(60,'BOUGHT','57','2016-11-15 12:27:34','FREE','1',1.1500),(61,'BOUGHT','57','2016-11-15 12:28:11','FREE','1',1.1500),(62,'BOUGHT','57','2016-11-15 12:30:41','FREE','1',1.1500),(63,'BOUGHT','57','2016-11-15 12:31:01','FREE','1',1.1500),(64,'BOUGHT','57','2016-11-15 12:31:11','FREE','1',1.1500),(65,'BOUGHT','57','2016-11-15 12:31:47','FREE','1',1.1500),(66,'BOUGHT','57','2016-11-15 12:31:49','FREE','1',1.1500),(67,'BOUGHT','57','2016-11-15 12:32:05','FREE','2',1.1500),(68,'BOUGHT','57','2016-11-15 12:32:06','FREE','2',1.1500),(69,'BOUGHT','57','2016-11-15 12:35:05','FREE','1',1.1500),(70,'BOUGHT','57','2016-11-15 12:35:35','FREE','1',1.1500),(71,'BOUGHT','57','2016-11-15 12:35:55','FREE','1',1.1500),(72,'BOUGHT','57','2016-11-15 12:36:13','FREE','1',1.1500),(73,'BOUGHT','57','2016-11-15 12:40:38','FREE','1',1.1500),(74,'BOUGHT','57','2016-11-15 12:42:17','FREE','1',1.1500),(75,'BOUGHT','57','2016-11-15 12:44:41','FREE','1',1.1500),(76,'BOUGHT','57','2016-11-15 12:51:05','FREE','1',1.1500),(77,'BOUGHT','57','2016-11-15 12:51:53','FREE','1',1.1500),(78,'BOUGHT','57','2016-11-15 12:53:45','FREE','1',1.1500),(79,'BOUGHT','57','2016-11-15 12:56:57','FREE','1',1.1500),(80,'BOUGHT','57','2016-11-15 12:57:53','FREE','1',1.1500),(81,'BOUGHT','57','2016-11-15 12:58:50','FREE','1',1.1500),(82,'BOUGHT','57','2016-11-15 12:59:13','FREE','1',1.1500),(83,'BOUGHT','57','2016-11-15 13:02:07','FREE','1',1.1500),(84,'BOUGHT','57','2016-11-15 13:02:40','FREE','1',1.1500),(85,'BOUGHT','57','2016-11-15 13:05:49','FREE','1',1.1500),(86,'BOUGHT','57','2016-11-15 13:06:11','FREE','1',1.1500),(87,'BOUGHT','57','2016-11-15 13:08:42','FREE','1',1.1500),(88,'BOUGHT','57','2016-11-15 13:11:48','FREE','1',1.1500),(89,'BOUGHT','57','2016-11-15 13:12:53','FREE','1',1.1500),(90,'BOUGHT','57','2016-11-15 13:13:34','FREE','1',1.1500),(91,'BOUGHT','57','2016-11-15 13:16:37','FREE','1',1.1500),(92,'BOUGHT','57','2016-11-15 13:17:14','FREE','1',1.1500),(93,'BOUGHT','57','2016-11-15 13:17:55','FREE','1',1.1500),(94,'BOUGHT','57','2016-11-15 13:20:25','FREE','1',1.1500),(95,'BOUGHT','57','2016-11-15 13:21:05','FREE','1',1.1500),(96,'BOUGHT','57','2016-11-15 13:22:44','FREE','1',1.1500),(97,'BOUGHT','57','2016-11-15 13:27:39','FREE','1',1.1500),(98,'BOUGHT','57','2016-11-15 13:33:43','FREE','1',1.1500),(99,'BOUGHT','57','2016-11-15 13:34:12','FREE','1',1.1500),(100,'BOUGHT','57','2016-11-15 13:38:13','FREE','1',1.1500),(101,'SOLD','57','2016-11-15 13:43:54','TWTR','2',19.0900),(102,'SOLD','57','2016-11-15 13:44:06','TWTR','0',19.0700),(103,'SOLD','57','2016-11-15 13:44:11','TWTR','0',19.0700),(104,'SOLD','57','2016-11-15 13:44:40','TWTR','0',19.0700),(105,'SOLD','57','2016-11-15 13:46:19','FREE','80',1.1500),(106,'SOLD','57','2016-11-15 13:46:23','FREE','0',1.1500),(107,'SOLD','57','2016-11-15 13:46:25','FREE','0',1.1500),(108,'SOLD','57','2016-11-15 13:47:49','FREE','0',1.1500),(109,'SOLD','57','2016-11-15 13:47:51','FREE','0',1.1500),(110,'SOLD','57','2016-11-15 13:47:56','GOOGL','9',773.9900),(111,'SOLD','57','2016-11-15 13:47:58','GOOGL','0',773.9900),(112,'SOLD','57','2016-11-15 13:53:18','HINKF','3',76.9300),(113,'SOLD','57','2016-11-15 13:55:04','BUD','1',103.0400),(114,'BOUGHT','57','2016-11-15 13:57:37','GOOGL','12',773.7000),(115,'BOUGHT','57','2016-11-15 15:12:14','FREE','1',1.1500),(116,'BOUGHT','57','2016-11-15 15:14:35','FREE','1',1.1500),(117,'BOUGHT','57','2016-11-15 15:14:58','FREE','1',1.1500),(118,'BOUGHT','57','2016-11-15 15:18:15','FB','1',117.7300),(119,'SOLD','57','2016-11-15 15:18:42','FREE','3',1.1500),(120,'SOLD','57','2016-11-15 15:18:46','GOOGL','12',780.0000),(121,'SOLD','57','2016-11-15 15:18:49','FB','2',117.7100),(122,'SOLD','57','2016-11-15 15:18:53','LB','1',68.2500),(123,'BOUGHT','57','2016-11-15 15:18:58','FB','1',117.7200),(124,'BOUGHT','57','2016-11-15 15:19:12','FB','1',117.7000),(125,'BOUGHT','57','2016-11-15 15:20:00','FB','1',117.5500),(126,'BOUGHT','57','2016-11-15 15:20:17','FB','1',117.5500),(127,'BOUGHT','57','2016-11-15 15:20:30','FB','1',117.5250),(128,'BOUGHT','57','2016-11-15 15:20:58','FB','1',117.5100),(129,'BOUGHT','57','2016-11-15 15:24:08','FB','1',117.5300),(130,'BOUGHT','57','2016-11-15 15:24:23','FB','1',117.5050),(131,'BOUGHT','57','2016-11-15 15:28:25','FB','1',117.4200),(132,'BOUGHT','57','2016-11-15 15:31:04','FB','1',117.3110),(133,'BOUGHT','57','2016-11-15 15:31:56','FREE','1',1.1500),(134,'BOUGHT','57','2016-11-15 15:33:50','FREE','1',1.1500),(135,'SOLD','57','2016-11-15 15:35:41','FB','10',117.4700),(136,'BOUGHT','65','2016-11-15 15:43:20','GOOGL','10',778.0300),(137,'BOUGHT','65','2016-11-15 15:43:37','FB','10',117.5650),(138,'BOUGHT','65','2016-11-15 15:43:51','TWTR','12',19.0800),(139,'SOLD','65','2016-11-15 15:46:22','FB','10',117.5100),(140,'SOLD','65','2016-11-15 15:48:11','GOOGL','10',777.7700),(141,'SOLD','65','2016-11-15 15:48:13','TWTR','12',19.0700),(142,'BOUGHT','65','2016-11-15 15:59:04','GOOGL','12',777.4864),(143,'BOUGHT','66','2016-11-15 16:03:51','FB','20',117.5400);
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolios`
--

DROP TABLE IF EXISTS `portfolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolios` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `symbol_bought` varchar(5) NOT NULL,
  `shares_owned` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `user_id` (`user_id`,`symbol_bought`),
  UNIQUE KEY `user_id_2` (`user_id`,`symbol_bought`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1 COMMENT='a Table to track user stock purchases';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolios`
--

LOCK TABLES `portfolios` WRITE;
/*!40000 ALTER TABLE `portfolios` DISABLE KEYS */;
INSERT INTO `portfolios` VALUES (89,5,'FB','1'),(91,5,'TWTR','1'),(178,58,'GOOGL','10'),(179,59,'GOOGL','10'),(180,60,'FB','10'),(181,61,'FB','10'),(182,62,'FB','12'),(240,57,'FREE','2'),(245,65,'GOOGL','12'),(246,66,'FB','20');
/*!40000 ALTER TABLE `portfolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `cash` decimal(65,4) unsigned NOT NULL DEFAULT '0.0000',
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'andi','$2y$10$c.e4DK7pVyLT.stmHxgAleWq4yViMmkwKz3x8XCo4b/u3r8g5OTnS',10000.0000,''),(2,'caesar','$2y$10$0p2dlmu6HnhzEMf9UaUIfuaQP7tFVDMxgFcVs0irhGqhOxt6hJFaa',10000.0000,''),(3,'eli','$2y$10$COO6EnTVrCPCEddZyWeEJeH9qPCwPkCS0jJpusNiru.XpRN6Jf7HW',10000.0000,''),(4,'hdan','$2y$10$o9a4ZoHqVkVHSno6j.k34.wC.qzgeQTBHiwa3rpnLq7j2PlPJHo1G',10000.0000,''),(5,'jason','$2y$10$ci2zwcWLJmSSqyhCnHKUF.AjoysFMvlIb1w4zfmCS7/WaOrmBnLNe',9857.6900,''),(6,'john','$2y$10$dy.LVhWgoxIQHAgfCStWietGdJCPjnNrxKNRs5twGcMrQvAPPIxSy',10000.0000,''),(7,'levatich','$2y$10$fBfk7L/QFiplffZdo6etM.096pt4Oyz2imLSp5s8HUAykdLXaz6MK',10000.0000,''),(8,'rob','$2y$10$3pRWcBbGdAdzdDiVVybKSeFq6C50g80zyPRAxcsh2t5UnwAkG.I.2',10000.0000,''),(9,'skroob','$2y$10$395b7wODm.o2N7W5UZSXXuXwrC0OtFB17w4VjPnCIn/nvv9e4XUQK',10000.0000,''),(10,'zamyla','$2y$10$UOaRF0LGOaeHG4oaEkfO4O7vfI34B1W23WqHr9zCpXL68hfQsS3/e',10000.0000,''),(57,'W','$2y$10$psiZiv502U19HQEFG55EG.xMean1CutMg4VN.x.V7UJ0p/5/wlDBa',9733.2830,'w.schmidenberg@gmail.com'),(62,'e','$2y$10$P5qkQUTf2qgXeEJmV6bb0O9Z7jWp.B0qz9Bbh.6khgcIXKy7Mc1eO',8571.7600,''),(64,'waq','$2y$10$0soKclIunBjgt2MnZSoaP.SN1KqjeZ0pbgQ4wdjJXothddfi/81X.',10000.0000,'w@gmail.com'),(65,'MJ','$2y$10$kPxiN4kxPSmbJ96E7LpcHunHdbbXYnr2j16qH/LChRTvek37IaUGC',666.8932,'mjshroeter@gmail.com'),(66,'taxi','$2y$10$TV4C.tlGLYeVWJfuiy3a3eqalTY9VQgoKo66xtVzyOAMihSRBTMC.',7649.2000,'taryn.tegg@gmail.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-15 21:48:22
