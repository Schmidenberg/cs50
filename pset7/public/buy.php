<?php

// configuration
require("../includes/config.php");

// if user reached page via GET (as by clicking a link or via redirect)
if ($_SERVER["REQUEST_METHOD"] == "GET")
{
// else render form
render("buy_form.php", ["title" => "Stock Quote"]);
}
// else if user reached page via POST (as by submitting a form via POST)
else if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // symbol and shares can't be blank
    if (empty($_POST["symbol"]))
    {
        apologize("Enter the Ticker that you wish to purchase.");
    }
    else if (empty($_POST["shares"]))
    {
        apologize("Enter the number of shares that you wish to buy");
    }
    // ensure non-negative int.
    else if (!preg_match("/^\d+$/", $_POST["shares"]))
    {
        apologize("Please buy an whole share - not a fraction thereof.");
    }

    // check symbol validity
    $stock = lookup($_POST["symbol"]);

    if ($stock === false)
    {
        apologize("Invalid Stock Symbol.");
    }
    
    $it_costs = $stock["price"] * $_POST["shares"];
    $balance = CS50::query("SELECT cash FROM users WHERE id = ?", $_SESSION["id"]);
    
    // use number format from array, only if array populated
    $balance = isset($balance) ? number_format($balance[0]["cash"], 2, '.', '') : 0;
    
    // ensure sufficient funds in account
    if ($it_costs > $balance)
    {
        apologize("Haha! You don't have enough money for that purchase");
    }

    // insert shares into portfolio
    else
    {
        // uppercase only for stock post
        $stock = isset($_POST["symbol"]) ? strtoupper($stock["symbol"]) : '';
        
        CS50::query("INSERT INTO portfolios (user_id, symbol_bought, shares_owned) VALUES(?, ?, ?) 
        ON DUPLICATE KEY UPDATE shares_owned = shares_owned + VALUES(shares_owned)", $_SESSION["id"], $stock, $_POST["shares"]);
        
        $new_bal = CS50::query("UPDATE users SET cash = cash - ? WHERE id = ?", $it_costs, $_SESSION["id"] );
    }

    // update history table
    $stock = lookup($_POST["symbol"]);
    
    CS50::query("INSERT INTO history (user_id, transaction, date_time, stock_symbol, stock_amount, bought_sold) 
    VALUES(?, 'BOUGHT', (DATE_SUB(NOW(), INTERVAL 5 HOUR)), ?, ?, ?)", $_SESSION["id"], $stock["symbol"], $_POST["shares"], $stock["price"]);
    
    // redirect confirmation - preventing form resubmission
    $message = [$title = "Bought", $header = "Transaction Complete!", $msg = "Your stock purchase was successful"];

    $_SESSION["response"] = $message;
    redirect("/response.php");
    
}
?>