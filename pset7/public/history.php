<?php
    // configuration
    require("../includes/config.php"); 
    
    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
    
        // determine existing stocks for current user
        $rows = CS50::query("SELECT * FROM history WHERE user_id = ?", $_SESSION["id"]);
        
        $positions = [];
        foreach ($rows as $row)
        {
            $stock = lookup($row["stock_symbol"]);
            if ($rows !== false)
            {
                $positions[] = [
                "date_time" => $row["date_time"],
                "shares" => $row["stock_amount"],
                "name" => $stock["name"],
                "symbol" => $row["stock_symbol"],
                "transaction" => $row["transaction"],
                "bought_sold" => $row["bought_sold"]
                ];
            }
        }
        $balance = CS50::query("SELECT cash FROM users WHERE id = ?", $_SESSION["id"]);
    }
    // render stocks and balance
    render("history_table.php", ["balance" => $balance, "positions" => $positions, "title" => "History"]);
?>
