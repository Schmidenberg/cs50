<?php
// configuration
require("../includes/config.php");

// if user reached page via GET (as by clicking a link or via redirect)
if ($_SERVER["REQUEST_METHOD"] == "GET")
{
// else render form
render("quote_form.php", ["title" => "Stock Quote"]);
}
// else if user reached page via POST (as by submitting a form via POST)
else if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // symbol can't be blank
    if (empty($_POST["symbol"]))
    {
        apologize("You must provide the symbol for the quote.");
    }

    // add user to db
    $stock = lookup($_POST["symbol"]);
    
    // check symbol validity
    if ($stock === false)
    {
        // else apologize
        apologize("Invalid Stock Symbol.");
    }
    
    else
    {
        render("quote_result.php", ["stock" => $stock] );
    }
}
?>