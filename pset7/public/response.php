<?php

    // configuration
    require("../includes/config.php"); 
    
    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        // else render form
        $message = $_SESSION["response"];
        
        if (isset ($message[0]) && isset ($message[1]))
        {
            $title = $message[0];
            $header = $message[1];
            $msg = $message[2];
            
            render("response_form.php", ["title" => $title, "header" => $header, "message" => $msg]);
        }
    }
    else
    {
        apologize("Please Try Again.");
    }
    
?>
