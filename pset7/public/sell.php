<?php
    // configuration
    require("../includes/config.php");
    
// if user reached page via GET (as by clicking a link or via redirect)
if ($_SERVER["REQUEST_METHOD"] == "GET")
{
    $rows = CS50::query("SELECT * FROM portfolios WHERE user_id = ?", $_SESSION["id"]);
        
    // redirect if user has no stocks to sell
    if (empty($rows))
    {
        apologize("I'm afraid that you have no stocks to sell.");
    }
    
    // else populate list of available stocks
    $list_stock = [];
    {
        foreach ($rows as $row)
        {
            $stock = lookup($row["symbol_bought"]);
            if ($stock !== false)
            {
                $list_stock[] = [
                "symbol" => $row["symbol_bought"],
                ];
            }
        }
        
    }
    // determine current balabace for current user
    render("sell_stock.php", ["list_stock" => $list_stock, "title" => "Sell Stock"]);
}
// else if user reached page via POST (as by submitting a form via POST)
else if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    if (empty($_POST["symbol"]))
    {
        apologize("Select Ticker to sell.");
    }
    else
    {
        $share_count = 0;
        
        // ensure that if stock is sold it can't be sold again, refresh
        $rows = CS50::query("SELECT * FROM portfolios WHERE user_id = ? AND  symbol_bought = ?", $_SESSION["id"], $_POST["symbol"]);
        if (empty($rows))
        {
            apologize("I'm afraid that you do not have that stock to sell.");
        }
        else
        {
            // gather recent stock price
            $stock_now = isset($_POST["symbol"]) ? lookup($_POST["symbol"]) : 0; //indirectly checks if stock is available
            $stock_count = isset($_POST["symbol"]) ? CS50::query("SELECT shares_owned FROM portfolios WHERE user_id = ? AND symbol_bought = ?", $_SESSION["id"], $_POST["symbol"]) : 0;
            
            if (isset($stock_count[0]))
            {
                $owed = (number_format($stock_now["price"], 2, '.', '') * number_format($stock_count[0]["shares_owned"], 2, '.', ''));
                $share_count = $stock_count[0]["shares_owned"];
    
                // update the db with new value
                $new_bal = CS50::query("UPDATE users SET cash = cash + ? WHERE id = ?", $owed, $_SESSION["id"] );
                
                // del stock from db
                $sold = CS50::query("DELETE FROM portfolios WHERE user_id = ? AND symbol_bought = ?", $_SESSION["id"], $_POST["symbol"]);
            }
        }
        $balance = CS50::query("SELECT cash FROM users WHERE id = ?", $_SESSION["id"]);
        
        // update history table
        $stock = lookup($_POST["symbol"]);
        
        CS50::query("INSERT INTO history (user_id, transaction, date_time, stock_symbol, stock_amount, bought_sold) 
        VALUES(?, 'SOLD', (DATE_SUB(NOW(), INTERVAL 5 HOUR)), ?, ?, ?)", $_SESSION["id"], $stock["symbol"], $share_count, $stock["price"]);
        
        // redirect on confirmation - preventing form resubmission
        $message = [$title = "Sold", $header = "Transaction Complete!", $msg = "Your stock was sold - CHA CHING! (hopefully)"];

        $_SESSION["response"] = $message;
        redirect("/response.php");
    }
}
?>
