<?php

    // configuration
    require("../includes/config.php"); 
    
    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
    
        // determine existing stocks for current user
        $rows = CS50::query("SELECT symbol_bought, shares_owned FROM portfolios WHERE user_id = ?", $_SESSION["id"]);
        
        $positions = [];
        foreach ($rows as $row)
        {
            $stock = lookup($row["symbol_bought"]);
            if ($stock !== false)
            {
                $positions[] = [
                "name" => $stock["name"],
                "price" => $stock["price"],
                "shares" => $row["shares_owned"],
                "symbol" => $row["symbol_bought"],
                ];
            }
        }
        // determine current balabace for current user
        $balance = CS50::query("SELECT cash FROM users WHERE id = ?", $_SESSION["id"]);
    }
    
    // render stocks and balance
    render("portfolio.php", ["balance" => $balance, "positions" => $positions, "title" => "Portfolio"]);
?>
