<?php
    /**
     * 
     * Warren R. Schmidenberg - CS50
     * as per: phpmailer.php
     * 
     **/
     
    // configuration
    require("../includes/config.php");
    require("libphp-phpmailer/class.phpmailer.php");
    
    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render("change_pwd.php", ["title" => "Change Password"]);
    }
    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // query database for user
        $email = CS50::query("SELECT email FROM users WHERE id = ?", $_SESSION["id"]);

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->IsHTML(true);
        $mail->SMTPSecure = 'tls';
        $mail->Host = 'smtp.gmail.com'; // change to your email host
        $mail->Port = 587; // change to your email port
        $mail->Username = "w.schmidenberg@gmail.com"; // change to your username
        $mail->Password = "wchnftaclmwilmxw"; // change to your email password
        $mail->setFrom("w.schmidenberg@gmail.com"); // change to your email password
        $mail->AddAddress(implode(" ", $email[0])); // change to user's email address
        $mail->Subject = "Wazzy's Email Password Reset"; // change to email's subject
        $mail->Body = "<h1>hello, world!</h1>\n\nYou requested a password \n
        <a href='https://ide50-warren-schmidenberg.cs50.io/email.php'>reset</a>.\n
        <h5>Thanks,</h5>\n\n<h5>Team Schmidy</h5>"; // change to email's body, add the needed link here

        if ($mail->Send() == false)
        {
            apologize("Email Issue, Sorry. Try Again.");
        }
        else
        {
            // redirect to portfolio on confirmation - preventing form resubmission
            $message = [$title = "Sent", $header = "The email is on the way!", $msg = "Check your mailbox, don't be impatient ;)"];

            $_SESSION["response"] = $message;
            redirect("/response.php");
        }
    }
    
?>