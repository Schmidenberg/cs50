<?php
    // configuration
    require("../includes/config.php");
    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
    // else render form
    render("register_form.php", ["title" => "Register"]);
    }
    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // register can't be blank
        if (empty($_POST["username"]))
        {
            apologize("You must provide your username.");
        }
        else if (empty($_POST["password"]))
        {
            apologize("You must provide your password.");
        }
        else if (empty($_POST["confirmation"]))
        {
            apologize("You must provide your password confirmation.");
        }
        
        // make sure the password and confirmation match
        else if ($_POST["password"] != $_POST["confirmation"])
        {
            apologize("Your password and confirmation do not match.");
        }
        else if ($_POST["email"] != $_POST["confirmail"])
        {
            apologize("Your Emails do not match.");
        }
        
        // ensure email format
        if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) 
        {
            apologize("Invalid email format."); 
        }
        
        // make sur ethat the emails match
        
        // add user to db
        $rows = CS50::query("INSERT IGNORE INTO users (email, username, hash, cash) VALUES(?, ?, ?, 10000.0000)", $_POST["email"], $_POST["username"], password_hash($_POST["password"], PASSWORD_DEFAULT));
        
        if ($rows == 0)
        {
            // else apologize
            apologize("Username already exists.");
        }
            
        // select last inserted ID as active user
        $rows = CS50::query("SELECT LAST_INSERT_ID() AS id");
        
        // get the actual id
        $id = $rows[0]["id"];
    
        // store as current user
        $_SESSION["id"] = $id;
        
        // redirect to portfolio
        redirect("/index.php");
    }
?>