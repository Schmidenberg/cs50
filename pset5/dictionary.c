/**
 * dictionary.c
 *
 * Computer Science 50
 * Problem Set 5
 *
 * Implements a dictionary's functionality.
 */

#include <stdio.h>
#include <cs50.h>
#include <ctype.h>
#include <string.h>

#include "dictionary.h"
#include "structs_functions.h"

/**
 * Returns true if word is in dictionary else false.
 */
bool check(const char* word)
{
    int word_len = strlen(word);
    int index;
    char storage[word_len];
    
    //init the array
    for (int n = 0; n < word_len; n++)
    {
        storage[n] = 0;
    }
    
    // ensure each letter is legit
    for (int i = 0; i < word_len; i++)
    {
        index = word[i];
        if (isalpha(index) || index == '\'')
        {
            if (isupper(index))
            {
                 index = tolower(index);
            }
        }

        storage[i] = index;
    }
    bool result = check_nodes(storage, word_len);
        
    if (result)
    {
        return true;
    }
    return false;
}



/**
 * Loads dictionary into memory.  Returns true if successful else false.
 */
bool load(const char* dictionary)
{
    // create the root
    root = create_root();
    
    // bring in the chars - with error-checking
    int c;
    unsigned int count_words = 0;

    FILE* file = fopen(dictionary, "r");
    if (file == NULL)
    {
        printf("Input File Error\n");
        return false;
    }
    else
    {
        c = fgetc(file);
        if (c == EOF)
        {
            printf("File Error 5: Input File Blank\n");
            return false;
        }
    }
    
    // create index to use instead of c, for temp array
    int index = 0;
    
    char storage[LENGTH] = {'\0'};
    // count used to store a whole word in array, "between" \n, that is
    int count = 0;

    // check each letter & store into array lower with '\'' and '\n'
    for ( ; c != EOF; c = fgetc(file))
    {
        if (c != '\n')
        {
            if (isalpha(c) || (c == '\''))
            {
                // convert character to lower or take apostrophe - store
                if (c == '\'')
                {
                    index = '\'';
                }
                else
                {
                    index = c;
                }
            }
            // store each dictionary word into array - helps with checking each char
            storage[count] = index;
            count++;
        }
        // this is where the magic starts; try the trie, baby!
        else if (c == '\n')
        {
            // Restart count in anticipation of next word
            storage[count] = '\0';
            count = 0;
            
            int max = strlen(storage);
            count_words++;
            insert_nodes(storage, max);
        }
    }
    update_word_count(count_words);
    fclose(file);
    return true;
}

/**
 * Returns number of words in dictionary if loaded else 0 if not yet loaded.
 */
unsigned int size(void)
{
    unsigned int result = return_word_count();
    return result;
}

/**
 * Unloads dictionary from memory.  Returns true if successful else false.
 */
bool unload(void)
{
   return release_nodes();
}
