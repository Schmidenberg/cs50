/**
 * structs_and_functions.c
 *
 * Warren R. Schmidenberg
 * Problem Set 5
 *
 * Removing structs and functions for readability and experience
 */

#include "structs_functions.h"
#include <stdio.h>
#include <cs50.h>

// structs
typedef struct _node
{
    bool is_word;
    struct _node* children[27];
}
node;

typedef struct _root_node
{
    unsigned int word_count;
    node node_in_root;
}
root_node;

// create node init to NULLS
node* create_node(void)
{
    node* new_node = calloc(sizeof(node), 1);
    
    if (new_node == NULL)
    {
        printf("Create new_node Error\n");
        return NULL;
    }
    
    //init root - dunno if this is neccesary;
    new_node -> is_word = NULL;
    for (int i = 0; i < 26; i++)
    {
        new_node->children[i] = NULL;
    }
    return new_node;
}

// root struct differs from node, to accomodate word count
root_node* create_root(void)
{
    root_node* first_node = calloc(sizeof(root_node), 1);
    
    if (first_node == NULL)
    {
        printf("Create new_node Error\n");
        return NULL;
    }

    //init values
    first_node -> node_in_root.is_word = false;
    first_node -> word_count = 0;
    
    for (int i = 0; i < 26; i++)
    {
        first_node -> node_in_root.children[i] = NULL;
    }
    return first_node;
}

// insert node, if necessary
void insert_nodes(char* storage, int max)
{
    node* crawler = &root->node_in_root;
    
    int i;
    
    for (i = 0; i < max; i++)
    {
        // if null terminator before ASCII conversion - do nothing
        if (storage[i] != '\0')
        {
            int itt = (storage[i] == '\'') ? (storage[i] = 26) : (storage[i] -= 'a');
        
            if (crawler->children[itt] == NULL)
            {
                crawler->children[itt] = create_node();
                crawler = crawler->children[itt];
            }
            else
            {
                crawler = crawler->children[itt];
            }
        }
    }
    crawler->is_word = true;
    
    // re-init storage to NULL for next word
    for (int n = 0; n < LENGTH; n++)
    {
        storage[n] = '\0';
    }
}

// check if the node exists - if NULL or is_word = 0, then not a word
bool check_nodes(char* storage, int word_len)
{
    node* crawler = &root->node_in_root;
    
    int i;
    
    for (i = 0; i < word_len; i++)
    {
        // if null terminator before ASCII conversion - do nothing
        if (storage[i] != '\0')
        {
            int itt = (storage[i] == '\'') ? (storage[i] = 26) : (storage[i] -= 'a');
        
            if (crawler->children[itt] == NULL)
            {
                return false;
            }
            else
            {
                crawler = crawler->children[itt];
            }
        }
    }
    // check crawler is_word
    if (crawler -> is_word == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// needs to be in this file... updates the trie with the word count
void update_word_count(unsigned int count_words)
{
    root->word_count = count_words;
}

// needs to be in this file... return the word count
unsigned int return_word_count(void)
{
    unsigned int word_count = root->word_count;
    return word_count;
}

// used to free the nodes
bool release_nodes(void)
{
	bool result;
	
    result = recursive_release(&root -> node_in_root);

    if (!result)
    {
        return false;
    }
    else
    {
        free(root);
        return true;
    }
}

bool recursive_release(node* lead)
{
	int i;
	int max = 27;

	// frees nodes from last to first
	for (i = 0; i < max; i++)
	{
		if (lead->children[i] != NULL)
		{
            recursive_release(lead->children[i]);
		}
	}
	
	if (lead == &root -> node_in_root)
	{
	    return true;
	}
	else
	{
	    free(lead);
	}
    return true;
}
