/**
 * structs_functions.h
 * by Warren R. Schmidenberg
 *
 * Computer Science 50
 * Problem Set 5
 *
 * Declares prototypes for my functions for dictionary functionality.
 */

// below are the prototypes to the functions that I created

#ifndef STRUCTS_FUNCTIONS_H
#define STRUCTS_FUNCTIONS_H
#include <cs50.h>

// Maximum length for a word
// (e.g., pneumonoultramicroscopicsilicovolcanoconiosis)
#define LENGTH 45

/**
 * Nodes to hole structs - root and rest
 */
typedef struct _node node;
typedef struct _root_node root_node;

void insert_nodes(char* storage, int max);
bool check_nodes(char* storage, int word_len);
void update_word_count(unsigned int word_count);
unsigned int return_word_count(void);


node* create_node(void);
root_node* create_root(void);

// RECURSION
bool release_nodes(void);
node* give_root(root_node* root);
bool recursive_release(node* node);

root_node* root;

#endif // STRUCTS_FUNCTIONS_H
