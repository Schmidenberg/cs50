/**
 * helpers.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Helper functions for Problem Set 3.
 */
       
#include <cs50.h>

#include "helpers.h"

/**
 * Returns true if value is in array of n values, else false.
 */
bool search(int value, int values[], int n)
{
    int middle = 0;
    int bottom = 0;
    int top = n - 1;
	    
    while (bottom <= top)
    {
        for (int i = 0; i < n -1; i++)
    	{
        	middle = (top + bottom) / 2;

            if (value < values[middle])
            {
            	top =  middle -1;
            }
            else if (value > values[middle])
            {
                bottom = middle +1;
            }
            else if (value == values[middle])
            {
                return true;
            }
    	}
    }
    return false; 
}

/**
 * Sorts array of n values.
 */
void sort(int values[], int n)
{
    int swap;
    
    do
    {
        swap = 0;
        for (int i = 0; i < n-1; i++)
        {
            if (values[i] > values[i+1])
            {
                int temp = values[i];
                values[i] = values[i+1];
                values[i+1] = temp;
                swap = 1;
            }
        }
    }
    while ( swap != 0);

    return;
}