/**
 * fifteen.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Implements Game of Fifteen (generalized to d x d).
 *
 * Usage: fifteen d
 *
 * whereby the board's dimensions are to be d x d,
 * where d must be in [DIM_MIN,DIM_MAX]
 *
 * Note that usleep is obsolete, but it offers more granularity than
 * sleep and is simpler to use than nanosleep; `man usleep` for more.
 */
 
#define _XOPEN_SOURCE 500

#include <cs50.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// constants
#define DIM_MIN 3
#define DIM_MAX 9

// board
int board[DIM_MAX][DIM_MAX];
int winner[DIM_MAX][DIM_MAX];

// dimensions
int dim;

// prototypes
void clear(void);
void greet(void);
void init(void);
void draw(void);
bool move(int tile);
bool won(void);
bool it_is_right(int i, int j, int tile);
bool it_is_leftt(int i, int j, int tile);
bool it_is_up(int i, int j, int tile);
bool it_is_down(int i, int j, int tile);
void winning_pos(void);

int main(int argc, string argv[])
{
    // ensure proper usage
    if (argc != 2)
    {
        printf("Usage: fifteen d\n");
        return 1;
    }

    // ensure valid dimensions
    dim = atoi(argv[1]);
    if (dim < DIM_MIN || dim > DIM_MAX)
    {
        printf("Board must be between %i x %i and %i x %i, inclusive.\n",
            DIM_MIN, DIM_MIN, DIM_MAX, DIM_MAX);
        return 2;
    }

    // open log
    FILE* file = fopen("log.txt", "w");
    if (file == NULL)
    {
        return 3;
    }
    
    // greet user with instructions
    greet();

    // initialize the board
    init();

    // accept moves until game is won
    while (true)
    {
        // clear the screen
        clear();

        // draw the current state of the board
        draw();

        // log the current state of the board (for testing)
        for (int i = 0; i < dim; i++)
        {
            for (int j = 0; j < dim; j++)
            {
                fprintf(file, "%i", board[i][j]);
                if (j < dim - 1)
                {
                    fprintf(file, "|");
                }
            }
            fprintf(file, "\n");
        }
        fflush(file);

        // check for win
        if (won())
        {
            printf("ftw!\n");
            break;
        }

        // prompt for move
        printf("Tile to move: ");
        int tile = GetInt();
        
        // quit if user inputs 0 (for testing)
        if (tile == 0)
        {
            break;
        }

        // log move (for testing)
        fprintf(file, "%i\n", tile);
        fflush(file);

        // move if possible, else report illegality
        if (!move(tile))
        {
            printf("\nIllegal move.\n");
            usleep(500000);
        }

        // sleep thread for animation's sake
        usleep(20000);
    }
    
    // close log
    fclose(file);

    // success
    return 0;
}

/**
 * Clears screen using ANSI escape sequences.
 */
void clear(void)
{
    printf("\033[2J");
    printf("\033[%d;%dH", 0, 0);
}

/**
 * Greets player.
 */
void greet(void)
{
    clear();
    printf("WELCOME TO GAME OF FIFTEEN\n");
    usleep(200);
}
/**
 * Initializes the game's board with tiles numbered 1 through d*d - 1
 * (i.e., fills 2D array with values but does not actually print them).  
 */
void init(void)
{
    int row = 0;
    int column = 0;
    int board_number = dim * dim - 1;
    
    winning_pos();
    // need to check if board tiles are even or odd - swap 1 and 2 if odd
    if (dim % 2 == 1)
    {
        for (row = 0 ; row < dim ; row++)
        {
            for (column = 0 ; column < dim; column++)
            {
                board[row][column] = board_number;
                board_number--;
            }
        }
    }
    else
    {
        for (row = 0 ; row < dim ; row++)
        {
            for (column = 0 ; column < dim; column++)
            {
                // with a board size even - 1 and 2 swap
                if (board_number == 2)
                {
                    board[row][column] = 1;
                    board_number--;
                }
                else if (board_number == 1)
                {
                    board[row][column] = 2;
                    board_number--;
                }
                else
                {
                    board[row][column] = board_number;
                    board_number--;
                }
            }
        }
    }
}

/**
 * Prints the board in its current state.
 */
void draw(void)
{
    char change = '_';
    
    if (dim % 2 == 1)
    {
        for (int i = 0; i < dim; i++)
        {
            for (int j = 0; j < dim; j++)
            {
                if (board[i][j] == 0)
                {
                    printf("%3c", change);
                }
                else
                {
                    printf("%3i",board[i][j]);
                }
            }
            printf("\n\n");
        }
        printf("\n");
    }
    else
    {
        for (int i = 0; i < dim; i++)
        {
            for (int j = 0; j < dim; j++)
            {
                if (board[i][j] == 0)
                {
                    printf("%3c", change);
                }
                else
                {
                    printf("%3i",board[i][j]);
                }
            }
            printf("\n\n");
        }
        printf("\n");
    }
}


// boolean functions to determine whether direction selected is successful
bool it_is_right(int i, int j, int tile)
{
    int temp = tile;
    board[i][j] = board[i][j + 1];
    board[i][j + 1] = temp;
    return true;
}

bool it_is_left(int i, int j, int tile)
{
    int temp = tile;
    board[i][j] = board[i][j - 1];
    board[i][j - 1] = temp;
    return true;
}

bool it_is_up(int i, int j, int tile)
{
    int temp = tile;
    board[i][j] = board[i - 1][j];
    board[i - 1][j] = temp;
    return true;
}

bool it_is_down(int i, int j, int tile)
{
    int temp = tile;
    board[i][j] = board[i + 1][j];
    board[i + 1][j] = temp;
    return true;
}

/**
 * If tile borders empty space, moves tile and returns true, else
 * returns false. 
 */
bool move(int tile)
{
    // TODO
    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            int move_right = board[i][j + 1];
            int move_left = board[i][j - 1];
            int move_up = board[i - 1][j];
            int move_down = board[i + 1][j];
            
            // where is tile
            if (board[i][j] == tile)
            {
                // determine where tile is relative to board
                // tile is on left
                if (board[j] == board[0])
                {
                    // if tile is left / top
                    if (board[i] == board[0]) 
                    {
                        // found blank tile is right
                        if (move_right == 0)
                        {
                            it_is_right(i, j, tile);
                            return true;
                        }
                        // found blank tile down
                        else if (move_down == 0)
                        {
                            it_is_down(i, j, tile);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    // if tile is left / bottom
                    else if (board[i] == board[dim - 1])
                    {
                        // if blank is above
                        if (move_up == 0)
                        {
                            it_is_up(i, j, tile);
                            return true;
                        }
                        // if blank is on right
                        else if (move_right == 0)
                        {
                            it_is_right(i, j, tile);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    // it tile is left / middle
                    else
                    {
                        // if blank is above
                        if (move_up == 0)
                        {
                            it_is_up(i, j, tile);
                            return true;
                        }
                        // if blank is down
                        else if (move_down == 0)
                        {
                            it_is_down(i, j, tile);
                            return true;
                        }
                        // if blank is right
                        else if (move_right == 0)
                        {
                            it_is_right(i, j, tile);
                            return true;
                        }
                        else 
                        {
                            return false;
                        }
                    }
                }
                // tile is on right
                else if (board[j] == board[dim - 1])
                {
                    // if tile is right / top
                    if (board[i] == board[0])
                    {
                        // found blank tile is left
                        if (move_left == 0)
                        {
                            it_is_left(i, j, tile);
                            return true;
                        }
                        // found blank tile down
                        else if (move_down == 0)
                        {
                            it_is_down(i, j, tile);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    // if tile is right / bottom
                    else if (board[i] == board[dim - 1])
                    {
                        // if blank is above
                        if (move_up == 0)
                        {
                            it_is_up(i, j, tile);
                            return true;
                        }
                        // if blank is on left
                        else if (move_left == 0)
                        {
                            it_is_left(i, j, tile);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    // it is in right / middle
                    else
                    {
                         // if blank is above
                        if (move_up == 0)
                        {
                            it_is_up(i, j, tile);
                            return true;
                        }
                        // if blank is down
                        else if (move_down == 0)
                        {
                            it_is_down(i, j, tile);
                            return true;
                        }
                        // if blank is left
                        else if (move_left == 0)
                        {
                            it_is_left(i, j, tile);
                            return true;
                        }
                        else 
                        {
                            return false;
                        }
                    }
                    return false;
                }
                // tile is in middle
                else
                {
                    // if tile is on middle top
                    if (board[i] == board[0])
                    {
                        // found blank tile is left
                        if (move_left == 0)
                        {
                            it_is_left(i, j, tile);
                            return true;
                        }
                        // found blank tile down
                        else if (move_down == 0)
                        {
                            it_is_down(i, j, tile);
                            return true;
                        }
                        // found right
                        else if (move_right == 0)
                        {
                            it_is_right(i, j, tile);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    // if tile is on middle bottom
                    else if (board[i] == board[dim - 1])
                    {
                        // found blank tile is left
                        if (move_left == 0)
                        {
                            it_is_left(i, j, tile);
                            return true;
                        }
                        // found blank tile above
                        else if (move_up == 0)
                        {
                            it_is_up(i, j, tile);
                            return true;
                        }
                        // found right
                        else if (move_right == 0)
                        {
                            it_is_right(i, j, tile);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    // it is in the middle middle
                    else
                    {
                        // found blank tile is left
                        if (move_left == 0)
                        {
                            it_is_left(i, j, tile);
                            return true;
                        }
                        // found blank tile above
                        else if (move_up == 0)
                        {
                            it_is_up(i, j, tile);
                            return true;
                        }
                        // found right
                        else if (move_right == 0)
                        {
                            it_is_right(i, j, tile);
                            return true;
                        }
                        // found blank tile down
                        else if (move_down == 0)
                        {
                            it_is_down(i, j, tile);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }
    }
    return true;
}    

/**
 * Returns true if game is won (i.e., board is in winning configuration), 
 * else false.
 */
bool won(void)
{

    // TODO
    int wrong = 0;
    // need to cycle through array checking each
    for (int i = 0; i < dim ; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            if (board[i][j] == winner[i][j])
            {
                
            }
            else
            {
                wrong++;
            }
        }
    }
    
    if (wrong > 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void winning_pos(void)
{
    // TODO
    // need to cycle through array checking eachs
    int count = 1;

    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            if (winner[i] == winner[dim - 1] && winner[j] == winner[dim - 1])
            {
                winner[i][j] = 0;
            }
            else
            {
                winner[i][j] = count;
            }
            count++;
        }
    }
}
